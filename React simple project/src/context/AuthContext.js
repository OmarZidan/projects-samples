import React ,{createContext, useState} from 'react';

export const AuthContext = createContext();

export const AuthProvider = (props) => {
  const [user, setUser] = useState({
    token: '',
    token_type: '',
    expires_at: '',
    id: '',
    name: ''
  });

  return(
    <AuthContext.Provider value={[user, setUser]}>{props.children}</AuthContext.Provider>
  );
};