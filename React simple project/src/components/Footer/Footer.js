import React from "react";
import "./Footer.scss";
import facebook from "./../../images/facebook.png";
import instagram from "./../../images/instagram.png";
import mail from "./../../images/mail.png";
import fawry from "./../../images/fawrylogo.png";



function Footer(props) {
  return (
    <footer className="footer-container">
      <div className="footer-grid">
        <section>
          <div>
            <ul id="menu-footer-menu" className="menu">
              <li
              >
                <a href="https://aloula-eg.com/%d9%88%d8%b8%d8%a7%d8%a6%d9%81/">
                  وظائف
                </a>
              </li>
              <li
              >
                <a href="https://aloula-eg.com/%d8%aa%d9%88%d8%a7%d8%b5%d9%84-%d9%85%d8%b9%d9%86%d8%a7/">
                  تواصل معنا
                </a>
              </li>
            </ul>
          </div>
        </section>{" "}
        <div className="fawry">
          <a href="https://myfawry.com/">
            <img src={fawry} alt="fawry-logo" />
          </a>
        </div>
        <div className="socialMedia">


          <div>
            <a href="/" target="_blank">
              <img src={mail} alt="mail-icon"/>
            </a>
          </div>

          <div>
            <a href="https://www.instagram.com/aloulaeg/" rel="noopener noreferrer" target="_blank">
              <img src={instagram} alt="instagram-icon"/>
            </a>
          </div>

          <div>
            <a href="https://www.facebook.com/aloulaeg" rel="noopener noreferrer" target="_blank">
              <img src={facebook} alt="facebook-icon"/>
            </a>
          </div>
        </div>
      </div>
      <div className="copyrights">
        <p>
          © حقوق النسخ محفوظة, الأولى 2020. تصميم{" "}
          <a href="http://unpluggedweb.com/" rel="noopener noreferrer" target="_blank">
            Unplugged
          </a>{" "}
        </p>
        <ul className="footer_submenu">
          <li>
            <a href="/">شروط وأحكام</a>
          </li>
          <li className="divider">|</li>
          <li>
            <a href="/">الخصوصية</a>
          </li>
        </ul>
      </div>
    </footer>
  );
}

export default Footer;
