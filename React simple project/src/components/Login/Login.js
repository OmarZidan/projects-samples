import React, { useReducer, useContext, useState } from "react";
import "./Login.scss";
import Title from "./../Title/Title";
import Subtitle from "./../Subtitle/Subtitle";
import TextInput from "./../TextInput/TextInput";
import Button from "./../Button/Button";
import { AuthContext } from "./../../context/AuthContext";
import { useHistory } from "react-router-dom";
import {baseUrl} from './../baseUrl';

function Login(props) {
  const history = useHistory();
  const [user, setUser] = useContext(AuthContext);
  const [inputsErrors, setInputsErrors] = useState([]);
  const [loginErrors, setLoginErrors] = useState("");
  const [linkOtp, setLinkOtp] = useState('')
  const axios = require("axios");
  const [form, setForm] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    { mobile_number: "", password: "" }
  );

  const handleSubmit = event => {
    event.preventDefault();
    let url = `${baseUrl}api/login`; 
    axios
      .post(url, form)
      .then(function(response) {
        let data = response.data;
        console.log(data.data)
        localStorage.setItem('token', data.access_token);
        localStorage.setItem('name', data.data.name);

        setUser(user => ({
          ...user,
          token: data.access_token,
          token_type: data.token_type,
          expires_at: data.expires_at,
          name: data.data.name
        }));
        history.push("/loans");
      })
      .catch(function(error) {
        console.log(error)
        if (error.response.status === 422) {
          setInputsErrors(error.response.data.error.message);
        } else if (error.response.status === 401) {
          setLoginErrors(error.response.data.message);
        } else if (error.response.status === 403) {
          setLoginErrors(error.response.data.message);
          setLinkOtp(error.response.status);
        }
      });
  };

  const handleOnChange = event => {
    const { name, value } = event.target;
    setForm({ [name]: value });
    setInputsErrors([]);
    setLoginErrors("");
    setLinkOtp('');
  };

  return (
    <div className="loginConatiner">
      <Title text="تسجيل الدخول" />
      <Subtitle text="أدخل بياناتك الشخصية" customClassName="loginSubtitle" />
      {loginErrors && <p className="loginErrorMsg">{loginErrors}</p>}
      {linkOtp && (
      <a href={"/otp"}>
        <p className="linkOtp">ادخل الكود هنا</p>
      </a>
      )}
      <form onSubmit={handleSubmit}>
        <TextInput
          placeholder="التليفون المحمول"
          type="tel"
          value={form.mobile_number}
          onChange={handleOnChange}
          name="mobile_number"
          customClassName="loginInput"
          errorMsg={inputsErrors.mobile_number}
        />
        <TextInput
          placeholder="كلمة المرور"
          type="password"
          value={form.password}
          onChange={handleOnChange}
          name="password"
          customClassName="loginInput"
          errorMsg={inputsErrors.password}
        />
        <Button
          text="تسجيل"
          customClassName="loginBtn"
          onClick={handleSubmit}
        />
      </form>
      <a href={"/register"}>
        <p className="underlineText">لا يوجد عندك حساب؟ أنشاء حساب</p>
      </a>
      <a href={"/forgot-password"}>
        <p className="underlineText">نسيت كلمة المرور؟</p>
      </a>
    </div>
  );
}

export default Login;
