import React from 'react';
import './Button.scss';

function TextInput({text, customClassName, onClick}) {
  return (
    <div className={customClassName}>
      <input type="submit" value={text} className="submitBtn" onClick={onClick}/>
    </div>
  );
}

export default TextInput;