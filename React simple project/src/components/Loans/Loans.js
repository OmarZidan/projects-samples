import React, { useState, useContext, useEffect } from "react";
import "./Loans.scss";
import arrowDown from "./../../images/down-arrow.png";
import arrowUp from "./../../images/arrow-up.png";
import Title from "./../Title/Title";
import { AuthContext } from "./../../context/AuthContext";
import {baseUrl} from './../baseUrl';


function Loans(props) {
  const [user, setUser] = useContext(AuthContext);
  const [showDetails, setShowDetails] = useState(false);
  const [activeLoan, setActiveLoan] = useState("");
  const [emptyLoans, setEmptyLoans] = useState(false)
  const axios = require("axios");
  const [loans, setLoans] = useState([]);


  useEffect(() => {
    const AuthStr = "Bearer ".concat(user.token);
    // const URL = "http://142.93.249.231/api/user_loans";
    let url = `${baseUrl}api/user_loans`; 

    axios
      .get(url, { headers: { Authorization: AuthStr } })
      .then(response => {
        console.log(response.data)
        if(response.data.length === 0){
          setEmptyLoans(true)
        }
        setLoans(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }, [axios, user.token]);

  const handleConvertNumber = number => {
    let en_number = number.toString();
    let arabicDigits = "٠١٢٣٤٥٦٧٨٩";
    let arabianMap = arabicDigits.split("");
    let arabic_number = en_number.replace(/\d/g, function(m) {
      return arabianMap[parseInt(m)];
    });
    return arabic_number;
  };

  const handleShowDetails = id => {
    setShowDetails(!showDetails);
    setActiveLoan(id);
  };
  return (
    <div className="loansConatiner">
      <Title text="اقساطك" />
      <div className="loansContainer">
        {emptyLoans && <p className="noLoans">لا يوجد اقساط </p>}
        {loans &&
          loans.map(loan => {
            return (
              <table
                className="loanContainer"
                key={loan.id}
                border="0"
                cellSpacing="0"
              >
                <thead>
                  <tr>
                    <th style={{ width: "40%" }}>
                      <p className="headText black">القرض رقم</p>
                    </th>
                    <th>
                      <p className="headText red">
                        {handleConvertNumber(loan.loan_number)}
                      </p>
                    </th>
                    <th>
                      <div className="showBtnContainer">
                        <span
                          onClick={() => handleShowDetails(loan.id)}
                          className="showBtn"
                        >
                          <img
                            className="icon"
                            src={
                              showDetails && activeLoan === loan.id
                                ? arrowUp
                                : arrowDown
                            }
                            alt="arrow-down"
                          />
                        </span>
                      </div>
                    </th>
                  </tr>
                </thead>
                {showDetails && loan.id === activeLoan && (
                  <tbody className="loanDetails">
                    <tr>
                      <td className="detailsText black">اجمالي القرض القائم</td>
                      <td colSpan="2" className="detailsText red">
                        {handleConvertNumber(loan.total_due_amounts)}
                      </td>
                    </tr>

                    <tr>
                      <td className="detailsText black">
                        إجمالي الاقساط المستحقة
                      </td>
                      <td colSpan="2" className="detailsText red">
                        {handleConvertNumber(loan.total_outstanding_amounts)}
                      </td>
                    </tr>

                    <tr>
                      <td className="detailsText black">
                        عدد الاقساط المستحقة
                      </td>
                      <td colSpan="2" className="detailsText red">
                        {handleConvertNumber(loan.number_of_past_dues)}
                      </td>
                    </tr>

                    <tr>
                      <td className="detailsText black">عدد أيام التأخير</td>
                      <td colSpan="2" className="detailsText red">
                        {handleConvertNumber(loan.due_payments_days)}
                      </td>
                    </tr>

                    <tr>
                      <td className="detailsText black">
                        أخر قسط مدفوع بتاريخ
                      </td>
                      <td colSpan="2" className="detailsText red">
                        {handleConvertNumber(
                          loan.last_unpaid_date
                            .split("-")
                            .reverse()
                            .join("-")
                        )}
                      </td>
                    </tr>
                  </tbody>
                )}
              </table>
            );
          })}
      </div>
    </div>
  );
}

export default Loans;
