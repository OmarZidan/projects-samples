import React from "react";
import "./Main.scss";
import Register from "./Register/Register";
import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import banner from "./../images/banner.jpg";
import Login from "./Login/Login";
import Otp from "./Otp/Otp"
import Loans from "./Loans/Loans";
import ForgotPassword from "./forgotPassword/forgotPassword";
import ResetPassword from "./resetPassword/resetPassword";
import {Route, Switch} from 'react-router-dom';
import {AuthProvider} from './../context/AuthContext';
import { ProtectedRoute } from "./protected.route";



function Main() { 
  return (
    <div>
    <AuthProvider>

      <div>

        <Header />
        <div className="banner">
          <img src={banner} alt="banner" className="bannerImg" />
          <div className="bannerTitle">
            <h1>
              اﻻستعلام و<br />
              دفع الأقساط
            </h1>
          </div>
        </div>
      </div>
                <div className="content">
            <Switch>
              <Route exact path="/" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/otp" component={Otp} />
              <Route exact path="/forgot-password" component={ForgotPassword} />
              <ProtectedRoute exact path="/reset-password" component={ResetPassword} />
              <ProtectedRoute exact path="/loans" component={Loans} />
              <Route path="*" component={() => "404 NOT FOUND"} />
            </Switch>
          </div>
      <Footer/>
    </AuthProvider>

    </div>

  );
}

export default Main;
