import React from 'react';
import './Subtitle.scss';

function Subtitle(props) {
  return (
    <div className={props.customClassName}>
      <h2 className="sectionSubtitle">{props.text}</h2>
    </div>
  );
}

export default Subtitle;