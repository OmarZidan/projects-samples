import React, { useState, useContext } from "react";
import "./Otp.scss";
import Title from "./../Title/Title";
// import OTPInput, { ResendOTP } from "otp-input-react";
import OTPInput from "otp-input-react";
import Button from "./../Button/Button";
import { AuthContext } from "./../../context/AuthContext";
import { useHistory } from "react-router-dom";
import {baseUrl} from './../baseUrl';


function Otp(props) {
  const history = useHistory();
  const [OTP, setOTP] = useState("");
  const axios = require("axios");
  const [otpErrors, setOtpErrors] = useState("");
  const [user, setUser] = useContext(AuthContext);

  const handleSubmit = event => {
    event.preventDefault();
    const id = localStorage.getItem("userId");

    let data = {
      otp: OTP,
      id: id
    };
    let url = `${baseUrl}api/validate_otp`; 
    axios
      .post(url, data)
      .then(function(response) {
        let data = response.data.data;
        localStorage.setItem('token', data.accessToken);
        localStorage.setItem('name', data.name);
        setUser(user => ({ ...user, token: data.accessToken, name:data.name }));
        history.push("/loans");
      })
      .catch(function(error) {
        let data = error.response;
        if (data.status === 422) {
          setOtpErrors(data.data.error.message.otp);
        } else if (data.status === 403) {
          setOtpErrors(data.data.error.message);
        }
      });
  };

  return (
    <div className="registerConatiner">
      <Title text="إنشاء حساب جديد" />
      <h3 className="otpSubtitle">أدخل الرقم المرسل إليك على الهاتف</h3>
      <p className="otpError">{otpErrors}</p>
      <div dir="ltr">
        <OTPInput
          value={OTP}
          onChange={setOTP}
          autoFocus
          OTPLength={4}
          otpType="number"
          disabled={false}
          // secure
          inputStyles={{
            height: "5rem",
            width: "4rem",
            fontSize: "1.5rem",
            border: "1px solid rgba(0,0,0,0.3)"
          }}
          style={{ justifyContent: "center" }}
        />
      </div>
      {/* <ResendOTP handelResendClick={() => console.log("Resend clicked")} /> */}
      <Button text="تأكيد" customClassName="otpSubmit" onClick={handleSubmit} />
    </div>
  );
}

export default Otp;
