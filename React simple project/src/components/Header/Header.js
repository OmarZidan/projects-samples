import React, {useContext} from "react";
import "./Header.scss";
import logo from "./../../images/logo.png";
import { AuthContext } from "./../../context/AuthContext";
import { useHistory } from "react-router-dom";
import {baseUrl} from './../baseUrl';




function Header(props) {
  const history = useHistory();
  const axios = require("axios");
  const [user, setUser] = useContext(AuthContext);
  const name = localStorage.getItem("name");
  const token = localStorage.getItem("token");


  const handleLogout = event => {
    event.preventDefault();
    const AuthStr = "Bearer ".concat(token);
    let url = `${baseUrl}api/logout`; 
    axios
      .post(url, { headers: { Authorization: AuthStr } })
      .then(function(response) {
        localStorage.setItem('token', '');
        localStorage.setItem('name', '');
        localStorage.setItem('userId', '');
        setUser(user => ({
          ...user,
          token: '',
          token_type: '',
          expires_at: '',
          name: '',
          id: ''
        }));
        history.push("/");
      })
      .catch(function(error) {
        console.log(error)
      });
  };

  return (
    <div>
      <header>
        <img src={logo} alt="aloula-logo" className="headerLogo" />
        <div className="header-left">
        {name && (
          <div className="loggedContainer">
          <p className="loggedName">مرحبا {name} </p>
          <ul className="loggedMenu">
            <li><a href="/loans">اقساطي</a></li>
            <li><a href="/reset-password">تغيير كلمة المرور</a></li>
            <li><a onClick={handleLogout}>تسجيل الخروج</a></li>
          </ul>
        </div>
        )}
        <a className="headerBtn" href="https://aloula-eg.com">رجوع إلى الموقع</a>
        </div>

      </header>
    </div>
  );
}

export default Header;
