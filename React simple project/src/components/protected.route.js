import React, {useContext} from "react";
import { Route, Redirect } from "react-router-dom";
// import auth from "./auth";
import {AuthContext} from './../context/AuthContext'


export const ProtectedRoute = ({
  component: Component,
  ...rest
}) => {
  const [user] = useContext(AuthContext)
  const token = localStorage.getItem("token");
  return (
    <Route
      {...rest}
      render={props => {
        if (token) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: "/",
                state: {
                  from: props.location
                }
              }}
            />
          );
        }
      }}
    />
  );
};
