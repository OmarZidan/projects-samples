import React from 'react';
import './Title.scss';

function Title(props) {
  return (
    <div>
      <h1 className="sectionTitle">{props.text}</h1>
    </div>
  );
}

export default Title;