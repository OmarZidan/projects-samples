import React from "react";
import "./Subheader.scss";


function Subheader(props) {
  return (
    <div>      
        <div className="bottom-header-menu">
          <ul id="menu-bottom-header-menu" className="menu">
            <li id="tatwer" className="has-child">
              <a href="https://aloula-eg.com/%d8%a7%d9%84%d8%aa%d8%b9%d9%85%d9%8a%d8%b1-%d9%84%d9%84%d8%aa%d8%b7%d9%88%d9%8a%d8%b1-%d8%a7%d9%84%d8%b9%d9%82%d8%a7%d8%b1%d9%8a/" onMouseOver={() => {console.log('tatwer')}} >
                التعمير للتطوير العقاري
                
              </a>
              <ul className="sub-menu menu" id="tatwer-submenu">
                <li
                >
                  <a href="http://aloula-eg.com/%d8%a7%d9%84%d8%aa%d8%b9%d9%85%d9%8a%d8%b1-%d9%84%d9%84%d8%aa%d8%b7%d9%88%d9%8a%d8%b1-%d8%a7%d9%84%d8%b9%d9%82%d8%a7%d8%b1%d9%8a/#financial-reports">
                    التقارير المالية
                  </a>
                </li>
                <li
                >
                  <a href="http://aloula-eg.com/%d8%a7%d9%84%d8%aa%d8%b9%d9%85%d9%8a%d8%b1-%d9%84%d9%84%d8%aa%d8%b7%d9%88%d9%8a%d8%b1-%d8%a7%d9%84%d8%b9%d9%82%d8%a7%d8%b1%d9%8a/#board-members">
                    مجلس الإدارة
                  </a>
                </li>
                <li
                >
                  <a href="http://aloula-eg.com/%d8%a7%d9%84%d8%aa%d8%b9%d9%85%d9%8a%d8%b1-%d9%84%d9%84%d8%aa%d8%b7%d9%88%d9%8a%d8%b1-%d8%a7%d9%84%d8%b9%d9%82%d8%a7%d8%b1%d9%8a/#shareholders">
                    المساهمين
                  </a>
                </li>
                <li
                >
                  <a href="https://aloula-eg.com/%d8%a3%d8%ae%d8%a8%d8%a7%d8%b1%d9%86%d8%a7/">
                    أخبارنا
                  </a>
                </li>
                <li
                >
                  <a href="https://aloula-eg.com/%d8%a7%d9%84%d8%ad%d9%88%d9%83%d9%85%d8%a9/">
                    الحوكمة
                  </a>
                </li>
                <li
                >
                  <a href="https://aloula-eg.com/%d8%aa%d9%88%d8%a7%d8%b5%d9%84-%d9%85%d8%b9%d9%86%d8%a7/">
                    تواصل معنا
                  </a>
                </li>
              </ul>
            </li>
            <li id="ta2ger" className="has-child">
              <a href="https://aloula-eg.com/%d8%b4%d8%b1%d9%83%d8%a9-%d8%a7%d9%84%d8%aa%d8%a3%d8%ac%d9%8a%d8%b1-%d8%a7%d9%84%d8%aa%d9%85%d9%88%d9%8a%d9%84%d9%8a-%d9%88-%d8%a7%d9%84%d8%aa%d8%ae%d8%b5%d9%8a%d9%85/">
                التأجير التمويلي و التخصيم
              </a>
              <ul className="sub-menu" id="ta2ger-submenu">
                <li
                >
                  <a href="http://aloula-eg.com/%d8%a7%d9%84%d8%aa%d8%b9%d9%85%d9%8a%d8%b1-%d9%84%d9%84%d8%aa%d8%b7%d9%88%d9%8a%d8%b1-%d8%a7%d9%84%d8%b9%d9%82%d8%a7%d8%b1%d9%8a/#financial-reports">
                    التقارير المالية
                  </a>
                </li>
                <li
                >
                  <a href="http://aloula-eg.com/%d8%a7%d9%84%d8%aa%d8%b9%d9%85%d9%8a%d8%b1-%d9%84%d9%84%d8%aa%d8%b7%d9%88%d9%8a%d8%b1-%d8%a7%d9%84%d8%b9%d9%82%d8%a7%d8%b1%d9%8a/#board-members">
                    مجلس الإدارة
                  </a>
                </li>
                <li
                >
                  <a href="http://aloula-eg.com/التعمير-للتمويل-العقاري/#shareholders">
                    المساهمين
                  </a>
                </li>
                <li
                >
                  <a href="https://aloula-eg.com/%d8%b7%d9%84%d8%a8-%d8%aa%d8%a3%d8%ac%d9%8a%d8%b1-%d8%aa%d9%85%d9%88%d9%8a%d9%84%d9%89/">
                    طلب تأجير تمويلى
                  </a>
                </li>
                <li
                >
                  <a href="https://aloula-eg.com/%d8%a7%d9%84%d8%aa%d8%ae%d8%b5%d9%8a%d9%85/">
                    التخصيم
                  </a>
                </li>
                <li
                >
                  <a href="https://aloula-eg.com/%d8%a7%d8%b3%d8%a6%d9%84%d8%a9-%d8%b9%d8%a7%d9%85%d8%a9-%d9%88-%d9%85%d8%aa%d9%83%d8%b1%d8%b1%d8%a9/">
                    اسئلة عامة و متكررة
                  </a>
                </li>
              </ul>
            </li>
            <li id="ta3mer" className="has-child">
              <a href="https://aloula-eg.com/%d8%a7%d9%84%d8%aa%d8%b9%d9%85%d9%8a%d8%b1-%d9%84%d9%84%d8%aa%d9%85%d9%88%d9%8a%d9%84-%d8%a7%d9%84%d8%b9%d9%82%d8%a7%d8%b1%d9%8a/">
                التعمير للتمويل العقاري
              </a>
              <ul className="sub-menu" id="ta3mer-submenu">
                <li
                >
                  <a href="http://aloula-eg.com/التعمير-للتمويل-العقاري/#financial-reports">
                    التقارير المالية
                  </a>
                </li>
                <li
                >
                  <a href="http://aloula-eg.com/التعمير-للتمويل-العقاري/#board-members">
                    مجلس الإدارة
                  </a>
                </li>
                <li
                >
                  <a href="http://aloula-eg.com/التعمير-للتمويل-العقاري/#shareholders">
                    المساهمين
                  </a>
                </li>
                <li id="products" className="has-child">
                  <a href="http://#">منتجاتنا</a>
                  <ul className="sub-menu" id="products-submenu">
                    <li
                    >
                      <a href="https://aloula-eg.com/%d8%a7%d9%84%d8%aa%d9%85%d9%88%d9%8a%d9%84-%d8%a7%d9%84%d8%b9%d9%82%d8%a7%d8%b1%d9%8a/">
                        التمويل العقاري
                      </a>
                    </li>
                    <li
                    >
                      <a href="https://aloula-eg.com/%d8%a7%d9%84%d8%a5%d8%ac%d8%a7%d8%b1%d8%a9/">
                        الإجارة
                      </a>
                    </li>
                    <li
                    >
                      <a href="https://aloula-eg.com/%d8%a7%d9%84%d8%aa%d8%b4%d8%b7%d9%8a%d8%a8/">
                        التشطيب
                      </a>
                    </li>
                  </ul>
                </li>
                <li
                >
                  <a href="https://aloula-eg.com/%d8%a7%d9%84%d9%85%d8%b3%d8%aa%d9%86%d8%af%d8%a7%d8%aa-%d8%a7%d9%84%d9%85%d8%b7%d9%84%d9%88%d8%a8%d8%a9/">
                    المستندات المطلوبة
                  </a>
                </li>
                <li
                >
                  <a href="https://aloula-eg.com/%d8%a7%d9%84%d9%85%d8%b4%d8%b1%d9%88%d8%b9%d8%a7%d8%aa-%d8%a7%d9%84%d9%85%d8%b9%d8%aa%d9%85%d8%af%d8%a9/">
                    المشروعات المعتمدة
                  </a>
                </li>
                <li
                >
                  <a href="https://aloula-eg.com/%d8%a7%d8%b3%d8%a6%d9%84%d8%a9-%d8%b9%d8%a7%d9%85%d8%a9-%d9%88-%d9%85%d8%aa%d9%83%d8%b1%d8%b1%d8%a9/">
                    اسئلة عامة و متكررة
                  </a>
                </li>
              </ul>
            </li>
            <li
            >
              <a href="https://aloula-eg.com/%d8%aa%d9%85%d9%88%d9%8a%d9%84-%d8%a7%d9%84%d9%85%d8%b4%d8%b1%d9%88%d8%b9%d8%a7%d8%aa-%d8%a7%d9%84%d8%b5%d8%ba%d9%8a%d8%b1%d8%a9-%d9%88-%d8%a7%d9%84%d9%85%d8%aa%d9%88%d8%b3%d8%b7%d8%a9/">
                تمويل المشروعات الصغيرة و المتوسطة
              </a>
            </li>
          </ul>
        </div>
    </div>
  );
}

export default Subheader;
