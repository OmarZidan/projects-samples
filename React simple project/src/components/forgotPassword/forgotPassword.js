import React, { useReducer, useState} from 'react';
import './forgotPassword.scss';
import Title from './../Title/Title';
import TextInput from './../TextInput/TextInput';
import Button from './../Button/Button';
import {baseUrl} from './../baseUrl';


function ForgotPassword(props) {
  // const [user, setUser] = useContext(AuthContext);
  const [inputsErrors, setInputsErrors] = useState([])
  const [forgotErrors, setForgotErrors] = useState('');
  const [successMsg, setSuccessMsg] = useState(false);
  // const history = useHistory();
  const axios = require('axios');
  const [form, setForm] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    { mobile_number: ''});


  const handleOnChange = event => {
    const { name, value } = event.target;
    setForm({[name]: value });
    setInputsErrors([]);
    setForgotErrors('');
    setSuccessMsg(false);
  };

  const handleSubmit = event => {
    event.preventDefault();
    let url = `${baseUrl}api/forgot?mobile_number=${form.mobile_number}`; 
    axios.post(url, form)
    .then(function (response) {
      console.log(response)
      setSuccessMsg(true);
    })
    .catch(function (error) {
      console.log(error.response.data.error.message.mobile_number)
      if(error.response.status === 422){
        setInputsErrors(error.response.data.error.message)
      }
      else if (error.response.status === 404){
        setForgotErrors("رقم الموبايل غير صحيح");
      }
    });
  };
 
  return (
    <div className="registerConatiner">
      <div style={{marginBottom: '7rem'}}>
      <Title text="استرجاع كلمة المرور" />
      </div>
      {forgotErrors && (
        <p className="loginErrorMsg">{forgotErrors}</p>
      )}
      {successMsg && (
        <p className="loginErrorMsg"> تم ارسال كلمة  الجديدة بنجاح,
        <a href={"/"} style={{color: '#000'}}>
          يمكنك تسجيل الدخول اﻻن
        </a>
      </p>
      )}
      <form onSubmit={handleSubmit}>
        <TextInput 
          placeholder="التليفون المحمول" 
          type="tel"
          value={form.mobile_number}           
          onChange={handleOnChange}
          name="mobile_number"
          customClassName="registerInput"
          errorMsg={inputsErrors.mobile_number}
        />
        <Button text="تأكيد" customClassName="registerBtn" />  
      </form>
    </div>
  );
}

export default ForgotPassword;