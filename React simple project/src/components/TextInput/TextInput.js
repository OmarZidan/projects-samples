import React from 'react';
import './TextInput.scss';

function TextInput({type,value, placeholder, onChange, name, customClassName, errorMsg}) {
  return (
    <div className={customClassName}>
    <input 
    type={type} 
    value={value} 
    onChange={onChange} 
    placeholder={placeholder} 
    name={name}
    className="formInput" 
    />
    
    {errorMsg && (
      <p className="errorMsg">{errorMsg}</p>
    )}
    </div>
  );
}

export default TextInput;