import React, { useReducer, useContext, useState} from 'react';
import './resetPassword.scss';
import Title from './../Title/Title';
import TextInput from './../TextInput/TextInput';
import Button from './../Button/Button';
import {AuthContext} from './../../context/AuthContext'
import {baseUrl} from './../baseUrl';


function ResetPassword(props) {
  const [inputsErrors, setInputsErrors] = useState([])
  const [resetErrors, setResetErrors] = useState('');
  const [successMsg, setSuccessMsg] = useState(false);

  const axios = require('axios');
  const [form, setForm] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    { old_password: '', new_password: ''});
  const token = localStorage.getItem("token");



  const handleOnChange = event => {
    const { name, value } = event.target;
    setForm({[name]: value });
    setInputsErrors([]);
    setResetErrors('');
    setSuccessMsg(false);
  };

  const handleSubmit = event => {
    event.preventDefault();
    const AuthStr = "Bearer ".concat(token);
    let url = `${baseUrl}api/reset`; 
    axios.post(url, form, { headers: { Authorization: AuthStr } })
    .then(function (response) {
      console.log(response)
      setSuccessMsg(true);
    })
    .catch(function (error) {
      console.log(error.response)
      if(error.response.status === 422){
        setInputsErrors(error.response.data.error.message)
      }else if (error.response.status === 403){
        setResetErrors(error.response.data.error.message);
      }
    });
  };
 
  return (
    <div className="registerConatiner">
      <div style={{marginBottom: '7rem'}}>
        <Title text="اعادة تعيين كلمة المرور" />
      </div>
      {resetErrors && (
        <p className="loginErrorMsg">{resetErrors}</p>
      )}
      {successMsg && (
        <p className="loginErrorMsg"> تم تغيير كلمة المرور بنجاح</p>
      )}
      <form onSubmit={handleSubmit}>
      <TextInput 
          placeholder="كلمة المرور القديمة" 
          type="password"
          value={form.old_password}           
          onChange={handleOnChange}
          name="old_password"
          customClassName="registerInput"
          errorMsg={inputsErrors.old_password}
        />
                <TextInput 
          placeholder="كلمة المرور الجديدة" 
          type="password"
          value={form.new_password}           
          onChange={handleOnChange}
          name="new_password"
          customClassName="registerInput"
          errorMsg={inputsErrors.new_password}
        /> 
        <Button text="تأكيد" customClassName="registerBtn" />  
      </form>
    </div>
  );
}

export default ResetPassword;