import React, { useReducer, useContext, useState} from 'react';
import './Register.scss';
import Title from './../Title/Title';
import Subtitle from './../Subtitle/Subtitle';
import TextInput from './../TextInput/TextInput';
import Button from './../Button/Button';
import {AuthContext} from './../../context/AuthContext'
import { useHistory } from "react-router-dom";
import {baseUrl} from './../baseUrl';


function Register(props) {
  const [user, setUser] = useContext(AuthContext);
  const [inputsErrors, setInputsErrors] = useState([])
  const [registerErrors, setRegisterErrors] = useState('');


  const history = useHistory();
  const axios = require('axios');
  const [form, setForm] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {name:'', national_id: '', mobile_number: '', password: ''});


  const handleOnChange = event => {
    const { name, value } = event.target;
    setForm({[name]: value });
    setInputsErrors([]);
    setRegisterErrors('');
  };

  const handleSubmit = event => {
    event.preventDefault();
    let url = `${baseUrl}api/register`; 
    axios.post(url, form)
    .then(function (response) {
      let data = response.data;
      localStorage.setItem('userId', data.id);
      setUser(user => ({...user, id: data.id}))
      history.push("/otp");
    })
    .catch(function (error) {
      console.log(error.response)
      if(error.response.status === 422){
        setInputsErrors(error.response.data.error.message)
      }else if (error.response.status === 403){
        setRegisterErrors(error.response.data.error.message);
      }
    });
  };
 
  return (
    <div className="registerConatiner">
      <Title text="إنشاء حساب جديد" />
      <Subtitle text="أدخل بياناتك الشخصية" customClassName="registerSubtitle"/>
      {registerErrors && (
        <p className="loginErrorMsg">{registerErrors}</p>
      )}
      <form onSubmit={handleSubmit}>
      <TextInput
          name="name"
          placeholder="اسم المستخدم" 
          value={form.name}           
          onChange={handleOnChange}
          type="text"
          customClassName="registerInput"
          errorMsg={inputsErrors.national_id}
        />
        <TextInput
          name="national_id"
          placeholder="الرقم القومي" 
          value={form.nationalID}           
          onChange={handleOnChange}
          type="text"
          customClassName="registerInput"
          errorMsg={inputsErrors.national_id}
        />
        <TextInput 
          placeholder="التليفون المحمول" 
          type="tel"
          value={form.mobile_number}           
          onChange={handleOnChange}
          name="mobile_number"
          customClassName="registerInput"
          errorMsg={inputsErrors.mobile_number}

        />    
        <TextInput 
          placeholder="كلمة المرور" 
          type="password"
          value={form.password}           
          onChange={handleOnChange}
          name="password"
          customClassName="registerInput"
          errorMsg={inputsErrors.password}
        /> 
        <Button text="تسجيل" customClassName="registerBtn" />  
      </form>
      <a href={"/"}>
      <p className="underlineText">يوجد عندك حساب؟ تسجيل الدخول</p>
      </a>
    </div>
  );
}

export default Register;