import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    titleContainer: {
        marginTop: 150,
        marginBottom: 70
    },
    bgImg: { width: "100%", height: "100%", opacity: 0.8 }
});

export default styles;
