import React, { useState, useContext } from "react";
import { View, ScrollView, ImageBackground, TouchableOpacity } from "react-native";
import { Header, Button, Title, SignInInput, Text } from "@components";
import styles from "./sign-in.style";
import newPlan from "@config/api";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";
import { AuthContext } from "./../../contexts/AuthContext";

export default function SignIn(): React.ReactElement {
    const [user, setUser] = useContext(AuthContext);
    const { navigate } = useNavigation();
    const [mobileFocused, setMobileFocused] = useState(Boolean);
    const [passFocused, setPassFocused] = useState(Boolean);
    const [form, setForm] = useState({
        // eslint-disable-next-line @typescript-eslint/camelcase
        mobile_number: "",
        password: ""
    });

    const changeForm = (property, newValue) => {
        const newState = { ...form };
        newState[property] = newValue;
        setForm(newState);
    };

    const onMobileFocus = () => {
        setPassFocused(false);
        setMobileFocused(true);
    };
    const onPassFocus = () => {
        setMobileFocused(false);
        setPassFocused(true);
    };

    const handleSignIn = async () => {
        try {
            const response = await newPlan.post("/auth/login", form);
            setUser(response.data.data);
            const firstTime = response.data.data.first_time_login;
            if (firstTime === 0) {
                navigate("Sales");
            } else if (firstTime === 1) {
                navigate("CreatePassword");
            }
            console.log(response.data.data);
        } catch (error) {
            const statusCode = error.response.status;
            if (statusCode === 401) {
                alert(error.response.data.error.message);
            } else if (statusCode === 422) {
                alert(error.response.data.error.message.mobile_number);
            }
        }
    };

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={require("@assets/images/filtersBG.png")} style={styles.bgImg}>
                <Header withBack withDrawerMenu={false} />
                <ScrollView>
                    <View style={{ paddingHorizontal: 25 }}>
                        <View style={styles.titleContainer}>
                            <Title fontSize={36} weight="ExtraBold" textTransform="uppercase">
                                Sign In
                            </Title>
                        </View>
                        <SignInInput
                            onFocus={onMobileFocus}
                            placeholder="Mobile Number"
                            returnKeyType="next"
                            icon={require("@assets/icons/dial.png")}
                            autoFocus
                            style={{ marginHorizontal: mobileFocused ? 0 : 18 }}
                            value={form.mobile_number}
                            onChangeText={val => changeForm("mobile_number", val)}
                        />
                        <SignInInput
                            secureTextEntry
                            onFocus={onPassFocus}
                            placeholder="Password"
                            returnKeyType="done"
                            icon={require("@assets/icons/key.png")}
                            style={{ marginHorizontal: passFocused ? 0 : 18 }}
                            value={form.password}
                            onChangeText={val => changeForm("password", val)}
                        />

                        <View style={{ width: "100%", marginBottom: 32, marginTop: 70 }}>
                            <Button
                                text="Get Started"
                                gradient
                                fontSize={13}
                                weight="SemiBold"
                                textColor={colors.white}
                                onPress={handleSignIn}
                            />
                        </View>
                        <TouchableOpacity>
                            <Text
                                weight="Regular"
                                style={{
                                    fontSize: 11,
                                    textDecorationLine: "underline",
                                    textAlign: "center"
                                }}
                            >
                                Forgot Password?
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
