import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.lightBlueGrey,
        flex: 1
    },
    titleContainer: {
        justifyContent: "flex-start",
        width: "100%",
        marginTop: 150,
        marginBottom: 10,
        paddingLeft: 25
    },
    content: {
        padding: 25,
        paddingBottom: 75
    },
    notification: {
        paddingVertical: 16,
        paddingHorizontal: 18,
        backgroundColor: colors.white,
        marginBottom: 30
    },
    notifImageWrap: {
        // shadowColor: colors.charcoalGrey24,
        // shadowOffset: {
        //     width: 0,
        //     height: 4
        // },
        // shadowRadius: 8,
        // shadowOpacity: 0.9,
        // elevation: 2,
        // shadowOffset: { width: 10, height: 10 },
        // shadowColor: "#000",
        // shadowOpacity: 1,
        // elevation: 10,
        // backgroundColor: "#0000",
        marginRight: 20
    },
    notifImage: {
        width: 65,
        height: 65,
        borderRadius: 1
    },
    notifTitle: {
        fontSize: 13,
        color: colors.offWhite
    },
    notifText: {
        color: colors.dark,
        fontSize: 10,
        marginTop: 7
    },
    notifButtonWrap: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 18
    },
    Btn: {
        shadowColor: colors.charcoalGrey24,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 4,
        shadowOpacity: 1,
        elevation: 2
    }
});

export default styles;
