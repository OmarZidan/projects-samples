import React from "react";
import { View, Image } from "react-native";
import styles from "./notifications.styles";
import { Title, Button, Text, Header, BottomNavBar } from "@components";
import { ScrollView } from "react-native-gesture-handler";
import { colors } from "@utils";

export default function Availability(): React.ReactElement {
    return (
        <View style={{ flex: 1 }}>
            <ScrollView style={styles.container}>
                <Header withBack={false} />
                <View style={styles.titleContainer}>
                    <Title fontSize={22} weight="Bold">
                        Notifications
                    </Title>
                </View>
                <View style={styles.content}>
                    {[1, 2, 3, 4].map(item => (
                        <View key={item} style={styles.notification}>
                            <View style={{ flexDirection: "row" }}>
                                <View style={styles.notifImageWrap}>
                                    <Image
                                        style={styles.notifImage}
                                        source={require("@assets/icons/avLogo1.png")}
                                    />
                                </View>
                                <View style={{ flex: 1 }}>
                                    <Text weight="Bold" style={styles.notifTitle}>
                                        CLI approved for Mayar Ali!
                                    </Text>
                                    <Text style={styles.notifText}>
                                        Mayar Ali just got Approved on deal Mountain View ICity -
                                        Apartment
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.notifButtonWrap}>
                                <View style={{ width: "48%" }}>
                                    <Button
                                        text="Message Lead"
                                        backgroundColor={colors.dark}
                                        textColor={colors.white}
                                        weight="SemiBold"
                                        fontSize={10}
                                        containerStyles={styles.Btn}
                                    />
                                </View>
                                <View style={{ width: "48%" }}>
                                    <Button
                                        text="View Lead Profile"
                                        backgroundColor={colors.white}
                                        textColor={colors.dark}
                                        weight="SemiBold"
                                        fontSize={10}
                                        containerStyles={styles.Btn}
                                    />
                                </View>
                            </View>
                        </View>
                    ))}
                </View>
            </ScrollView>
            <BottomNavBar currentRoute="Notifications" />
        </View>
    );
}
