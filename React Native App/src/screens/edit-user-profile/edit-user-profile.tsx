import React, { useState, useEffect } from "react";
import { View, ScrollView, ImageBackground } from "react-native";
import { Text, Header, Button, Title, TextInput, Select, PhoneInput } from "@components";
import styles from "./edit-user-profile.style";
import misritalia from "@config/api";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";
import OTPInputView from "@twotalltotems/react-native-otp-input";

export default function EditUserProfile(): React.ReactElement {
    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={require("@assets/images/filtersBG.png")} style={styles.bgImg}>
                <Header withDrawerMenu={false} />
                <ScrollView>
                    <View style={{ paddingHorizontal: 25 }}>
                        <View style={styles.titleContainer}>
                            <Title fontSize={22} weight="Bold" textTransform="capitalize">
                                Edit Profile
                            </Title>
                        </View>
                        <View style={styles.HLine} />
                        <View style={{ marginHorizontal: 7 }}>
                            <View style={{ marginBottom: 32 }}>
                                <Text weight="SemiBold" style={styles.label}>
                                    Your Name
                                </Text>
                                <TextInput value="Abdelrahman Fathy" />
                            </View>

                            <View style={{ marginBottom: 32 }}>
                                <Text weight="SemiBold" style={styles.label}>
                                    Job Title
                                </Text>
                                <Select
                                    placeholder={{
                                        label: "Senior Real Estate Broker",
                                        vlaue: "Senior Real Estate Broker"
                                    }}
                                    onValueChange={v => console.log(v)}
                                    items={[
                                        { label: "Broker", value: "broker" },
                                        { label: "Sales", value: "sales" }
                                    ]}
                                />
                            </View>

                            <View style={{ marginBottom: 32 }}>
                                <Text weight="SemiBold" style={styles.label}>
                                    Your Email
                                </Text>
                                <TextInput
                                    value="Username@domain.com"
                                    keyboardType="email-address"
                                />
                            </View>

                            <View style={{ marginBottom: 42 }}>
                                <Text weight="SemiBold" style={styles.label}>
                                    Your Phone
                                </Text>
                                <PhoneInput value="+201026733813" />
                            </View>
                            <View style={{ width: "100%" }}>
                                <Button
                                    text="Update Profile"
                                    gradient
                                    fontSize={13}
                                    weight="SemiBold"
                                    textColor={colors.white}
                                    // onPress={() => {
                                    //     setSuccessModalVisible(true);
                                    // }}
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
