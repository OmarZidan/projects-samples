import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    createPasswordContainer: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    titleContainer: {
        marginTop: 150
    },
    bgImg: { width: "100%", height: "100%", opacity: 0.8 },
    HLine: {
        borderBottomColor: colors.charcoalGrey16,
        borderBottomWidth: 1,
        marginVertical: 16
    },
    selectContainer: {
        marginTop: 32
    }
});

export default styles;
