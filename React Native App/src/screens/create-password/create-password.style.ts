import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    createPasswordContainer: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    titleContainer: {
        marginTop: 150
    },
    bgImg: { width: "100%", height: "100%", opacity: 0.8 },
    HLine: {
        borderBottomColor: colors.charcoalGrey16,
        borderBottomWidth: 1,
        marginVertical: 16
    },
    createPasswordText: {
        fontSize: 9.5,
        lineHeight: 16,
        letterSpacing: 1.06,
        color: colors.dark,
        marginBottom: 55
    },
    popModalContainer: { backgroundColor: "transparent", flex: 1 },
    popModal: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignSelf: "center",
        width: "96%",
        backgroundColor: colors.sickGreen,
        position: "absolute",
        top: 5,
        borderRadius: 2,
        paddingHorizontal: 24,
        paddingVertical: 20
    },
    successModalContainer: { backgroundColor: colors.dark90, flex: 1 },
    successModal: {
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%",
        // backgroundColor: colors.white,
        position: "absolute",
        bottom: 0,
        borderRadius: 10
    },
    cardFooterDown: {
        flexDirection: "row",
        marginTop: 75,
        justifyContent: "space-between"
    },

    availabilityButton: {
        shadowColor: colors.charcoalGrey,
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 8,
        borderWidth: 1,
        borderColor: "transparent"
    }
});

export default styles;
