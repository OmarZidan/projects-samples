/* eslint-disable @typescript-eslint/camelcase */
import React, { useState, useEffect, useContext } from "react";
import {
    View,
    ScrollView,
    ImageBackground,
    Modal,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image
} from "react-native";
import { Text, Header, Button, Title, TextInput } from "@components";
import styles from "./create-password.style";
import newPlan from "@config/api";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";
import { AuthContext } from "./../../contexts/AuthContext";

export default function CreatePassword(): React.ReactElement {
    const [user, setUser] = useContext(AuthContext);
    const { navigate } = useNavigation();
    const [successModalVisible, setSuccessModalVisible] = useState(false);
    const [popModalVisible, setPopModalVisible] = useState(true);
    const [form, setForm] = useState({
        password: "",
        password_confirmation: "",
        mobile_number: ""
    });

    const changeForm = (property, newValue) => {
        const newState = { ...form };
        newState[property] = newValue;
        setForm(newState);
    };

    const setPopVisible = visible => {
        setPopModalVisible(visible);
    };

    const setSuccessVisible = visible => {
        setSuccessModalVisible(visible);
    };

    useEffect(() => {
        const timer = setTimeout(() => {
            setPopVisible(false);
        }, 5000);
        return () => clearTimeout(timer);
    }, []);

    const handleSubmit = async () => {
        const AuthStr = "Bearer ".concat(user.access_token);
        form.mobile_number = user.mobile_number;

        try {
            const response = await newPlan.post("/password/change", form, {
                headers: { Authorization: AuthStr }
            });
            // setUser(response.data.data);
            // const firstTime = response.data.data.first_time_login;
            // if (firstTime === 0) {
            //     navigate("Sales");
            // } else if (firstTime === 1) {
            //     navigate("CreatePassword");
            // }
            console.log(response.data);
        } catch (error) {
            console.log(error.response.data.error.message);
            const statusCode = error.response.status;
            if (statusCode === 401) {
                alert(error.response.data.error.message);
            } else if (statusCode === 422) {
                alert(error.response.data.error.message.password);
            }
        }
    };

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={require("@assets/images/filtersBG.png")} style={styles.bgImg}>
                <Header withBack={false} withDrawerMenu={false} />
                <ScrollView>
                    <View style={{ paddingHorizontal: 25 }}>
                        <View style={styles.titleContainer}>
                            <Title fontSize={22} weight="Bold" textTransform="capitalize">
                                Create Password
                            </Title>
                        </View>
                        <View style={styles.HLine} />
                        <Text weight="Regular" style={styles.createPasswordText}>
                            Your password should be 6 characters long, with at least, a Uppercase &
                            a lowercase letter, a number, and a symbol
                        </Text>

                        <TextInput
                            placeholder="New Password"
                            secureTextEntry
                            placeholderColor={colors.charcoalGrey}
                            style={{ marginBottom: 32 }}
                            value={form.password}
                            onChangeText={val => changeForm("password", val)}
                        />
                        <TextInput
                            placeholder="Re-Enter Password"
                            secureTextEntry
                            placeholderColor={colors.charcoalGrey}
                            value={form.password_confirmation}
                            onChangeText={val => changeForm("password_confirmation", val)}
                        />
                        <View style={{ marginHorizontal: 50, marginTop: 100 }}>
                            <Button
                                text="Confirm Password"
                                gradient
                                fontSize={13}
                                weight="SemiBold"
                                textColor={colors.white}
                                onPress={handleSubmit}
                                // disabled={!form.password || !form.password_confirmation}
                            />
                        </View>
                    </View>

                    <View>
                        <Modal animationType="fade" transparent={true} visible={popModalVisible}>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    setPopVisible(!popModalVisible);
                                }}
                            >
                                <View style={styles.popModalContainer}>
                                    <TouchableWithoutFeedback>
                                        <View style={styles.popModal}>
                                            <View>
                                                <Text weight="Medium" style={{ fontSize: 17 }}>
                                                    Successful log in!
                                                </Text>
                                                <Text weight="Regular" style={{ fontSize: 12 }}>
                                                    Make sure to change your Password here
                                                </Text>
                                            </View>
                                            <View>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        setPopVisible(false);
                                                    }}
                                                >
                                                    <Image
                                                        source={require("@assets/icons/close.png")}
                                                        style={{ height: 17, width: 17 }}
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>
                    </View>

                    <View>
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={successModalVisible}
                        >
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    setSuccessVisible(!successModalVisible);
                                }}
                            >
                                <View style={styles.successModalContainer}>
                                    <TouchableWithoutFeedback>
                                        <View style={styles.successModal}>
                                            <Image
                                                source={require("@assets/icons/padlock.png")}
                                                style={{ height: 192, width: 140 }}
                                            />
                                            <Text
                                                weight="Bold"
                                                style={{
                                                    fontSize: 32,
                                                    letterSpacing: 1,
                                                    color: colors.white,
                                                    textAlign: "center",
                                                    marginTop: 40
                                                }}
                                            >
                                                SUCCESS!
                                            </Text>
                                            <Text
                                                weight="Regular"
                                                style={{
                                                    fontSize: 14,
                                                    letterSpacing: 0,
                                                    lineHeight: 20,
                                                    color: colors.white,
                                                    textAlign: "center",
                                                    marginTop: 14,
                                                    marginHorizontal: 35
                                                }}
                                            >
                                                Your password is all set! You can always change it
                                                from your profile.
                                            </Text>
                                            <View style={styles.cardFooterDown}>
                                                <View style={{ width: "43%", marginHorizontal: 5 }}>
                                                    <Button
                                                        text="Invite Brokers"
                                                        backgroundColor={colors.white}
                                                        textColor={colors.offWhite}
                                                        fontSize={11}
                                                        borderWidth={1}
                                                        borderColor={colors.white}
                                                        containerStyles={styles.availabilityButton}
                                                    />
                                                </View>
                                                <View style={{ width: "43%", marginHorizontal: 5 }}>
                                                    <Button
                                                        text="Back to Dashboard"
                                                        backgroundColor="transparent"
                                                        borderColor={colors.white}
                                                        textColor={colors.white}
                                                        fontSize={11}
                                                        borderWidth={1}
                                                        containerStyles={styles.availabilityButton}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
