import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    titleContainer: {
        marginTop: 150,
        marginBottom: 24,
        marginHorizontal: 25
    },
    leadsContainer: {
        marginTop: 28,
        marginHorizontal: 32,
        marginBottom: 85
    }
});

export default styles;
