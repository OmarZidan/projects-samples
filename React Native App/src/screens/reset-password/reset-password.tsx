import React, { useState, useEffect } from "react";
import { View, ScrollView, ImageBackground } from "react-native";
import { Text, Header, Button, Title, TextInput } from "@components";
import styles from "./reset-password.style";
import misritalia from "@config/api";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";

export default function ResetPassword(): React.ReactElement {
    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={require("@assets/images/filtersBG.png")} style={styles.bgImg}>
                <Header withBack withDrawerMenu={false} />
                <ScrollView>
                    <View style={{ paddingHorizontal: 25 }}>
                        <View style={styles.titleContainer}>
                            <Title fontSize={22} weight="Bold" textTransform="capitalize">
                                Reset Password
                            </Title>
                        </View>
                        <View style={styles.HLine} />
                        <Text weight="Regular" style={styles.createPasswordText}>
                            Your password should be 8 characters long, with at least, a Uppercase &
                            a lowercase letter, a number, and a symbol
                        </Text>

                        <TextInput
                            placeholder="New Password"
                            secureTextEntry
                            placeholderColor={colors.charcoalGrey}
                            style={{ marginBottom: 32 }}
                        />
                        <TextInput
                            placeholder="Confirm New Password"
                            secureTextEntry
                            placeholderColor={colors.charcoalGrey}
                        />
                        <View style={{ marginHorizontal: 50, marginTop: 100 }}>
                            <Button
                                text="Confirm Password"
                                gradient
                                fontSize={13}
                                weight="SemiBold"
                                textColor={colors.white}
                            />
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
