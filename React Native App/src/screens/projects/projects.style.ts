import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    titleContainer: {
        marginTop: 150,
        marginBottom: 28,
        marginHorizontal: 25
    }
});

export default styles;
