import React, { useState, useEffect } from "react";
import { View, ScrollView, ActivityIndicator } from "react-native";
import styles from "./projects.style";
import { Title, BottomNavBar, ProjectList, SearchboxAndFilters, Header } from "@components";
import { useNavigation } from "react-navigation-hooks";
import newPlan from "@config/api";
import { colors } from "@utils";

export default function Projects(): React.ReactElement {
    const [projectsList, setProjectsList] = useState([]);
    const [loading, setLoading] = useState(true);
    const navigation = useNavigation();

    const projectsData = async () => {
        try {
            const response = await newPlan.get("/projects?developer=7");
            setProjectsList(response.data.data);
            setLoading(false);
            console.log(response.data.data);
        } catch (error) {
            console.log(error.response);
        }
    };

    useEffect(() => {
        projectsData();
    }, []);

    return (
        <View style={{ flex: 1 }}>
            <ScrollView style={styles.container}>
                <Header withBack={true} withDrawerMenu={false} />
                <View style={styles.titleContainer}>
                    <Title fontSize={24} weight="ExtraBold">
                        PROJECTS
                    </Title>
                </View>
                <SearchboxAndFilters
                    onPress={() => {
                        navigation.navigate("ProjectsFilters");
                    }}
                />
                {loading && (
                    <View style={{ marginTop: 100 }}>
                        <ActivityIndicator size="large" color={colors.offWhite} />
                    </View>
                )}

                <View>
                    <ProjectList projects={projectsList} navigation={navigation} />
                </View>
            </ScrollView>
            {/* <BottomNavBar /> */}
        </View>
    );
}
