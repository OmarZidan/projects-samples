import React from "react";
import { Text, View, Image, Share } from "react-native";
import styles from "./singleAvUnit.style";
import { Button, Header, BottomNavBar } from "@components";
import { ScrollView } from "react-native-gesture-handler";
import DataWithLogo from "./data-with-logo";
import { colors } from "@utils";

export default function singleAvUnit(): React.ReactElement {
    const onShare = async () => {
        try {
            await Share.share({
                message: "Twin Villa | NewPlan Developments",
                url: "https://google.com"
            });
        } catch (error) {
            alert(error.message);
        }
    };

    return (
        <View style={{ flex: 1 }}>
            <ScrollView
                style={styles.container}
                contentContainerStyle={{
                    alignItems: "center",
                    justifyContent: "center"
                }}
            >
                <Header withDrawerMenu={false} />

                <View style={styles.singleUnit}>
                    <View style={styles.unitOwnerLogo}>
                        <Image
                            style={styles.unitOwnerLogoImage}
                            source={require("@assets/icons/avLogo1.png")}
                        />
                    </View>
                    <View style={styles.unitDetails}>
                        <View style={{ marginBottom: 5 }}>
                            <Text style={styles.unitTitle}>Twin Villa</Text>
                        </View>
                        <View style={{ marginBottom: 5 }}>
                            <Text style={styles.unitSupplier}>NewPlan Developments</Text>
                        </View>
                        <DataWithLogo
                            iconPath={require("@assets/icons/mapPlaceholder.png")}
                            dataText={"NewCapital, Cairo"}
                        />
                        <Button
                            text="Share"
                            backgroundColor="#fff"
                            containerStyles={styles.unitButton}
                            onPress={onShare}
                        />
                    </View>
                    <View style={styles.unitAboutCont}>
                        <View style={styles.unitAbout}>
                            <View style={styles.unitAboutTitleCont}>
                                <Text style={styles.unitAboutTitle}>About Unit</Text>
                            </View>

                            <View style={styles.unitRow}>
                                <View style={styles.unitCell}>
                                    <Text style={styles.unitCellTitle}>Unit No.:</Text>
                                    <Text style={styles.unitCellData}>T - 312</Text>
                                </View>
                                <View style={styles.unitCell}>
                                    <Text style={styles.unitCellTitle}>Floor No.:</Text>
                                    <Text style={styles.unitCellData}>N/A</Text>
                                </View>
                                <View style={styles.unitCell}>
                                    <Text style={styles.unitCellTitle}>Unit No.:</Text>
                                    <Text
                                        style={[
                                            styles.unitStateColored,
                                            { color: colors.sickGreen }
                                        ]}
                                    >
                                        Ready to Move
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.unitRow}>
                                <View style={styles.unitCell}>
                                    <Text style={styles.unitCellTitle}>Land Area:</Text>
                                    <Text style={styles.unitCellData}>
                                        {/* {"" + areaInMeterSquare + String.fromCharCode(0x33a1)} */}
                                        1000 {String.fromCharCode(0x33a1)}
                                    </Text>
                                </View>
                                <View style={styles.unitCell}>
                                    <Text style={styles.unitCellTitle}>Roof Area:</Text>
                                    <Text style={styles.unitCellData}>
                                        800 {String.fromCharCode(0x33a1)}
                                    </Text>
                                </View>
                                <View style={styles.unitCell}>
                                    <Text style={styles.unitCellTitle}>Floorplan Type:</Text>
                                    <Text style={[styles.unitCellData]}>TP - 660A</Text>
                                </View>
                            </View>
                            <View style={styles.unitRow}>
                                <View style={styles.unitCell}>
                                    <Text style={styles.unitCellTitle}>Rooms No.:</Text>
                                    <Text style={styles.unitCellData}>5 Rooms</Text>
                                </View>
                                <View style={styles.unitCell}>
                                    <Text style={styles.unitCellTitle}>Bathrooms No.:</Text>
                                    <Text style={styles.unitCellData}>4 Rooms</Text>
                                </View>
                                <View style={styles.unitCell}>
                                    <Text style={styles.unitCellTitle}>Finishing:</Text>
                                    <Text style={[styles.unitStateColored]}>Furnished</Text>
                                </View>
                            </View>
                            <View style={styles.unitRow}>
                                <View style={styles.unitCell}>
                                    <Text style={styles.unitCellTitle}>Built-up Area (BUA):</Text>
                                    <Text style={styles.unitCellData}>
                                        874 {String.fromCharCode(0x33a1)}
                                    </Text>
                                </View>
                            </View>

                            <View style={styles.unitInsideCard}>
                                <View style={styles.insideCardLeft}>
                                    <Text style={styles.leftText}>Payment Details</Text>
                                </View>
                                <View style={styles.insideCardRight}>
                                    <View style={{ marginBottom: 10 }}>
                                        <Text style={styles.unitCellTitle}>Unit Price:</Text>
                                        <Text
                                            style={[
                                                styles.cardRightData,
                                                { fontFamily: "Gotham-Bold" }
                                            ]}
                                        >
                                            EGP 23,000,000
                                        </Text>
                                    </View>
                                    <View>
                                        <Text style={styles.unitCellTitle}>
                                            Payment Plan Duration
                                        </Text>
                                        <Text
                                            style={[
                                                styles.cardRightData,
                                                { fontFamily: "Gotham-Regular" }
                                            ]}
                                        >
                                            6 - 15 years
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        {/* Edit Button Font family to be semibold */}
                        <Button
                            text="Add Lead"
                            backgroundColor={colors.dark}
                            textColor="white"
                            weight="SemiBold"
                            containerStyles={styles.unitButton}
                        />
                    </View>
                </View>
            </ScrollView>
            <BottomNavBar currentRoute="Availability" />
        </View>
    );
}
