import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.lightBlueGrey,
        flex: 1
    },
    singleUnit: {
        marginVertical: 150,
        // width: "100%",
        marginHorizontal: 25,
        paddingHorizontal: 7,
        paddingVertical: 25,
        backgroundColor: colors.white50,
        alignItems: "center"
    },
    unitOwnerLogoImage: { width: "100%", height: "100%", resizeMode: "cover" },
    unitOwnerLogo: {
        shadowColor: "rgb(50,56,65)",
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowOpacity: 0.25,
        shadowRadius: 8,
        elevation: 8,
        width: 80,
        height: 80,
        borderWidth: 1,
        borderColor: "transparent"
    },
    unitDetails: {
        // borderWidth: 1,
        // borderColor: "fuchsia",
        margin: 15,
        width: "100%",
        borderBottomColor: colors.charcoalGrey24,
        borderBottomWidth: 1,
        paddingBottom: 25
    },
    unitSupplier: {
        color: colors.offWhite,
        borderRadius: 1,
        // width: "100%",
        fontFamily: "Gotham-Regular",
        fontSize: 10,
        textAlign: "center"
    },
    unitTitle: {
        fontFamily: "Gotham-Bold",
        fontSize: 15,
        color: colors.dark,
        textAlign: "center"
        // marginBottom: 5
    },
    unitAboutCont: {
        // borderWidth: 1,
        // borderColor: "purple"
        width: "100%"
    },
    unitAbout: {
        // borderWidth: 1,
        // borderColor: "red",
        width: "100%",
        backgroundColor: "white",
        padding: 12
    },
    unitAboutTitleCont: {
        borderBottomWidth: 1,
        borderBottomColor: colors.lightBlueGrey
    },
    unitAboutTitle: {
        fontSize: 12,
        fontFamily: "Gotham-Medium",
        color: colors.dark
    },
    unitRow: {
        // borderWidth: 1,
        // borderColor: "yellowgreen",
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingVertical: 10
    },
    unitCell: {
        // borderWidth: 1,
        // borderColor: "blue",
        flexBasis: "33%",
        padding: 1,
        flexGrow: 1
    },
    unitCellTitle: {
        fontSize: 10,
        fontFamily: "Gotham-Light",
        color: colors.charcoalGrey
    },
    unitCellData: {
        fontSize: 10,
        fontFamily: "Gotham-Regular",
        color: colors.charcoalGrey
    },
    unitStateColored: {
        fontSize: 10,
        fontFamily: "Gotham-Bold",
        color: colors.charcoalGrey
    },
    unitInsideCard: {
        marginTop: 15,
        padding: 20,
        shadowColor: colors.charcoalGrey,
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 8,
        borderWidth: 1,
        borderColor: "transparent",
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "white"
    },
    insideCardLeft: {
        // padding: 10,
        flexBasis: "50%"
    },
    leftText: {
        textAlign: "center",
        fontSize: 12,
        fontFamily: "Gotham-Medium",
        color: colors.dark
    },
    insideCardRight: {
        borderLeftColor: colors.blueyGrey32,
        borderLeftWidth: 1,
        paddingLeft: 10,
        flexBasis: "50%"
    },
    cardRightData: {
        fontSize: 11,
        color: colors.dark
    },
    unitButton: {
        // width: "48%",
        shadowColor: colors.dark,
        shadowOffset: {
            width: 0,
            height: 8
        },
        shadowRadius: 16,
        shadowOpacity: 0.6,
        elevation: 1,
        borderWidth: 1,
        borderColor: "transparent",
        marginTop: 25,
        fontSize: 13,
        fontFamily: "Gotham-SemiBold"
    }
});

export default styles;
