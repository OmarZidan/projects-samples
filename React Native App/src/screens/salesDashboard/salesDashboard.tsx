import React from "react";
import { Text, View, ScrollView } from "react-native";
import styles from "./salesDashboard.style";
import { Title, Button, BottomNavBar, Header } from "@components";
import { LinearGradient } from "expo-linear-gradient";
import { colors } from "@utils";

export default function SalesDashboard(): React.ReactElement {
    return (
        <View style={{ flex: 1 }}>
            <ScrollView style={styles.container}>
                <Header withBack={false} />

                <View style={styles.titleContainer}>
                    <Title fontSize={22} textTransform="capitalize" weight="Bold">
                        Hello, Dina
                    </Title>
                </View>
                <View>
                    <LinearGradient
                        colors={[colors.offWhite, colors.beige]}
                        style={styles.card}
                        start={{ x: 0, y: 0.5 }}
                        end={{ x: 1, y: 0.5 }}
                    >
                        <Text style={styles.currency}>EGP</Text>
                        <Text style={styles.amount}>175,986.32</Text>
                        <View style={styles.volumeContainer}>
                            <Text style={styles.volumeText}>Sales Volume</Text>
                        </View>
                    </LinearGradient>
                    <View style={styles.performanceContainer}>
                        <Text style={styles.performanceTitle}>Performance Overview</Text>
                        <View
                            style={{
                                flexDirection: "row",
                                justifyContent: "space-between",
                                marginBottom: 25
                            }}
                        >
                            <View style={styles.box}>
                                <Text style={[styles.boxUnit, { color: colors.sickGreen }]}>
                                    38
                                </Text>
                                <Text style={styles.boxText}>Won{"\n"}Deals</Text>
                            </View>
                            <View style={styles.box}>
                                <Text style={[styles.boxUnit, { color: colors.offWhite }]}>12</Text>
                                <Text style={styles.boxText}>Follow - Ups</Text>
                            </View>
                            <View style={styles.box}>
                                <Text style={[styles.boxUnit, { color: colors.redWine }]}>8</Text>
                                <Text style={styles.boxText}>Lost{"\n"}Deals</Text>
                            </View>
                        </View>
                        <View>
                            <Button
                                text="Check Commissions Dates"
                                backgroundColor={colors.dark}
                                textColor={colors.white}
                                containerStyles={styles.checkBtn}
                                fontSize={11}
                            />
                            <Button
                                text="Invite Brokers"
                                backgroundColor={colors.white}
                                textColor={colors.offWhite}
                                containerStyles={styles.checkBtn}
                                fontSize={11}
                            />
                        </View>
                    </View>
                </View>
            </ScrollView>
            <BottomNavBar currentRoute="brokerDashboard" />
        </View>
    );
}
