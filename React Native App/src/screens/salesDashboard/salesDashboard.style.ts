import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    titleContainer: {
        marginTop: 150,
        marginBottom: 42,
        marginHorizontal: 25
    },
    card: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 2,
        height: 115,
        flexDirection: "row",
        position: "relative",
        marginHorizontal: 25,
        shadowOpacity: 0.75,
        shadowRadius: 2,
        shadowColor: colors.charcoalGrey,
        shadowOffset: { height: 4, width: 0 },
        elevation: 2
    },
    currency: {
        color: colors.white,
        fontSize: 14,
        letterSpacing: 0.77,
        marginRight: 6,
        fontFamily: "Gotham-Regular"
    },
    amount: {
        color: colors.white,
        fontSize: 32,
        letterSpacing: 1.75,
        fontFamily: "Gotham-Bold"
    },
    volumeContainer: {
        backgroundColor: colors.white,
        position: "absolute",
        left: -10,
        top: -10
    },
    volumeText: {
        fontSize: 12,
        paddingHorizontal: 21,
        paddingVertical: 5,
        fontFamily: "Gotham-Medium"
    },
    performanceContainer: {
        backgroundColor: colors.white50,
        marginHorizontal: 35,
        paddingHorizontal: 15
    },
    performanceTitle: {
        fontSize: 12,
        color: colors.dark,
        marginTop: 26,
        marginBottom: 17,
        fontFamily: "Gotham-Medium"
    },
    box: {
        backgroundColor: colors.white,
        borderRadius: 2,
        height: 84,
        width: 82,
        justifyContent: "center",
        alignItems: "center",
        padding: 14,
        shadowOpacity: 0.75,
        shadowRadius: 2,
        shadowColor: colors.blueyGrey,
        shadowOffset: { height: 8, width: 0 },
        elevation: 2
    },
    boxUnit: {
        fontSize: 32,
        fontFamily: "Gotham-Bold"
    },
    boxText: {
        color: colors.dark,
        fontSize: 9,
        letterSpacing: 0.42,
        textAlign: "center",
        fontFamily: "Gotham-Medium"
    },
    checkBtn: {
        marginBottom: 13
    }
});

export default styles;
