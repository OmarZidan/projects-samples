import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    updatesContainer: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    listContainer: {
        marginTop: 160,
        paddingHorizontal: 25
    },
    listItem: {
        marginBottom: 25
    },
    // itemImage: {
    //     width: "100%",
    //     height: 220,
    //     position: "relative"
    // },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: "rgba(0,0,0,0.3)"
    },
    itemContent: {
        paddingTop: 40,
        paddingLeft: 40,
        paddingBottom: 10
    },
    exploreBtn: {
        marginTop: 40
    },
    // itemTitle: {
    //     color: "#fff",
    //     fontSize: 17,
    //     lineHeight: 23,
    //     letterSpacing: 0.09,
    //     marginBottom: 10
    // },
    // itemDate: {
    //     color: "#fff",
    //     fontSize: 11,
    //     letterSpacing: 1
    // },
    itemAvatar: {
        width: 32,
        height: 32,
        borderRadius: 32 / 2,
        marginRight: 10
    },
    itemAuthor: {
        color: "#fff",
        fontSize: 8,
        letterSpacing: 0.85
    },
    moreContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 35,
        marginBottom: 55
    },
    moreBtn: {
        minWidth: 180,
        minHeight: 42,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        borderWidth: 1,
        borderColor: colors.dark,
        backgroundColor: "transparent"
    },
    itemBox: {
        backgroundColor: colors.white,
        height: 135,
        marginBottom: 32,
        flexDirection: "row"
    },
    itemImage: {
        width: 115,
        height: "100%"
    },
    itemDate: {
        fontFamily: "Gotham-Light",
        fontSize: 13,
        letterSpacing: 0
    },
    itemTitle: {
        fontFamily: "Gotham-Bold",
        fontSize: 13,
        letterSpacing: 0
    },
    btnContainer: {
        marginTop: 25,
        maxWidth: 125,
        width: 125
    }
});

export default styles;
