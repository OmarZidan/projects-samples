import React, { useState, useEffect } from "react";
import { View, FlatList, ScrollView, Image, ActivityIndicator } from "react-native";
import { Text, Header, Button, BottomNavBar } from "@components";
import styles from "./construction-updates.style";
import newPlan from "@config/api";
import { getAssetUrl, colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";

export default function ConstructionUpdates(): React.ReactElement {
    const [updates, setUpdates] = useState([]);
    const [next, setNext] = useState(null);
    const { navigate } = useNavigation();
    const [loading, setLoading] = useState(true);

    const loadPosts = (page = 1) => {
        newPlan
            .get(`/construction-updates/7?page=${page}`)
            // {server_url}/api/construction-updates?project=6
            .then(response => {
                console.log(response.data);
                setUpdates(response.data.data);
                setLoading(false);
                // setNext(response.data.paginator.next_page);
            })
            .catch(err => console.log("ERROR: ", err));
    };
    useEffect(() => {
        loadPosts();
        // setLoading(false);
    }, []);

    return (
        <View style={styles.updatesContainer}>
            <ScrollView>
                <Header withDrawerMenu={false} withBack={true} />
                {loading && (
                    <View style={{ marginTop: 100 }}>
                        <ActivityIndicator size="large" color={colors.offWhite} />
                    </View>
                )}
                <View>
                    <FlatList
                        contentContainerStyle={styles.listContainer}
                        data={updates}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => {
                            return (
                                <View key={item.date} style={styles.itemBox}>
                                    <Image
                                        style={styles.itemImage}
                                        source={{ uri: getAssetUrl(item.image) }}
                                    />
                                    <View style={{ padding: 16 }}>
                                        <Text style={styles.itemTitle}>{item.title}</Text>
                                        <Text style={styles.itemDate}>{item.date}</Text>
                                        <View style={styles.btnContainer}>
                                            <Button
                                                // containerStyles={{ width: "100%" }}
                                                onPress={() =>
                                                    navigate("SingleConstructionUpdates", {
                                                        update: item
                                                    })
                                                }
                                                text="View Details"
                                                weight="SemiBold"
                                                fontSize={10}
                                                textColor={colors.white}
                                                backgroundColor={colors.dark}
                                            />
                                        </View>
                                    </View>
                                </View>
                            );
                        }}
                    />
                </View>
                <View style={styles.moreContainer}>
                    {next && (
                        <Button
                            backgroundColor={colors.dark}
                            textColor={colors.white}
                            text="Load More"
                            onPress={() => this.loadPosts(next)}
                            containerStyles={styles.moreBtn}
                            fontSize={13}
                            weight="ExtraBold"
                        />
                    )}
                </View>
            </ScrollView>
            {/* <BottomNavBar /> */}
        </View>
    );
}
