import React, { useState, useEffect } from "react";
import { View, ScrollView, ImageBackground, Image } from "react-native";
import { Text, Header, Button, Title, TextInput, Select, PhoneInput } from "@components";
import styles from "./press-filters.style";
import misritalia from "@config/api";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";
import { TouchableOpacity } from "react-native-gesture-handler";

export default function PressFilters(): React.ReactElement {
    const [fromDate, setFromDate] = useState<Date | null>(new Date("1/1/20"));
    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={require("@assets/images/filtersBG.png")} style={styles.bgImg}>
                <Header withDrawerMenu={false} />
                <ScrollView>
                    <View style={{ paddingHorizontal: 25 }}>
                        <View style={styles.titleContainer}>
                            <Title fontSize={22} weight="Bold" textTransform="capitalize">
                                Filter By
                            </Title>
                        </View>
                        <View style={styles.HLine} />
                        <View style={{ marginHorizontal: 7 }}>
                            <View style={{ width: "100%" }}>
                                <Text
                                    weight="Medium"
                                    style={{
                                        fontSize: 12,
                                        lineHeight: 17,
                                        letterSpacing: 0.6,
                                        marginBottom: 16,
                                        marginTop: 70
                                    }}
                                >
                                    Date Range
                                </Text>
                                <View
                                    style={{
                                        justifyContent: "space-between",
                                        flexDirection: "row"
                                    }}
                                >
                                    <TextInput
                                        date
                                        onDateChange={date => date && setFromDate(date)}
                                        icon={require("@assets/icons/schedule.png")}
                                        value={fromDate ? fromDate.toDateString() : ""}
                                        style={{ width: "45%" }}
                                    />
                                    <View
                                        style={{
                                            borderLeftWidth: 1,
                                            borderLeftColor: colors.charcoalGrey16,
                                            marginVertical: 5
                                        }}
                                    />
                                    <TextInput
                                        date
                                        onDateChange={date => date && setFromDate(date)}
                                        icon={require("@assets/icons/schedule.png")}
                                        value={fromDate ? fromDate.toDateString() : ""}
                                        style={{ width: "45%" }}
                                    />
                                </View>
                                <View style={{ marginTop: 32 }}>
                                    <Select
                                        placeholder={{
                                            label: "Select Preferred Project",
                                            vlaue: null
                                        }}
                                        onValueChange={v => console.log(v)}
                                        items={[
                                            { label: "First", value: "first" },
                                            { label: "Second", value: "second" }
                                        ]}
                                    />
                                </View>

                                <View style={{ marginTop: 120 }}>
                                    <Button
                                        text="Apply Filters"
                                        gradient
                                        fontSize={13}
                                        weight="SemiBold"
                                        textColor={colors.white}
                                        // onPress={() => {
                                        //     setSuccessModalVisible(true);
                                        // }}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
