import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    createPasswordContainer: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    titleContainer: {
        marginTop: 150
    },
    bgImg: { width: "100%", height: "100%", opacity: 0.8 },
    HLine: {
        borderBottomColor: colors.charcoalGrey16,
        borderBottomWidth: 1,
        marginVertical: 16
    },
    label: {
        fontSize: 12,
        color: colors.offWhite,
        letterSpacing: 0,
        marginBottom: 16
    },
    popModalContainer: { backgroundColor: "transparent", flex: 1 },
    popModal: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignSelf: "center",
        width: "96%",
        backgroundColor: colors.sickGreen,
        position: "absolute",
        top: 5,
        borderRadius: 2,
        paddingHorizontal: 24,
        paddingVertical: 20
    }
});

export default styles;
