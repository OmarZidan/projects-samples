import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    conceptBackground: {
        flex: 1,
        marginTop: 140,
        paddingTop: 30
    },
    goldDots: {
        height: 240,
        width: 240,
        position: "absolute",
        marginLeft: 25,
        resizeMode: "contain"
    },
    articleContent: {
        marginHorizontal: 25,
        marginTop: 45,
        marginBottom: 120
    },
    galleryContainer: { alignSelf: "flex-end", elevation: 8, zIndex: 999 },
    articleText: { fontSize: 12, letterSpacing: 0.17, textAlign: "justify" },
    articleTitle: {
        fontSize: 16,
        letterSpacing: 0.55,
        color: colors.offWhite,
        marginBottom: 15
    }
});

export default styles;
