import React, { useState, useEffect } from "react";
import { View, ScrollView, Image } from "react-native";
import { useNavigation } from "react-navigation-hooks";
import styles from "./singlePressArticle.style";
import { Header, Text, BottomNavBar } from "@components";
import { getAssetUrl } from "@utils";

interface SinglePressArticleProps {
    navigation: any;
}

export default function SinglePressArticle({
    navigation
}: SinglePressArticleProps): React.ReactElement {
    const [activeSlide, setActiveSlide] = useState(0);
    const [item, setItem] = useState({
        title: "",
        description: "",
        image: ""
    });
    const { navigate } = useNavigation();

    useEffect(() => {
        const item = navigation.getParam("item");
        console.log(item);
        setItem(item);
    }, []);

    return (
        <View style={{ flex: 1 }}>
            <ScrollView style={styles.container}>
                <Header withBack={true} withDrawerMenu={false} />
                <View style={styles.conceptBackground}>
                    <View style={styles.galleryContainer}>
                        <Image
                            source={{ uri: getAssetUrl(item.image) }}
                            style={{ height: 250, width: 315 }}
                        />
                    </View>
                    <Image
                        style={styles.goldDots}
                        source={require("@assets/images/goldDots.png")}
                    />
                </View>
                <View style={styles.articleContent}>
                    <Text style={styles.articleTitle} weight="Bold">
                        {item.title}
                    </Text>
                    <Text weight="Light" style={styles.articleText}>
                        {item.description}
                    </Text>
                </View>
            </ScrollView>
            {/* <BottomNavBar /> */}
        </View>
    );
}
