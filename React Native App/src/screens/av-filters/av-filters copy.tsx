import React from "react";
import { Text, View, TouchableOpacity, Image, FlatList } from "react-native";
import styles from "./av-filters.style";
// import AvailabilityHeader from "./NOTUSEDavailability-header";
// import DataWithLogo from "./data-with-logo";
import { Title, Button, SearchboxAndFilters, TextInput, Select, RangeSlider } from "@components";
import { ScrollView } from "react-native-gesture-handler";
import { useNavigation } from "react-navigation-hooks";

export default function avFilters(props): React.ReactElement {
    const { navigate } = useNavigation();
    return (
        <ScrollView
            style={styles.container}
            contentContainerStyle={{
                alignItems: "center",
                justifyContent: "center"
            }}
        >
            <View style={styles.titleContainer}>
                <Title>Filter By</Title>
            </View>
        </ScrollView>
    );
}
