import React, { useState } from "react";
import { Text, View, ImageBackground, Dimensions } from "react-native";
import styles from "./av-filters.style";
import { Button, TextInput, Select, RangeSlider, CheckBox, Header } from "@components";
import { ScrollView } from "react-native-gesture-handler";
import { useNavigation } from "react-navigation-hooks";
import { colors } from "@utils";

const meterSquaredSymbol = String.fromCharCode(0x33a1);
const SCREEN_WIDTH = Dimensions.get("screen").width;
export default function avFilters(props): React.ReactElement {
    const { navigate } = useNavigation();
    const [gardenOption, setGardenOption] = useState(false);
    const [garageOption, setGarageOption] = useState(false);
    const [terraceOption, setTerraceOption] = useState(false);
    const [poolOption, setPoolOption] = useState(false);
    const [insuranceOption, setInsuranceOption] = useState(false);
    return (
        <View style={{ flex: 1 }}>
            <ScrollView
                style={styles.container}
                contentContainerStyle={{
                    alignItems: "center",
                    justifyContent: "center"
                }}
            >
                <ImageBackground
                    source={require("@assets/images/filtersBkg.png")}
                    style={styles.filtersBackground}
                >
                    <Header withBack />

                    <View style={styles.screenWithBackgroundCont}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.filtersTitle}>Filter By</Text>
                        </View>
                        <View style={styles.filtersCont}>
                            <View style={styles.textInputContainer}>
                                <Select
                                    placeholder={{ label: "Select Location", vlaue: null }}
                                    onValueChange={v => console.log(v)}
                                    items={[
                                        { label: "Cairo", value: "cairo" },
                                        { label: "Giza", value: "giza" }
                                    ]}
                                />
                            </View>
                            <Select
                                placeholder={{ label: "Select Project Category", vlaue: null }}
                                onValueChange={v => console.log(v)}
                                items={[
                                    { label: "Residential", value: "residential" },
                                    { label: "Commercial", value: "commercial" }
                                ]}
                            />

                            <View style={styles.textInputContainer}>
                                <Select
                                    placeholder={{ label: "Select Preferred Project", vlaue: null }}
                                    onValueChange={v => console.log(v)}
                                    items={[
                                        { label: "DownTown Cairo", value: "downtowncairo" },
                                        { label: "New Cairo", value: "newcairo" }
                                    ]}
                                />
                            </View>
                            <View>
                                <Select
                                    placeholder={{ label: "Select Unit Type", vlaue: null }}
                                    onValueChange={v => console.log(v)}
                                    items={[
                                        { label: "Residential", value: "residential" },
                                        { label: "Commercial", value: "commercial" }
                                    ]}
                                />
                            </View>

                            <View style={[styles.filterDetails]}>
                                <Text style={styles.filterDetailsTitle}>Built-Up Area (BUA)</Text>
                                <Text style={styles.filterDetailsData}>
                                    120 {meterSquaredSymbol} - 1000
                                    {meterSquaredSymbol}
                                </Text>
                            </View>

                            <View
                                style={{
                                    alignItems: "center",
                                    marginBottom: 10
                                }}
                            >
                                <RangeSlider
                                    sliderLength={SCREEN_WIDTH - 80}
                                    min={250}
                                    max={1000000}
                                    step={10000}
                                    snapped
                                    formatNumber
                                    values={[250000, 1000000]}
                                />
                            </View>

                            <View>
                                <Select
                                    placeholder={{
                                        label: "Select Preferred Floor Number",
                                        vlaue: null
                                    }}
                                    onValueChange={v => console.log(v)}
                                    items={[
                                        { label: "First", value: "first" },
                                        { label: "Second", value: "second" }
                                    ]}
                                />
                            </View>

                            <View style={styles.textInputContainer}>
                                <TextInput
                                    placeholder="Number of Rooms"
                                    height={45}
                                    style={{
                                        flex: 1
                                    }}
                                    icon={require("@assets/icons/doubleBed.png")}
                                />
                            </View>
                            <View>
                                <TextInput
                                    placeholder="Number of Bathrooms"
                                    height={45}
                                    style={{
                                        flex: 1
                                    }}
                                    icon={require("@assets/icons/bathtub.png")}
                                />
                            </View>
                            <View
                                style={{
                                    flexDirection: "row",
                                    // alignItems: "center",
                                    justifyContent: "flex-start",
                                    flexWrap: "wrap"
                                }}
                            >
                                <CheckBox
                                    checked={gardenOption}
                                    toggleCheck={() => setGardenOption(!gardenOption)}
                                    label="Garden"
                                />
                                <CheckBox
                                    checked={garageOption}
                                    toggleCheck={() => setGarageOption(!garageOption)}
                                    label="Garage"
                                />
                                <CheckBox
                                    checked={terraceOption}
                                    toggleCheck={() => setTerraceOption(!terraceOption)}
                                    label="Terrace"
                                />
                                <CheckBox
                                    checked={poolOption}
                                    toggleCheck={() => setPoolOption(!poolOption)}
                                    label="Pool"
                                />
                                <CheckBox
                                    checked={insuranceOption}
                                    toggleCheck={() => setInsuranceOption(!insuranceOption)}
                                    label="Insurance"
                                />
                            </View>
                            <View style={styles.textInputContainer}>
                                <Select
                                    placeholder={{ label: "Select Unit Status", vlaue: null }}
                                    onValueChange={v => console.log(v)}
                                    items={[
                                        { label: "First", value: "first" },
                                        { label: "Second", value: "second" }
                                    ]}
                                />
                            </View>
                            <View>
                                <Select
                                    placeholder={{ label: "Select Unit Finishing", vlaue: null }}
                                    onValueChange={v => console.log(v)}
                                    items={[
                                        { label: "Normal", value: "first" },
                                        { label: "Super", value: "second" }
                                    ]}
                                />
                            </View>

                            <View style={[styles.filterDetails]}>
                                <Text style={[styles.filterDetailsTitle, { flexBasis: "30%" }]}>
                                    Budget Range
                                </Text>
                                <Text
                                    style={[
                                        styles.filterDetailsData,
                                        { flexBasis: "70%", fontFamily: "Gotham-Bold" }
                                    ]}
                                >
                                    EGP 5,000,000 - EGP 22,000,000
                                </Text>
                            </View>

                            <View
                                style={{
                                    alignItems: "center"
                                }}
                            >
                                <RangeSlider
                                    sliderLength={SCREEN_WIDTH - 80}
                                    min={5000000}
                                    max={22000000}
                                    step={10000}
                                    snapped
                                    formatNumber
                                    values={[250000, 25000000]}
                                />
                            </View>

                            {/* Payment Plan Section  */}

                            <View
                                style={{
                                    marginTop: 15,
                                    marginBottom: 24
                                }}
                            >
                                <View style={styles.inputTitleCont}>
                                    <Text style={styles.inputTitle}>Payment Plan Duration</Text>
                                </View>
                                <View style={styles.inputsCont}>
                                    <View
                                        style={[
                                            styles.textInputContainer,
                                            {
                                                flexBasis: "48%",
                                                justifyContent: "center",
                                                alignContent: "center"
                                                // borderWidth: 1,
                                                // borderColor: "red"
                                            }
                                        ]}
                                    >
                                        <TextInput
                                            placeholder="Minimum"
                                            height={45}
                                            style={{
                                                flex: 1
                                            }}
                                        />
                                    </View>
                                    <View style={styles.separator}></View>
                                    <View
                                        style={[
                                            styles.textInputContainer,
                                            {
                                                flexBasis: "48%",
                                                justifyContent: "center",
                                                alignContent: "center"
                                                // borderWidth: 1,s"
                                            }
                                        ]}
                                    >
                                        <TextInput
                                            placeholder="Maximum"
                                            height={45}
                                            style={{
                                                flex: 1
                                            }}
                                        />
                                    </View>
                                </View>
                                <View style={styles.inputsFooter}>
                                    <Text style={styles.inputsFooterText}>
                                        *Min. 0 YRS - Max. 20 YRS
                                    </Text>
                                </View>
                            </View>
                            <Button
                                text="Apply Filters"
                                gradient={true}
                                textColor={colors.white}
                                weight="SemiBold"
                                fontSize={13}
                                onPress={() => {
                                    navigate("Availability");
                                }}
                            />
                        </View>
                    </View>
                </ImageBackground>
            </ScrollView>
        </View>
    );
}
