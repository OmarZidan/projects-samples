import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.lightBlueGrey,
        flex: 1
    },
    titleContainer: {
        justifyContent: "flex-start",
        width: "100%",
        borderColor: colors.charcoalGrey16,
        borderBottomWidth: 1,
        paddingBottom: 23
    },
    filtersBackground: { width: "100%", height: "100%" },
    screenWithBackgroundCont: { padding: 25, marginTop: 150 },
    filtersTitle: {
        fontSize: 22,
        color: colors.dark,
        textTransform: "capitalize",
        fontFamily: "Gotham-Bold"
    },
    textInputContainer: {
        overflow: "hidden",
        height: 45,
        flexBasis: "100%",
        marginVertical: 24
        // borderColor: "#fd1",
        // borderWidth: 1
    },
    filtersCont: {
        // borderColor: "blue",
        // borderWidth: 1,
        width: "100%",
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    filterDetails: {
        marginVertical: 24,
        flexDirection: "row",
        // alignItems: "center",
        justifyContent: "space-between"
    },
    filterDetailsTitle: {
        // borderColor: "olive",
        // borderWidth: 1,
        width: "100%",
        flexBasis: "50%",
        textAlign: "left",
        fontSize: 11,
        fontFamily: "Gotham-Medium",
        letterSpacing: 0.39,
        color: colors.charcoalGrey
    },
    filterDetailsData: {
        // borderColor: "violet",
        // borderWidth: 1,
        width: "100%",
        flexBasis: "50%",
        textAlign: "right",
        fontSize: 11,
        fontFamily: "Gotham-Medium",
        letterSpacing: 0.39,
        color: colors.charcoalGrey
    },
    inputsCont: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        // borderColor: "olive",
        // borderWidth: 1,
        width: "100%",
        height: 45
    },
    inputTitleCont: {
        // borderWidth: 1,
        // borderColor: "blue"
        marginBottom: 10
    },
    inputTitle: {
        fontSize: 12,
        fontFamily: "Gotham-Medium",
        color: colors.dark
    },
    separator: { borderWidth: 0.5, borderColor: colors.charcoalGrey24, height: 40 },
    inputsFooter: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end",
        // borderColor: "darkred",
        // borderWidth: 1,
        width: "100%",
        height: 45
    },
    inputsFooterText: {
        color: colors.redWine,
        fontSize: 9,
        textAlign: "right",
        fontFamily: "Gotham-Medium"
    },
    applyFilters: {
        width: "100%",
        // height: 40,
        flexBasis: "48%",
        shadowColor: colors.charcoalGrey,
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 8,
        borderWidth: 1,
        borderColor: "transparent"
    }
});

export default styles;
