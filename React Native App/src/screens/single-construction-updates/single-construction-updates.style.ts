import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },

    conceptContent: {
        paddingHorizontal: 32,
        marginBottom: 100
    },
    conceptTitle: {
        textTransform: "uppercase",
        color: colors.offWhite,
        fontFamily: "Gotham-ExtraBold",
        fontSize: 19,
        letterSpacing: 0,
        marginBottom: 16
    },
    carouselContainer: {
        marginTop: 175,
        marginBottom: 30,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7
    },
    date: {
        fontSize: 11,
        color: colors.black,
        textTransform: "capitalize"
    },
    contactBtn: {
        position: "absolute",
        bottom: 150,
        borderRadius: 40,
        overflow: "hidden",
        height: 80,
        width: 80,
        backgroundColor: "white",
        right: 10,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 20,
        alignSelf: "flex-end",

        zIndex: 30,
        alignItems: "center",
        justifyContent: "center"
    },
    video: {
        width: "100%",
        height: 370,
        marginTop: 130
    },
    logo: {
        height: 95,
        maxWidth: 240,
        marginBottom: 30,
        backgroundColor: colors.white
    },
    inactiveDots: {
        width: 10,
        borderRadius: 0,
        height: 10,
        borderWidth: 1,
        marginHorizontal: 0,
        borderColor: colors.offWhite,
        backgroundColor: "transparent"
    },
    dot: {
        width: 10,
        height: 10,
        borderRadius: 0,
        marginHorizontal: 0,
        backgroundColor: colors.offWhite
    },
    paginationContainer: {
        backgroundColor: "transparent",
        paddingHorizontal: 0,
        width: "100%"
    },
    playImageContainer: {
        backgroundColor: colors.dark,
        width: 75,
        height: 75,
        justifyContent: "center",
        alignItems: "center"
    },
    playImage: {
        height: 37,
        width: 30,
        resizeMode: "contain"
    }
});

export default styles;
