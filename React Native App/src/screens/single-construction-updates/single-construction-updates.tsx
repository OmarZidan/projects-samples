import React, { useState, useEffect } from "react";
import { View, Image, Dimensions, TouchableOpacity, Modal } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { Header, Text, BottomNavBar } from "@components";
import styles from "./single-construction-updates.style";
import { getAssetUrl, colors } from "@utils";
import { ScrollView } from "react-native-gesture-handler";
import WebView from "react-native-webview";

const { width: screenWidth } = Dimensions.get("window");

interface SingleConstructionUpdatesProps {
    navigation: any;
}
interface UpdateProps {
    sliders: any;
    date: any;
    title: any;
}

interface ProjectProps {
    logo: any;
}

export default function SingleConstructionUpdates({
    navigation
}: SingleConstructionUpdatesProps): React.ReactElement {
    const [activeSlide, setActiveSlide] = useState(0);
    const [update, setUpdate] = useState({} as UpdateProps);
    const [project, setProject] = useState({} as ProjectProps);
    const [activeVideo, setActiveVideo] = useState(false);
    const [video, setVideo] = useState("");
    const carouselRef = React.useRef(undefined);

    useEffect(() => {
        const update = navigation.getParam("update");
        console.log(update);
        setUpdate(update);
        setProject(update.project.data);
    }, []);

    interface ItemProps {
        item: any;
    }

    const renderItem = ({ item }: ItemProps) => (
        <View style={{ height: 370 }}>
            <Image
                style={{ height: "100%", width: "100%", resizeMode: "cover" }}
                source={{
                    uri: getAssetUrl(item.path)
                }}
            />
            {item.video_url && item.video_url !== "" && (
                <View
                    style={{
                        width: 80,
                        height: 80,
                        position: "absolute",
                        left: (310 - 80) / 2,
                        top: 145
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            setActiveVideo(true);
                            setVideo(item.video_url);
                        }}
                        style={styles.playImageContainer}
                    >
                        <Image
                            style={styles.playImage}
                            source={require("@assets/icons/playArrow.png")}
                        />
                    </TouchableOpacity>
                </View>
            )}
        </View>
    );

    return (
        <View style={{ flex: 1 }}>
            <ScrollView style={styles.container}>
                <Header withDrawerMenu={false} />
                <Modal
                    visible={activeVideo}
                    onRequestClose={() => {
                        setActiveVideo(false);
                        setVideo("");
                    }}
                >
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        <TouchableOpacity
                            style={{
                                flex: 1,
                                backgroundColor: "black",
                                position: "absolute",
                                top: 0,
                                bottom: 0,
                                left: 0,
                                right: 0
                            }}
                            onPress={() => setActiveVideo(false)}
                        ></TouchableOpacity>
                        <View style={{ height: 300 }}>
                            <WebView
                                style={{
                                    width: Dimensions.get("window").width
                                }}
                                source={{
                                    uri: video
                                }}
                            />
                        </View>
                    </View>
                </Modal>

                {update.sliders && (
                    <View style={styles.carouselContainer}>
                        <Carousel
                            ref={c => {
                                this._carousel = c;
                            }}
                            data={update.sliders.data}
                            renderItem={renderItem}
                            sliderWidth={screenWidth}
                            itemWidth={310}
                            sliderHeight={310}
                            itemHeight={200}
                            onSnapToItem={index => setActiveSlide(index)}
                        />
                        <Pagination
                            carouselRef={this._carousel}
                            tappableDots
                            dotsLength={update.sliders.data.length}
                            activeDotIndex={activeSlide}
                            containerStyle={styles.paginationContainer}
                            dotStyle={styles.dot}
                            inactiveDotStyle={styles.inactiveDots}
                            inactiveDotOpacity={0.9}
                            inactiveDotScale={0.9}
                        />
                    </View>
                )}
                <View style={styles.conceptContent}>
                    <Image
                        style={styles.logo}
                        source={{ uri: getAssetUrl(project.logo) }}
                        resizeMode="contain"
                    />
                    <Text style={styles.conceptTitle}>{update.title}</Text>
                    <Text weight="SemiBold" style={styles.date}>
                        {update.date}
                    </Text>
                </View>
            </ScrollView>
            {/* <BottomNavBar /> */}
        </View>
    );
}
