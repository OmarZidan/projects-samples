import React, { Fragment } from "react";
import { Text, View, Linking, Image, ImageBackground } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import MapView, { Marker } from "react-native-maps";
import { Header, Button } from "@components";

import styles from "./singleProject.style";
import { getAssetUrl, colors } from "@utils";

interface LocationProps {
    project: any;
}

const Location = ({ project }: LocationProps) => (
    <Fragment>
        {project.project_code !== "tonino" && (
            <ScrollView>
                <Header withBack withDrawerMenu={false} />
                <View style={styles.locationContainer}>
                    <View style={{ marginBottom: 45 }}>
                        {Boolean(project.location_points_latitude) &&
                            Boolean(project.location_points_latitude[0]) && (
                                <MapView
                                    // customMapStyle={mapStyle}
                                    style={styles.mapView}
                                    initialRegion={{
                                        latitude: parseFloat(
                                            project.location_points_latitude[0].latitude
                                        ),
                                        longitude: parseFloat(
                                            project.location_points_longitude[0].longitude
                                        ),
                                        latitudeDelta: 0.035,
                                        longitudeDelta: 0.035
                                    }}
                                >
                                    <Marker
                                        coordinate={{
                                            latitude: parseFloat(
                                                project.location_points_latitude[0].latitude
                                            ),
                                            longitude: parseFloat(
                                                project.location_points_longitude[0].longitude
                                            )
                                        }}
                                        title={"title"}
                                        description={"description"}
                                    />
                                </MapView>
                            )}
                        <Image
                            style={styles.goldDots}
                            source={require("@assets/images/goldDots.png")}
                        />
                    </View>
                    <View style={styles.conceptContent}>
                        <Text style={styles.conceptTitle}>{project.location_title}</Text>
                        <Text style={styles.conceptText}>{project.location_description}</Text>
                        <View style={styles.rowBtns}>
                            <View style={{ width: "48%" }}>
                                <Button
                                    text="Get Directions"
                                    fontSize={13}
                                    gradient={true}
                                    textColor={colors.white}
                                    weight="SemiBold"
                                    onPress={() => Linking.openURL(project.google_maps)}
                                />
                            </View>
                            <View style={{ width: "48%" }}>
                                <Button
                                    text="View Masterplan"
                                    backgroundColor={colors.white}
                                    textColor={colors.offWhite}
                                    fontSize={13}
                                    weight="SemiBold"
                                    onPress={() => {
                                        Linking.openURL(getAssetUrl(project.master_plan_path));
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        )}

        {project.project_code === "tonino" && (
            <ImageBackground
                source={require("@assets/images/patternDark2.png")}
                style={{
                    flex: 1
                }}
                imageStyle={{ resizeMode: "cover" }}
            >
                <ScrollView>
                    <Header withBack withDrawerMenu={false} tonino />
                    <View style={styles.locationContainer}>
                        <View style={{ marginBottom: 45 }}>
                            {Boolean(project.location_points_latitude) &&
                                Boolean(project.location_points_latitude[0]) && (
                                    <MapView
                                        // customMapStyle={mapStyle}
                                        style={[
                                            styles.mapView,
                                            {
                                                width: "96%",
                                                alignSelf: "center"
                                            }
                                        ]}
                                        initialRegion={{
                                            latitude: parseFloat(
                                                project.location_points_latitude[0].latitude
                                            ),
                                            longitude: parseFloat(
                                                project.location_points_longitude[0].longitude
                                            ),
                                            latitudeDelta: 0.035,
                                            longitudeDelta: 0.035
                                        }}
                                    >
                                        <Marker
                                            coordinate={{
                                                latitude: parseFloat(
                                                    project.location_points_latitude[0].latitude
                                                ),
                                                longitude: parseFloat(
                                                    project.location_points_longitude[0].longitude
                                                )
                                            }}
                                            title={"title"}
                                            description={"description"}
                                        />
                                    </MapView>
                                )}
                            {/* <Image
                                style={styles.goldDots}
                                source={require("@assets/images/goldDots.png")}
                            /> */}
                        </View>
                        <View style={styles.conceptContent}>
                            <Text style={styles.toninoConceptTitle}>{project.location_title}</Text>
                            <Text
                                style={{
                                    fontFamily: "ChakraPetch-Bold",
                                    color: colors.white,
                                    lineHeight: 22,
                                    fontSize: 13,
                                    letterSpacing: 2,
                                    marginBottom: 16
                                }}
                            >
                                LOCATION
                            </Text>
                            <Text style={styles.toninoConceptText}>
                                {project.location_description}
                            </Text>
                            <View style={styles.rowBtns}>
                                <View style={{ width: "48%" }}>
                                    <Button
                                        text="Get Directions"
                                        fontSize={13}
                                        tonino
                                        gradient={true}
                                        textColor={colors.white}
                                        weight="Medium"
                                        onPress={() => Linking.openURL(project.google_maps)}
                                    />
                                </View>
                                <View style={{ width: "48%" }}>
                                    <Button
                                        text="View Masterplan"
                                        backgroundColor={colors.white}
                                        textColor={colors.red}
                                        fontSize={13}
                                        weight="Medium"
                                        tonino
                                        onPress={() => {
                                            Linking.openURL(getAssetUrl(project.master_plan_path));
                                        }}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        )}
    </Fragment>
);

export default Location;
