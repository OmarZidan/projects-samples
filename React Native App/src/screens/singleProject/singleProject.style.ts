import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#edeef7"
    },
    videoWrap: {
        width: "100%",
        flexDirection: "row",
        elevation: 9,
        position: "relative",
        zIndex: 999
    },
    video: {
        width: 315,
        height: 250,
        alignSelf: "flex-end"
    },

    videoPlay: {
        // ...StyleSheet.absoluteFill,
        top: 60,
        alignItems: "center",
        justifyContent: "center",
        zIndex: 10
    },
    tabsContainer: {
        flexDirection: "row",
        position: "absolute",
        justifyContent: "space-between",
        bottom: 0,
        backgroundColor: "white",
        left: 10,
        right: 10,
        paddingVertical: 15,
        paddingHorizontal: 25,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7
    },
    toninoTabsContainer: {
        flexDirection: "row",
        position: "absolute",
        justifyContent: "space-between",
        bottom: 0,
        backgroundColor: "black",
        left: 10,
        right: 10,
        paddingVertical: 15,
        paddingHorizontal: 25,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7
    },
    tabBtn: {
        alignItems: "center"
    },
    tabText: {
        fontSize: 10,
        letterSpacing: 0.42,
        marginTop: 8
    },
    toninoTabText: {
        fontSize: 10,
        letterSpacing: 0.42,
        marginTop: 8,
        fontFamily: "Roboto-Bold"
    },
    contactBtn: {
        position: "absolute",
        bottom: 100,
        borderRadius: 40,
        overflow: "hidden",
        height: 80,
        width: 80,
        backgroundColor: "white",
        right: 10,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 20,
        alignSelf: "flex-end",

        zIndex: 30,
        alignItems: "center",
        justifyContent: "center"
    },
    conceptBackground: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end",
        paddingBottom: 100,
        paddingTop: 175
    },
    conceptContent: { paddingHorizontal: 32, paddingVertical: 40 },
    conceptTitle: {
        textTransform: "uppercase",
        color: colors.offWhite,
        fontFamily: "Gotham-Bold",
        fontSize: 15,
        letterSpacing: 0,
        marginBottom: 16
    },
    conceptText: {
        color: colors.black,
        fontFamily: "Gotham-Regular",
        fontSize: 11,
        letterSpacing: 0.2,
        lineHeight: 16,
        textAlign: "justify"
    },
    toninoConceptTitle: {
        textTransform: "uppercase",
        color: colors.red,
        fontFamily: "ChakraPetch-Bold",
        fontSize: 16,
        letterSpacing: 0,
        marginBottom: 16,
        lineHeight: 22
    },
    toninoConceptText: {
        color: colors.white,
        fontFamily: "Roboto-Regular",
        fontSize: 11,
        letterSpacing: 0.18,
        lineHeight: 18,
        textAlign: "justify"
    },
    locationContainer: {
        flex: 1,
        width: "100%",
        justifyContent: "space-between",
        paddingBottom: 100,
        paddingTop: 175
        // marginBottom: 32
    },
    // locationVideo: {
    //     width: "100%",
    //     height: 617,

    //     shadowColor: "#000",
    //     shadowOffset: {
    //         width: 0,
    //         height: 10
    //     },
    //     shadowOpacity: 0.29,
    //     shadowRadius: 4.65,
    //     elevation: 7
    // },
    mapView: {
        width: 315,
        height: 250,
        alignSelf: "flex-end",
        elevation: 9,
        zIndex: 999
    },
    locationTitle: {
        color: colors.dark,
        fontSize: 17,
        fontFamily: "Gotham-ExtraBold",
        textTransform: "uppercase",
        marginBottom: 10
    },
    locationText: {
        color: colors.dark,
        fontSize: 11,
        lineHeight: 20,
        fontFamily: "Gotham-Regular"
    },
    locationLine: {
        width: 35,
        height: 3,
        backgroundColor: colors.dark,
        marginBottom: 15,
        marginTop: 15
    },
    rowBtns: {
        flexDirection: "row",
        marginRight: 32,
        justifyContent: "space-between",
        width: "100%",
        marginTop: 32
    },
    locationPoint: {
        color: colors.dark,
        fontSize: 11,
        lineHeight: 20,
        fontFamily: "Gotham-SemiBold",
        letterSpacing: 0.92,
        width: "100%"
    },
    carouselContainer: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
        marginTop: 190
    },
    galleryTitle: {
        position: "absolute",
        // bottom: 365,
        // bottom: 0,
        left: 15,
        backgroundColor: colors.white,
        paddingHorizontal: 30,
        paddingVertical: 5,
        color: colors.dark,
        textTransform: "uppercase",
        fontSize: 12,
        fontFamily: "Gotham-SemiBold"
    },
    updateItem: {
        height: 250,
        borderRadius: 10,
        overflow: "hidden",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
        paddingHorizontal: 16,
        paddingVertical: 25,
        justifyContent: "flex-end"
    },
    updateItemGradient: {
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    updateItemTitle: {
        fontSize: 8,
        fontFamily: "Gotham-Regular",
        color: "white"
    },
    updateItemDate: {
        fontSize: 17,
        fontFamily: "Gotham-ExtraBold",
        color: "white"
    },
    updateBanner: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
        overflow: "hidden"
    },
    updatePlay: {
        width: 279,
        height: 65,
        borderRadius: 37.5,
        backgroundColor: colors.dark,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
        transform: [
            {
                translateY: -32.5
            }
        ],
        alignSelf: "center",
        flexDirection: "row",
        paddingVertical: 14,
        paddingHorizontal: 20
    },
    unitItem: {
        height: 150,
        width: 100,
        borderRadius: 10,
        overflow: "hidden",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
        paddingHorizontal: 16,
        paddingVertical: 25,
        justifyContent: "flex-end"
    },
    unitGalleryPagination: {
        backgroundColor: "transparent",
        paddingHorizontal: 0,
        width: "100%"
    },
    unitGalleryDots: {
        width: 10,
        height: 10,
        borderRadius: 0,
        marginHorizontal: 0,
        backgroundColor: colors.offWhite
    },
    unitGalleryInactiveDots: {
        width: 10,
        borderRadius: 0,
        height: 10,
        borderWidth: 1,
        marginHorizontal: 0,
        borderColor: colors.offWhite,
        backgroundColor: "transparent"
    },
    unitContent: { paddingTop: 72, paddingHorizontal: 32, paddingBottom: 26 },
    unitOverlay: {
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    unitBtnsContainer: {
        flexDirection: "row",
        marginRight: 32,
        justifyContent: "space-between",
        width: "100%",
        marginTop: 16
    },
    headerImage: {
        width: "100%",
        height: 275
    },
    modalBackground: {
        flex: 1,
        backgroundColor: "rgba(1,1,1,.7)",
        alignItems: "center",
        justifyContent: "center"
    },
    updateContainer: {
        flex: 1,
        width: "100%",
        justifyContent: "space-between",
        paddingBottom: 100
    },
    pageTitle: {
        fontSize: 36,
        letterSpacing: 0.19,
        color: colors.white,
        textTransform: "uppercase",
        marginTop: 130,
        marginLeft: 25
    },
    inputIOS: {
        backgroundColor: "#fff",
        fontFamily: `Gotham-Regular`,
        fontSize: 11,
        paddingHorizontal: 19,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: "#fff",
        borderRadius: 12,
        letterSpacing: 1.7,
        color: "black",
        paddingRight: 30,
        marginHorizontal: 25,
        marginTop: 20
    },
    inputAndroid: {
        backgroundColor: "#fff",
        fontSize: 11,
        paddingHorizontal: 19,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: "#fff",
        borderRadius: 12,
        letterSpacing: 1.7,
        color: "black",
        paddingRight: 30,
        marginHorizontal: 25,
        marginTop: 20,
        shadowColor: colors.dark,
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.9,
        shadowRadius: 2
    },
    iconContainer: {
        top: 40,
        right: 45
    },
    filterList: {
        marginTop: 10,
        marginRight: 25
    },
    filterItem: {
        marginLeft: 25
    },
    filterItemsText: {
        color: colors.dark,
        fontSize: 12,
        letterSpacing: 0.06
    },
    activeFilter: {
        color: colors.dark,
        fontSize: 12,
        letterSpacing: 0.06
    },
    activeFilterUnderline: {
        borderBottomColor: colors.dark,
        borderBottomWidth: 1,
        maxWidth: 25,
        marginTop: 5
    },
    projectsList: {
        marginBottom: 50,
        marginHorizontal: 25
    },
    projectImage: {
        width: "100%",
        height: 190,
        borderRadius: 15,
        marginTop: 25
    },
    goldDots: {
        height: 240,
        width: 240,
        position: "absolute",
        marginLeft: 23,
        top: 40,
        resizeMode: "contain"
    },
    unitItemText: {
        alignSelf: "center",
        color: colors.dark,
        fontSize: 15,
        fontFamily: "Gotham-Bold",
        lineHeight: 21,
        letterSpacing: 0.7
        // marginLeft: 14
    },
    unitItemImage: {
        height: 100,
        width: 80,
        resizeMode: "cover"
        // marginRight: 14
    },
    unitItemContainer: {
        backgroundColor: colors.white,
        padding: 12,
        flexDirection: "row",
        borderRadius: 1,
        // width: 250,
        height: 125
    },
    albumsContainer: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    albumsListContainer: {
        marginTop: 160,
        paddingHorizontal: 5
    },
    albumsListItem: {
        marginBottom: 25
    },
    albumItemBox: {
        marginBottom: 25
    },
    albumItemBoxBG: {
        width: "100%",
        height: 215,
        position: "relative",
        borderRadius: 2
    },
    albumBoxOverlay: {
        height: 215,
        flex: 1,
        position: "absolute",
        left: 0,
        top: 0,
        opacity: 0.5,
        backgroundColor: colors.charcoalGrey,
        width: "100%",
        borderRadius: 2
    },
    albumItemImage: {
        width: 115,
        height: "100%",
        resizeMode: "contain"
    },
    albumItemTitle: {
        fontFamily: "Gotham-Medium",
        fontSize: 32,
        letterSpacing: 0,
        textAlign: "center",
        color: colors.white,
        zIndex: 999,
        marginTop: 80,
        textTransform: "capitalize"
    },
    albumBtnContainer: {
        marginTop: 40,
        maxWidth: 125,
        width: 125
    },
    albumItemBtn: {
        textAlign: "center",
        color: colors.white,
        zIndex: 999,
        fontFamily: "Gotham-Regular",
        fontSize: 10,
        marginTop: 30,
        textDecorationLine: "underline"
    },
    galleryContainer: { flex: 1, backgroundColor: colors.lightBlueGrey },
    galleryDots: {
        width: 10,
        height: 10,
        borderRadius: 0,
        marginHorizontal: 0,
        backgroundColor: colors.offWhite
    },
    galleryInactiveDot: {
        width: 10,
        borderRadius: 0,
        height: 10,
        borderWidth: 1,
        marginHorizontal: 0,
        borderColor: colors.offWhite,
        backgroundColor: "transparent"
    }
});

export default styles;
