import React, { useState, useEffect, Fragment } from "react";
import { Text, View, Dimensions, Image, ImageBackground } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { Header } from "@components";
import styles from "./singleProject.style";
import { getAssetUrl, colors } from "@utils";
import newPlan from "@config/api";

const { width: screenWidth, height: screenHeight } = Dimensions.get("window");

interface GalleryProps {
    navigation: any;
}

function Gallery({ navigation }: GalleryProps) {
    const [activeSlide, setActiveSlide] = useState(0);
    const [projectCode, setProjectCode] = useState("");
    const [gallery, setGallery] = useState({
        data: [{ title: "", path: "" }]
    });

    useEffect(() => {
        const id = navigation.getParam("id");
        // eslint-disable-next-line @typescript-eslint/camelcase
        setProjectCode(navigation.getParam("project_code"));
        newPlan(`/album/${id}`).then(response => {
            setGallery(response.data.data.gallery);
        });
    }, []);

    interface ItemProps {
        item: any;
    }

    const renderItem = ({ item }: ItemProps) => {
        return (
            <View style={{ height: 310 }}>
                <Image
                    style={{ height: "100%", width: "100%", resizeMode: "contain" }}
                    source={{
                        uri: getAssetUrl(item.path)
                    }}
                />
            </View>
        );
    };
    return (
        <View style={styles.galleryContainer}>
            {projectCode !== "tonino" && (
                <Fragment>
                    <Header withBack withDrawerMenu={false} />
                    {gallery && (
                        <View style={{ marginTop: 190 }}>
                            <Carousel
                                ref={c => {
                                    this._carousel = c;
                                }}
                                data={gallery.data}
                                renderItem={renderItem}
                                sliderWidth={screenWidth}
                                itemWidth={310}
                                onSnapToItem={index => setActiveSlide(index)}
                            />
                            <Pagination
                                carouselRef={this._carousel}
                                tappableDots
                                dotsLength={gallery.data.length}
                                activeDotIndex={activeSlide}
                                containerStyle={{
                                    backgroundColor: "transparent",
                                    paddingHorizontal: 0,
                                    width: "100%"
                                }}
                                dotStyle={styles.galleryDots}
                                inactiveDotStyle={styles.galleryInactiveDot}
                                inactiveDotOpacity={0.9}
                                inactiveDotScale={0.9}
                            />
                            <View>
                                <Text
                                    style={[
                                        [
                                            styles.galleryTitle,
                                            { bottom: gallery.data.length > 1 ? 365 : 300 }
                                        ]
                                    ]}
                                >
                                    {gallery.data[activeSlide].title}
                                </Text>
                            </View>
                        </View>
                    )}
                </Fragment>
            )}

            {projectCode === "tonino" && (
                <ImageBackground
                    source={require("@assets/images/patternDark2.png")}
                    style={{
                        flex: 1
                    }}
                    imageStyle={{ resizeMode: "cover" }}
                >
                    <Fragment>
                        <Header withBack withDrawerMenu={false} tonino />
                        {gallery && (
                            <View style={{ marginTop: 190 }}>
                                <Carousel
                                    ref={c => {
                                        this._carousel = c;
                                    }}
                                    data={gallery.data}
                                    renderItem={renderItem}
                                    sliderWidth={screenWidth}
                                    itemWidth={310}
                                    onSnapToItem={index => setActiveSlide(index)}
                                />
                                <Pagination
                                    carouselRef={this._carousel}
                                    tappableDots
                                    dotsLength={gallery.data.length}
                                    activeDotIndex={activeSlide}
                                    containerStyle={{
                                        backgroundColor: "transparent",
                                        paddingHorizontal: 0,
                                        width: "100%"
                                    }}
                                    dotStyle={[
                                        styles.galleryDots,
                                        { backgroundColor: colors.white }
                                    ]}
                                    inactiveDotStyle={[
                                        styles.galleryInactiveDot,
                                        { borderColor: colors.white }
                                    ]}
                                    inactiveDotOpacity={0.9}
                                    inactiveDotScale={0.9}
                                />
                                <View>
                                    <Text
                                        style={[
                                            [
                                                styles.galleryTitle,
                                                {
                                                    fontSize: 10,
                                                    fontFamily: "ChakraPetch-Bold",
                                                    bottom: gallery.data.length > 1 ? 365 : 300
                                                }
                                            ]
                                        ]}
                                    >
                                        {gallery.data[activeSlide].title}
                                    </Text>
                                </View>
                            </View>
                        )}
                    </Fragment>
                </ImageBackground>
            )}
        </View>
    );
}

export default Gallery;
