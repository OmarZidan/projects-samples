import React from "react";
import { View, Text, ImageBackground, TouchableOpacity, FlatList } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { Header } from "@components";
import { colors, getAssetUrl } from "@utils";
import styles from "./singleProject.style";
import { useNavigation } from "react-navigation-hooks";

interface AlbumsProps {
    project: any;
}

function Albums({ project }: AlbumsProps) {
    const { albums } = project;
    const { navigate } = useNavigation();

    interface ItemProps {
        item: any;
    }

    return (
        <View style={styles.albumsContainer}>
            {project.project_code !== "tonino" && (
                <ScrollView>
                    <Header withDrawerMenu={false} withBack={true} />
                    <View>
                        <FlatList
                            contentContainerStyle={styles.albumsListContainer}
                            data={albums.data}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item }: ItemProps) => {
                                return (
                                    <View key={item.id} style={{ marginBottom: 25 }}>
                                        <ImageBackground
                                            source={{ uri: getAssetUrl(item.cover) }}
                                            imageStyle={{ resizeMode: "cover", borderRadius: 2 }}
                                            style={styles.albumItemBoxBG}
                                        >
                                            <View style={styles.albumBoxOverlay} />
                                            <Text style={styles.albumItemTitle}>{item.name}</Text>

                                            <TouchableOpacity
                                                onPress={() =>
                                                    navigate("Gallery", {
                                                        id: item.id
                                                    })
                                                }
                                            >
                                                <Text style={styles.albumItemBtn}>View Photos</Text>
                                            </TouchableOpacity>
                                        </ImageBackground>
                                    </View>
                                );
                            }}
                        />
                    </View>
                </ScrollView>
            )}

            {project.project_code === "tonino" && (
                <ImageBackground
                    source={require("@assets/images/patternDark2.png")}
                    style={{
                        flex: 1
                    }}
                    imageStyle={{ resizeMode: "cover" }}
                >
                    <ScrollView>
                        <Header withDrawerMenu={false} withBack tonino />
                        <View>
                            <FlatList
                                contentContainerStyle={styles.albumsListContainer}
                                data={albums.data}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item }: ItemProps) => {
                                    return (
                                        <View key={item.id} style={{ marginBottom: 10 }}>
                                            <ImageBackground
                                                source={{ uri: getAssetUrl(item.cover) }}
                                                imageStyle={{
                                                    resizeMode: "cover",
                                                    borderRadius: 2
                                                }}
                                                style={styles.albumItemBoxBG}
                                            >
                                                <View style={styles.albumBoxOverlay} />
                                                <Text
                                                    style={[
                                                        styles.albumItemTitle,
                                                        { fontFamily: "ChakraPetch-Bold" }
                                                    ]}
                                                >
                                                    {item.name}
                                                </Text>

                                                <TouchableOpacity
                                                    onPress={() =>
                                                        navigate("Gallery", {
                                                            id: item.id,
                                                            // eslint-disable-next-line @typescript-eslint/camelcase
                                                            project_code: project.project_code
                                                        })
                                                    }
                                                >
                                                    <Text
                                                        style={[
                                                            styles.albumItemBtn,
                                                            { fontFamily: "Roboto-Medium" }
                                                        ]}
                                                    >
                                                        View Photos
                                                    </Text>
                                                </TouchableOpacity>
                                            </ImageBackground>
                                        </View>
                                    );
                                }}
                            />
                        </View>
                    </ScrollView>
                </ImageBackground>
            )}
        </View>
    );
}

export default Albums;
