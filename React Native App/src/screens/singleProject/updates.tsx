import React, { useState, Fragment } from "react";
import {
    View,
    Text,
    Image,
    ImageBackground,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    Modal,
    FlatList
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { Header } from "@components";
import { colors, getAssetUrl } from "@utils";
import styles from "./singleProject.style";
import WebView from "react-native-webview";

interface UpdatesProps {
    project: any;
}

function Updates({ project }: UpdatesProps) {
    console.log(project.updates.data);
    const [video, setVideo] = useState("");
    const [activeVideo, setActiveVideo] = useState(false);
    const [loadingVideo, setLoadingVideo] = useState(false);

    const handleItemPress = async item => {
        setActiveVideo(true);
        setVideo(item.video.replace("/storage/", ""));
    };

    const { updates } = project;
    return (
        <Fragment>
            <Modal visible={loadingVideo} transparent>
                <View style={styles.modalBackground}>
                    <ActivityIndicator color="#fff" />
                </View>
            </Modal>
            <Header />
            <ScrollView>
                <View style={{ marginTop: 160 }}>
                    {updates.data &&
                        updates.data.map(item => (
                            <View
                                key={item.date}
                                style={{
                                    backgroundColor: colors.white,
                                    height: 135,
                                    marginHorizontal: 24,
                                    marginBottom: 32,
                                    flexDirection: "row"
                                }}
                            >
                                <ImageBackground
                                    style={{
                                        width: 115,
                                        height: "100%",
                                        justifyContent: "center",
                                        alignItems: "center"
                                        // resizeMode: 'contain'
                                    }}
                                    source={{ uri: getAssetUrl(item.image) }}
                                >
                                    <TouchableOpacity
                                        onPress={() => handleItemPress(item)}
                                        style={{
                                            backgroundColor: colors.dark,
                                            paddingLeft: 16,
                                            paddingRight: 11,
                                            paddingVertical: 11
                                        }}
                                    >
                                        <Image
                                            style={{
                                                height: 22,
                                                width: 17.5,
                                                resizeMode: "contain"
                                            }}
                                            source={require("@assets/icons/playArrow.png")}
                                        />
                                    </TouchableOpacity>
                                </ImageBackground>
                                <View style={{ padding: 16 }}>
                                    <Text
                                        style={{
                                            fontFamily: "Gotham-Bold",
                                            fontSize: 13,
                                            letterSpacing: 0
                                        }}
                                    >
                                        Construction Updates
                                    </Text>
                                    <Text
                                        style={{
                                            fontFamily: "Gotham-Light",
                                            fontSize: 13,
                                            letterSpacing: 0
                                        }}
                                    >
                                        {item.date}
                                    </Text>
                                </View>
                            </View>
                        ))}
                </View>
                <Modal
                    visible={activeVideo}
                    onRequestClose={() => {
                        setVideo("");
                        setActiveVideo(false);
                    }}
                >
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        <TouchableOpacity
                            style={{
                                flex: 1,
                                backgroundColor: "black",
                                position: "absolute",
                                top: 0,
                                bottom: 0,
                                left: 0,
                                right: 0
                            }}
                            onPress={() => setActiveVideo(false)}
                        ></TouchableOpacity>
                        <View style={{ height: 300 }}>
                            <WebView
                                style={{
                                    width: Dimensions.get("window").width
                                }}
                                source={{
                                    uri: video
                                }}
                            />
                        </View>
                    </View>
                </Modal>
            </ScrollView>
        </Fragment>
    );
}

export default Updates;
