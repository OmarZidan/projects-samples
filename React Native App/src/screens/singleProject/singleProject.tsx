import React, { useState, useEffect } from "react";
import { Text, View, TouchableWithoutFeedback, Image, ActivityIndicator } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import newPlan from "@config/api";

import Units from "./units";
import Gallery from "./gallery";
import Concept from "./concept";
import Location from "./location";
import Albums from "./albums";

// import Updates from "./updates";

import styles from "./singleProject.style";
import { colors } from "@utils";

interface SingleProjectProps {
    navigation: any;
}

function SingleProject({ navigation }: SingleProjectProps) {
    const [tabIndex, setTabIndex] = useState(0);
    const [tabs, setTabs] = useState([]);
    const [project, setProject] = useState({
        // eslint-disable-next-line @typescript-eslint/camelcase
        project_code: ""
    });

    useEffect(() => {
        const id = navigation.getParam("id");
        newPlan(`/projects/${id}`).then(response => {
            console.log(response.data.date);
            setProject(response.data.data);
            setTabs([
                {
                    title: "About",
                    icon: require("@assets/icons/concept.png"),
                    // eslint-disable-next-line react/display-name
                    render: () => <Concept project={response.data.data} />
                },
                {
                    title: "Location",
                    icon: require("@assets/icons/location.png"),
                    // eslint-disable-next-line react/display-name
                    render: () => <Location project={response.data.data} />
                },
                {
                    title: "Gallery",
                    icon: require("@assets/icons/gallery.png"),
                    // eslint-disable-next-line react/display-name
                    render: () => <Albums project={response.data.data} />
                },
                {
                    title: "Units",
                    icon: require("@assets/icons/units.png"),
                    // eslint-disable-next-line react/display-name
                    render: () => <Units project={response.data.data} />
                }
            ]);
        });
    }, []);

    return (
        <View style={{ flex: 1, position: "relative" }}>
            {project == null && (
                <View style={{ flex: 1, justifyContent: "center" }}>
                    <ActivityIndicator size="large" color={colors.offWhite} />
                </View>
            )}
            {project && (
                <ScrollView contentContainerStyle={styles.container}>
                    {Boolean(tabs.length) && tabs[tabIndex].render()}
                </ScrollView>
            )}
            {project.project_code !== "tonino" && (
                <View style={styles.tabsContainer}>
                    {tabs.map(({ title, icon }, i) => (
                        <TouchableWithoutFeedback
                            key={`tabPress_${i}`}
                            onPress={() => setTabIndex(i)}
                        >
                            <View style={styles.tabBtn}>
                                <Image
                                    style={{
                                        height: 26,
                                        width: 26,
                                        tintColor: tabIndex === i ? colors.offWhite : colors.dark
                                    }}
                                    source={icon}
                                />
                                <Text
                                    style={[
                                        styles.tabText,
                                        { color: tabIndex === i ? colors.offWhite : colors.dark }
                                    ]}
                                >
                                    {title}
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    ))}
                </View>
            )}

            {project.project_code === "tonino" && (
                <View style={styles.toninoTabsContainer}>
                    {tabs.map(({ title, icon }, i) => (
                        <TouchableWithoutFeedback
                            key={`tabPress_${i}`}
                            onPress={() => setTabIndex(i)}
                        >
                            <View style={styles.tabBtn}>
                                <Image
                                    style={{
                                        height: 26,
                                        width: 26,
                                        tintColor: tabIndex === i ? "white" : "#6d6d6d"
                                    }}
                                    source={icon}
                                />
                                <Text
                                    style={[
                                        styles.toninoTabText,
                                        { color: tabIndex === i ? "white" : "#6d6d6d" }
                                    ]}
                                >
                                    {title}
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    ))}
                </View>
            )}
            <View
                style={[
                    styles.contactBtn,
                    {
                        backgroundColor:
                            project.project_code !== "tonino" ? colors.white : colors.black40
                    }
                ]}
            >
                <TouchableWithoutFeedback
                    onPress={() =>
                        navigation.navigate("InterestedForm", { id: navigation.getParam("id") })
                    }
                    style={{
                        width: "100%",
                        height: "100%"
                    }}
                >
                    <Image
                        style={{
                            tintColor: colors.offWhite,
                            width: 40,
                            height: 35
                        }}
                        source={require("@assets/icons/letter.png")}
                    />
                </TouchableWithoutFeedback>
                <Text
                    style={{
                        color: colors.offWhite,
                        fontSize: 10,
                        fontFamily:
                            project.project_code !== "tonino" ? "Gotham-Regular" : "Roboto-Regular"
                    }}
                >
                    Interested?
                </Text>
            </View>
        </View>
    );
    // }
}

export default SingleProject;
