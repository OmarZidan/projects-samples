import React, { Fragment, useState } from "react";
import {
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    Text,
    Linking,
    ImageBackground,
    Alert
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { Header, Button, Select } from "@components";
import styles from "./singleProject.style";
import { getAssetUrl, colors } from "@utils";
import newPlan from "@config/api";

const { width: screenWidth } = Dimensions.get("window");

interface UnitsProps {
    project: any;
}
function Units({ project }: UnitsProps) {
    const [activeSlide, setActiveSlide] = useState(0);
    const [currentUnit, setCurrentUnit] = useState(0);
    const scrollRef = React.useRef(undefined);
    const [units, setUnits] = useState(project.units.data);
    // const units = project.units.data;
    const brochure = project.brochure_path;

    interface ItemProps {
        item: any;
    }
    const renderItem = ({ item }: ItemProps) => {
        return (
            <View style={{ height: 310 }}>
                <Image
                    style={{ height: "100%", width: "100%", resizeMode: "contain" }}
                    source={{ uri: getAssetUrl(item.path) }}
                />
            </View>
        );
    };
    const onUnitPress = index => {
        setActiveSlide(0);
        setCurrentUnit(index);
        scrollRef.current.scrollTo({
            y: 0,
            animated: true
        });
    };

    const handleRooms = val => {
        const id = project.id;
        if (val === "all") {
            setUnits(project.units.data);
        } else if (val !== "all") {
            newPlan(`/units?rooms=${val}&project=${id}`).then(response => {
                if (response.data.data.length == 0) {
                    Alert.alert("", "No Units Available", [
                        {
                            text: "Cancel",
                            onPress: () => console.log("Cancel Pressed"),
                            style: "cancel"
                        }
                    ]);
                } else setUnits(response.data.data);
            });
        }
    };

    return (
        <Fragment>
            {project.project_code !== "tonino" && (
                <ScrollView ref={scrollRef}>
                    <Header withBack withDrawerMenu={false} />
                    {units[currentUnit] && (
                        <View style={styles.carouselContainer}>
                            <Carousel
                                ref={c => {
                                    this._carousel = c;
                                }}
                                data={units[currentUnit].gallery.data}
                                renderItem={renderItem}
                                sliderWidth={screenWidth}
                                itemWidth={310}
                                sliderHeight={200}
                                itemHeight={200}
                                onSnapToItem={index => setActiveSlide(index)}
                            />
                            <Pagination
                                dotsLength={units[currentUnit].gallery.data.length}
                                activeDotIndex={activeSlide}
                                containerStyle={styles.unitGalleryPagination}
                                dotStyle={styles.unitGalleryDots}
                                inactiveDotStyle={styles.unitGalleryInactiveDots}
                                inactiveDotOpacity={0.9}
                                inactiveDotScale={0.9}
                            />
                        </View>
                    )}
                    {units[currentUnit] && (
                        <View style={styles.conceptContent}>
                            <Text style={[styles.conceptTitle, { lineHeight: 21 }]}>
                                {units[currentUnit].name}
                                {/* {units[currentUnit].type ? ` Type - ${units[currentUnit].type}` : ""} */}
                            </Text>
                            <Text style={styles.conceptText}>{units[currentUnit].description}</Text>
                            <View style={styles.unitBtnsContainer}>
                                <View style={{ width: "48%" }}>
                                    <Button
                                        text="Download Brochure"
                                        fontSize={11}
                                        gradient={true}
                                        textColor={colors.white}
                                        weight="SemiBold"
                                        onPress={() => Linking.openURL(getAssetUrl(brochure))}
                                    />
                                </View>
                                <View style={{ width: "48%" }}>
                                    <Button
                                        text="View Floorplan"
                                        backgroundColor={colors.white}
                                        textColor={colors.offWhite}
                                        fontSize={12}
                                        weight="SemiBold"
                                    />
                                </View>
                            </View>
                        </View>
                    )}

                    <View style={{ marginHorizontal: 32, marginBottom: 40 }}>
                        <Select
                            placeholder={{ label: "Select Number of Rooms", vlaue: null }}
                            onValueChange={v => handleRooms(v)}
                            items={[
                                { label: "All", value: "all" },
                                { label: "2 Rooms", value: "2" },
                                { label: "3 Rooms", value: "3" },
                                { label: "4 Rooms", value: "4" },
                                { label: "5 Rooms", value: "4" }
                            ]}
                        />
                    </View>
                    <ScrollView
                        contentContainerStyle={{ paddingLeft: 32, paddingBottom: 105 }}
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                    >
                        {units.map((item, index) => (
                            <TouchableOpacity
                                key={`unit_${index}`}
                                onPress={() => onUnitPress(index)}
                                style={{
                                    marginHorizontal: 5
                                }}
                            >
                                <View style={styles.unitItemContainer}>
                                    <Image
                                        style={styles.unitItemImage}
                                        source={{ uri: getAssetUrl(item.image) }}
                                    />
                                    <View
                                        style={{
                                            borderRightWidth: 1,
                                            borderColor: colors.blueyGrey32
                                        }}
                                    ></View>
                                    <Text style={styles.unitItemText}>
                                        {/* {item.name} */}
                                        {item.type ? ` Type - ${item.type}` : ""}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </ScrollView>
                </ScrollView>
            )}

            {project.project_code === "tonino" && (
                <ImageBackground
                    source={require("@assets/images/patternDark2.png")}
                    style={{
                        flex: 1
                    }}
                    imageStyle={{ resizeMode: "cover" }}
                >
                    <ScrollView ref={scrollRef}>
                        <Header withBack withDrawerMenu={false} tonino />
                        {units[currentUnit] && (
                            <View style={styles.carouselContainer}>
                                <Carousel
                                    ref={c => {
                                        this._carousel = c;
                                    }}
                                    data={units[currentUnit].gallery.data}
                                    renderItem={renderItem}
                                    sliderWidth={screenWidth}
                                    itemWidth={310}
                                    sliderHeight={200}
                                    itemHeight={200}
                                    onSnapToItem={index => setActiveSlide(index)}
                                />
                                <Pagination
                                    dotsLength={units[currentUnit].gallery.data.length}
                                    activeDotIndex={activeSlide}
                                    containerStyle={styles.unitGalleryPagination}
                                    dotStyle={[
                                        styles.unitGalleryDots,
                                        { backgroundColor: colors.white }
                                    ]}
                                    inactiveDotStyle={[
                                        styles.unitGalleryInactiveDots,
                                        { borderColor: colors.white }
                                    ]}
                                    inactiveDotOpacity={0.9}
                                    inactiveDotScale={0.9}
                                />
                            </View>
                        )}
                        {units[currentUnit] && (
                            <View style={styles.conceptContent}>
                                <Text
                                    style={[
                                        styles.conceptTitle,
                                        {
                                            lineHeight: 21,
                                            fontFamily: "ChakraPetch-Bold",
                                            color: colors.red
                                        }
                                    ]}
                                >
                                    {units[currentUnit].name}
                                </Text>
                                <Text style={[styles.conceptText, { color: colors.white }]}>
                                    {units[currentUnit].description}
                                </Text>
                                <View style={styles.unitBtnsContainer}>
                                    <View style={{ width: "48%" }}>
                                        <Button
                                            text="Download Brochure"
                                            fontSize={12}
                                            gradient={true}
                                            textColor={colors.white}
                                            weight="Medium"
                                            tonino
                                            onPress={() => Linking.openURL(getAssetUrl(brochure))}
                                        />
                                    </View>
                                    <View style={{ width: "48%" }}>
                                        <Button
                                            text="View Floorplan"
                                            backgroundColor={colors.white}
                                            textColor={colors.red}
                                            fontSize={12}
                                            weight="Medium"
                                            tonino
                                            onPress={() => {
                                                Linking.openURL(
                                                    getAssetUrl(project.master_plan_path)
                                                );
                                            }}
                                        />
                                    </View>
                                </View>
                            </View>
                        )}
                        <View style={{ marginHorizontal: 32, marginBottom: 40 }}>
                            <Select
                                tonino
                                placeholder={{ label: "Select Number of Rooms", vlaue: null }}
                                onValueChange={v => handleRooms(v)}
                                items={[
                                    { label: "All", value: "all" },
                                    { label: "2 Rooms", value: "2" },
                                    { label: "3 Rooms", value: "3" },
                                    { label: "4 Rooms", value: "4" }
                                ]}
                            />
                        </View>
                        <ScrollView
                            contentContainerStyle={{ paddingLeft: 32, paddingBottom: 105 }}
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                        >
                            {units.map((item, index) => (
                                <TouchableOpacity
                                    key={`unit_${index}`}
                                    onPress={() => onUnitPress(index)}
                                    style={{
                                        marginHorizontal: 5
                                    }}
                                >
                                    <View style={{ flexDirection: "row" }}>
                                        <View style={{ backgroundColor: colors.white, padding: 5 }}>
                                            <Image
                                                style={[
                                                    styles.unitItemImage,
                                                    { height: 125, width: 90 }
                                                ]}
                                                source={{ uri: getAssetUrl(item.image) }}
                                            />
                                        </View>
                                        <View
                                            style={{
                                                backgroundColor: colors.black40,
                                                width: 150,
                                                justifyContent: "center",
                                                alignItems: "flex-start"
                                            }}
                                        >
                                            <Text
                                                style={[
                                                    styles.unitItemText,
                                                    {
                                                        color: colors.white,
                                                        alignSelf: "flex-start",
                                                        marginLeft: 30,
                                                        fontFamily: "ChakraPetch-Bold"
                                                    }
                                                ]}
                                            >
                                                {item.type ? ` Type - ${item.type}` : ""}
                                            </Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            ))}
                        </ScrollView>
                    </ScrollView>
                </ImageBackground>
            )}
        </Fragment>
    );
}

export default Units;
