import React, { useEffect, useState } from "react";
import { Text, Linking, View, Image, ActivityIndicator, ImageBackground } from "react-native";
import { Header, Button } from "@components";
import styles from "./singleProject.style";
import { getAssetUrl, colors } from "@utils";
import { ScrollView } from "react-native-gesture-handler";
import WebView from "react-native-webview";

interface ConceptProps {
    project: any;
}

const Concept = ({ project }: ConceptProps) => {
    return (
        <View style={{ flex: 1 }}>
            {project.project_code !== "tonino" && (
                <ScrollView>
                    <Header withDrawerMenu={false} withBack />
                    <View style={styles.conceptBackground}>
                        {project.location_video_path && (
                            <View style={{ marginBottom: 45 }}>
                                <View style={styles.videoWrap}>
                                    <WebView
                                        style={styles.video}
                                        source={{
                                            uri: project.location_video_path.replace(
                                                "/storage/",
                                                ""
                                            )
                                        }}
                                    />
                                </View>
                                <Image
                                    style={styles.goldDots}
                                    source={require("@assets/images/goldDots.png")}
                                />
                            </View>
                        )}
                        <View style={styles.conceptContent}>
                            <Image
                                style={{
                                    height: 96,
                                    width: 240,
                                    marginBottom: 33,
                                    resizeMode: "contain"
                                }}
                                source={{ uri: getAssetUrl(project.logo) }}
                            />
                            <Text style={styles.conceptTitle}>{project.name}</Text>
                            <Text style={styles.conceptText}>{project.concept}</Text>
                            <View style={{ width: 200, marginTop: 15 }}>
                                {project.brochure_path && (
                                    <Button
                                        text="Download Brochure"
                                        gradient={true}
                                        textColor={colors.white}
                                        weight="SemiBold"
                                        fontSize={13}
                                        onPress={() => {
                                            Linking.openURL(getAssetUrl(project.brochure_path));
                                        }}
                                    />
                                )}
                            </View>
                        </View>
                    </View>
                </ScrollView>
            )}

            {project.project_code === "tonino" && (
                <ImageBackground
                    source={require("@assets/images/patternDark2.png")}
                    style={{
                        flex: 1
                    }}
                    imageStyle={{ resizeMode: "cover" }}
                >
                    <ScrollView>
                        <Header withDrawerMenu={false} withBack tonino />
                        <View style={styles.conceptBackground}>
                            {/* {project.location_video_path && (
                                <View style={{ marginBottom: 45 }}>
                                    <View style={styles.videoWrap}>
                                        <WebView
                                            style={styles.video}
                                            source={{
                                                uri: project.location_video_path.replace(
                                                    "/storage/",
                                                    ""
                                                )
                                            }}
                                        />
                                    </View>
                                    <Image
                                        style={styles.goldDots}
                                        source={require("@assets/images/goldDots.png")}
                                    />
                                </View>
                            )} */}
                            {project.concept_image && (
                                <View style={{ marginBottom: 45 }}>
                                    <Image
                                        source={{ uri: getAssetUrl(project.concept_image) }}
                                        style={{
                                            height: 250,
                                            width: 365,
                                            resizeMode: "cover"
                                        }}
                                    />
                                </View>
                            )}
                            <View style={styles.conceptContent}>
                                <View
                                    style={{
                                        backgroundColor: colors.lightWhite,
                                        width: 240,
                                        height: 96,
                                        justifyContent: "center",
                                        alignItems: "center",
                                        flexDirection: "row",
                                        marginBottom: 33
                                    }}
                                >
                                    <Image
                                        style={{
                                            height: 85,
                                            width: 150,
                                            resizeMode: "contain"
                                        }}
                                        source={{ uri: getAssetUrl(project.logo) }}
                                    />
                                </View>
                                <Text style={styles.toninoConceptTitle}>{project.name}</Text>
                                <Text style={styles.toninoConceptText}>{project.concept}</Text>
                                <View style={{ width: 200, marginTop: 32 }}>
                                    {project.brochure_path && (
                                        <Button
                                            text="Download Brochure"
                                            gradient={true}
                                            textColor={colors.white}
                                            weight="Medium"
                                            tonino={true}
                                            fontSize={13}
                                            onPress={() => {
                                                Linking.openURL(getAssetUrl(project.brochure_path));
                                            }}
                                        />
                                    )}
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </ImageBackground>
            )}
        </View>
    );
};

export default Concept;
