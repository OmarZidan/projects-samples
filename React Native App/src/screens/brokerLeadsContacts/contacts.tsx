import React from "react";
import { Text, View, ScrollView, Image } from "react-native";
import styles from "./brokerLeadsContacts.style";
import { Title, Button, BottomNavBar, Header, SearchboxAndFilters, ContactItem } from "@components";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";
import { TouchableOpacity } from "react-native-gesture-handler";

export default function Contacts(): React.ReactElement {
    const { navigate } = useNavigation();
    const contacts = [
        {
            id: "1",
            title: "Yara Shawky Ahmed",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "Contracted"
        },
        {
            id: "2",
            title: "Dania Hussien Ali",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "Down Payment"
        },
        {
            id: "3",
            title: "Samir Salem Fakhoury",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "Not Interested"
        },
        {
            id: "4",
            title: "Abdelrahman Fares",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "URF"
        }
    ];

    return (
        <View style={{ flex: 1 }}>
            <View>
                <View>
                    <SearchboxAndFilters onPress={() => null} />
                </View>
                <TouchableOpacity
                    style={{
                        marginHorizontal: 32,
                        borderColor: colors.blueyGrey,
                        borderWidth: 1.2,
                        paddingVertical: 16,
                        borderStyle: "dashed",
                        borderRadius: 1,
                        marginTop: 32
                    }}
                >
                    <Text
                        style={{
                            textAlign: "center",
                            fontFamily: "Gotham-Bold",
                            fontSize: 11,
                            letterSpacing: 1,
                            color: colors.blueyGrey
                        }}
                    >
                        Add New Contact
                    </Text>
                </TouchableOpacity>
                <View style={styles.leadsContainer}>
                    {contacts.map(item => (
                        <ContactItem key={item.id} item={item} />
                    ))}
                </View>
            </View>
        </View>
    );
}
