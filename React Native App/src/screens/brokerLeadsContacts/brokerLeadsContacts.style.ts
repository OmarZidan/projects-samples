import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    titleContainer: {
        marginTop: 150,
        marginBottom: 24,
        marginHorizontal: 25
    },
    leadsContainer: {
        marginTop: 28,
        marginHorizontal: 32,
        marginBottom: 85
    },
    tabTitle: {
        fontSize: 16,
        fontFamily: "Gotham-SemiBold",
        paddingVertical: 13,
        textAlign: "center"
    },
    tabsContainer: {
        marginTop: 150,
        flexDirection: "row",
        justifyContent: "center",
        marginHorizontal: 25,
        marginBottom: 32
    }
});

export default styles;
