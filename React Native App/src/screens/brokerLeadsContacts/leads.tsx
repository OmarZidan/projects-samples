import React from "react";
import { Text, View, ScrollView, Image } from "react-native";
import styles from "./brokerLeadsContacts.style";
import { Title, Button, BottomNavBar, Header, SearchboxAndFilters, LeadItem } from "@components";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";

export default function BrokerLeads(): React.ReactElement {
    const { navigate } = useNavigation();
    const leads = [
        {
            id: "1",
            title: "Yara Shawky Ahmed",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "Contracted"
        },
        {
            id: "2",
            title: "Dania Hussien Ali",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "Down Payment"
        },
        {
            id: "3",
            title: "Samir Salem Fakhoury",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "Not Interested"
        },
        {
            id: "4",
            title: "Abdelrahman Fares",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "URF"
        }
    ];

    return (
        <View style={{ flex: 1 }}>
            <View>
                <View>
                    <SearchboxAndFilters
                        onPress={() => {
                            navigate("BrokerLeadsFilters");
                        }}
                    />
                </View>
                <View style={styles.leadsContainer}>
                    {leads.map(lead => (
                        <LeadItem key={lead.id} lead={lead} />
                    ))}
                </View>
            </View>
        </View>
    );
}
