import React, { useState, useEffect } from "react";
import { Text, View, TouchableWithoutFeedback, Image, ActivityIndicator } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { Title, Button, BottomNavBar, Header, SearchboxAndFilters, LeadItem } from "@components";
import styles from "./brokerLeadsContacts.style";
import Leads from "./leads";
import Contacts from "./contacts";
import { colors } from "@utils";

interface SingleProjectProps {
    navigation: any;
}

function BrokerLeadsContacts({ navigation }: SingleProjectProps) {
    const [tabIndex, setTabIndex] = useState(0);
    const [brokerTabs, setBrokerTabs] = useState([
        {
            title: "Leads",
            // eslint-disable-next-line react/display-name
            render: () => <Leads />
        },
        {
            title: "Contacts",
            // eslint-disable-next-line react/display-name
            render: () => <Contacts />
        }
    ]);

    return (
        <View style={styles.container}>
            <ScrollView style={styles.container}>
                <Header withBack={false} />
                <View style={styles.tabsContainer}>
                    {brokerTabs.map(({ title }, i) => (
                        <TouchableWithoutFeedback
                            key={`tabPress_${i}`}
                            onPress={() => setTabIndex(i)}
                        >
                            <View
                                style={{
                                    width: "50%",
                                    flex: 1
                                }}
                            >
                                <Text
                                    style={[
                                        styles.tabTitle,
                                        { color: tabIndex === i ? colors.dark : colors.blueyGrey },
                                        {
                                            backgroundColor:
                                                tabIndex === i ? colors.white : colors.lightBlueGrey
                                        }
                                    ]}
                                >
                                    {title}
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    ))}
                </View>
                <View>{Boolean(brokerTabs.length) && brokerTabs[tabIndex].render()}</View>
            </ScrollView>
            <BottomNavBar currentRoute="leads" />
        </View>
    );
}

export default BrokerLeadsContacts;
