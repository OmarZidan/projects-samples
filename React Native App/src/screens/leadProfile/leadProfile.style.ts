import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    content: {
        backgroundColor: colors.white50,
        marginTop: 150,
        marginHorizontal: 25,
        paddingTop: 27,
        paddingBottom: 12
    },
    userInfo: {
        marginHorizontal: 25
    },
    userTitle: {
        fontSize: 18,
        letterSpacing: 0.82,
        color: colors.dark,
        marginBottom: 6
    },
    userSubTitle: {
        flexDirection: "row"
    },
    userSubTitleText: {
        fontSize: 10,
        letterSpacing: 0,
        color: colors.charcoalGrey
    },
    HLine: {
        borderBottomColor: colors.charcoalGrey16,
        borderBottomWidth: 1,
        marginVertical: 30
    },
    titleContainer: {
        marginTop: 150,
        marginBottom: 24,
        marginHorizontal: 25
    },
    leadsContainer: {
        // marginTop: 28,
        marginHorizontal: 7,
        marginBottom: 85
    }
});

export default styles;
