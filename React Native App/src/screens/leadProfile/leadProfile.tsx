import React from "react";
import { View, ScrollView, Image } from "react-native";
import styles from "./leadProfile.style";
import {
    Text,
    Title,
    Button,
    BottomNavBar,
    Header,
    ContactLead,
    LeadProfileItem
} from "@components";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";

export default function LeadProfile(): React.ReactElement {
    const { navigate } = useNavigation();
    const leads = [
        {
            id: "1",
            title: "Yara Shawky Ahmed",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "Contracted"
        },
        {
            id: "2",
            title: "Dania Hussien Ali",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "Down Payment"
        },
        {
            id: "3",
            title: "Samir Salem Fakhoury",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "Not Interested"
        },
        {
            id: "4",
            title: "Abdelrahman Fares",
            image: require("@assets/images/serrano.png"),
            email: "1@1.com",
            phone: "01117037513",
            status: "URF"
        }
    ];

    const user = {
        id: "1",
        title: "Yara Shawky Ahmed",
        image: require("@assets/images/serrano.png"),
        email: "1@1.com",
        phone: "01117037513",
        status: "Contracted"
    };

    return (
        <View style={{ flex: 1 }}>
            <ScrollView style={styles.container}>
                <Header withBack={false} />
                <View style={styles.content}>
                    <View style={styles.userInfo}>
                        <Text weight="ExtraBold" style={styles.userTitle}>
                            {user.title}
                        </Text>
                        <View style={styles.userSubTitle}>
                            <Text
                                weight="Medium"
                                style={[styles.userSubTitleText, { marginRight: 16 }]}
                            >
                                {user.email}
                            </Text>
                            <Text weight="Medium" style={styles.userSubTitleText}>
                                {user.phone}
                            </Text>
                        </View>
                        <ContactLead
                            lead={{}}
                            containerStyles={{
                                marginBottom: 25,
                                marginTop: 22,
                                marginHorizontal: 20
                            }}
                        />
                        <Button
                            text="Activity Log"
                            backgroundColor={colors.dark}
                            textColor={colors.white}
                            fontSize={11}
                            weight="SemiBold"
                        />
                    </View>
                    <View style={styles.HLine} />
                    <View>
                        <View style={styles.leadsContainer}>
                            {leads.map(lead => (
                                <LeadProfileItem key={lead.id} lead={lead} />
                            ))}
                        </View>
                    </View>
                </View>
                {/* <LeadProfileUser user={user} /> */}
            </ScrollView>
            <BottomNavBar currentRoute="leads" />
        </View>
    );
}
