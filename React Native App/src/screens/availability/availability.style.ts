import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.lightBlueGrey,
        flex: 1
    },
    titleContainer: {
        justifyContent: "flex-start",
        width: "100%",
        marginTop: 150,
        marginBottom: 30,
        paddingLeft: 25
    },
    listOfUnits: {
        width: "100%",
        padding: 25,
        paddingBottom: 75
    },

    unitContainer: {
        borderRadius: 1,
        width: "100%",
        marginBottom: 20
    },
    unitInnerContainer: {
        padding: 15,
        backgroundColor: "white"
    },
    unitCardHeader: {
        borderBottomWidth: 1,
        borderColor: colors.paleLilac,
        backgroundColor: "white",
        borderRadius: 1,
        width: "100%",
        // paddingBottom: 20,
        paddingTop: 15,
        paddingBottom: 10,
        // paddingHorizontal: 15,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center"
    },
    unitOwnerLogo: {
        shadowColor: colors.charcoalGrey,
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowOpacity: 0.25,
        shadowRadius: 8,
        elevation: 8,
        width: 70,
        height: 70,
        borderWidth: 1,
        borderColor: "transparent"
    },
    unitReady: {
        borderRadius: 1,
        backgroundColor: colors.sickGreen,
        width: "100%",
        fontFamily: "Gotham-Regular"
    },
    unitResale: {
        backgroundColor: colors.blueyGrey,
        borderRadius: 1,
        width: "100%",
        fontFamily: "Gotham-Regular"
    },
    unitDelivered: {
        backgroundColor: colors.offWhite,
        borderRadius: 1,
        width: "100%",
        fontFamily: "Gotham-Regular"
    },
    unitOffPlan: {
        backgroundColor: colors.redWine,
        borderRadius: 1,
        width: "100%",
        fontFamily: "Gotham-Regular"
    },
    unitFlagText: { textAlign: "center", color: "white" },
    unitOwnerLogoImage: { width: "100%", height: "100%", resizeMode: "cover" },
    unitDetails: {
        paddingLeft: 24,
        alignItems: "flex-start",
        width: "70%"
    },
    unitTitle: {
        fontFamily: "Gotham-Bold",
        fontSize: 13,
        color: colors.dark,
        marginBottom: 6
    },
    unitType: {
        fontFamily: "Gotham-Regular",
        fontSize: 8,
        color: colors.offWhite,
        marginBottom: 6
    },
    unitNumbers: {
        marginTop: 10,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%"
    },
    unitCardFooter: {
        borderRadius: 1,
        backgroundColor: "white",
        width: "100%",
        padding: 12,
        alignItems: "center",
        justifyContent: "center"
    },
    cardFooterUpper: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    unitPrice: {
        backgroundColor: "white",
        width: "50%"
        // paddingVertical: 10
    },
    unitPriceTitleCont: {
        fontSize: 8,
        color: colors.charcoalGrey90,
        fontFamily: "Gotham-Light"
    },
    unitPriceText: { fontSize: 10, color: colors.charcoalGrey, fontFamily: "Gotham-SemiBold" },
    unitPaymentTitleCont: {
        backgroundColor: "white",
        width: "50%"
    },

    unitPaymentText: {
        fontSize: 8,
        color: colors.charcoalGrey90,
        fontFamily: "Gotham-Light"
    },
    unitPaymentYearsText: {
        fontSize: 10,
        color: colors.charcoalGrey,
        fontFamily: "Gotham-Light"
    },
    cardFooterDown: {
        flexDirection: "row",
        // alignItems: "center",
        justifyContent: "space-between"
    },

    availabilityButton: {
        // width: "100%",
        // height: 40,
        // flexBasis: "48%",
        shadowColor: colors.charcoalGrey,
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 8,
        borderWidth: 1,
        borderColor: "transparent"
    }
});

export default styles;
