import React from "react";
import { View, Image, Text, ImageURISource } from "react-native";
import styles from "./availability.style";
import { colors } from "@utils";

interface DataWithLogoProps {
    iconPath: ImageURISource;
    dataText: string;
}
export default function DataWithLogo({
    iconPath,
    dataText
}: DataWithLogoProps): React.ReactElement {
    return (
        <View
            style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                marginBottom: 3
            }}
        >
            <View style={{}}>
                <Image style={{ width: 10, height: 10, resizeMode: "contain" }} source={iconPath} />
            </View>
            <Text
                style={{
                    fontSize: 8,
                    marginLeft: 3,
                    color: colors.dark,
                    fontFamily: "Gotham-Light"
                }}
            >
                {dataText}
            </Text>
        </View>
    );
}
