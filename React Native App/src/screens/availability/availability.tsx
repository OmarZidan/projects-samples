import React from "react";
import { Text, View, TouchableOpacity, Image, TextInput, FlatList } from "react-native";
import styles from "./availability.style";
// import AvailabilityHeader from "./NOTUSEDavailability-header";
// import DataWithLogo from "./data-with-logo";
import { Title, Button, SearchboxAndFilters, BottomNavBar, Header } from "@components";
import Unit from "./unit";
import { ScrollView } from "react-native-gesture-handler";
import { useNavigation } from "react-navigation-hooks";
type itemFlag = "ready" | "resale" | "delivered" | "offPlan";
const UnitsData = [
    {
        id: "1",
        flag: "ready",
        logo: require("@assets/icons/avLogo1.png"),
        unitTitle: "Penthouse",
        unitType: "NewPlan Developments - Residential",
        unitLocation: "13 El-morshedy, New Capital, Cairo",
        numberOfBeds: "05",
        numberOfBaths: "04",
        areaInMeterSquare: 230,
        price: "EGP 100,000,000",
        duration: "5 - 10 Years"
    },
    {
        id: "2",
        flag: "resale",
        logo: require("@assets/icons/avLogo2.png"),
        unitTitle: "Twin Villa",
        unitType: "NewPlan Developments - Residential",
        unitLocation: "13 El-morshedy, New Capital, Cairo",
        numberOfBeds: "06",
        numberOfBaths: "04",
        areaInMeterSquare: 1020,
        price: "EGP 10000,000,000",
        duration: "5 - 10 Years"
    },
    {
        id: "3",
        flag: "delivered",
        logo: require("@assets/icons/avLogo3.png"),
        unitTitle: "Duplex",
        unitType: "NewPlan Developments - Commercial",
        unitLocation: "13 El-morshedy, New Capital, Cairo",
        numberOfBeds: "05",
        numberOfBaths: "04",
        areaInMeterSquare: 230,
        price: "EGP 100,000,000",
        duration: "5 - 10 Years"
    },
    {
        id: "4",
        flag: "offPlan",
        logo: require("@assets/icons/avLogo4.jpg"),
        unitTitle: "Townhouse",
        unitType: "NewPlan Developments - Residential",
        unitLocation: "13 El-morshedy, New Capital, Cairo",
        numberOfBeds: "02",
        numberOfBaths: "04",
        areaInMeterSquare: 100,
        price: "EGP 100,000,000",
        duration: "5 - 10 Years"
    }
];
export default function Availability(): React.ReactElement {
    const { navigate } = useNavigation();
    return (
        <View style={{ flex: 1 }}>
            <ScrollView
                style={styles.container}
                contentContainerStyle={{
                    alignItems: "center",
                    justifyContent: "center"
                }}
            >
                <Header withBack={false} />
                <View style={styles.titleContainer}>
                    <Title fontSize={24} weight="ExtraBold">
                        Availability
                    </Title>
                </View>
                <SearchboxAndFilters
                    onPress={() => {
                        navigate("AvailabilityFilters");
                    }}
                />
                <View style={styles.listOfUnits}>
                    {/* <View style={styles.unitsInnerContainer}> */}
                    <FlatList
                        data={UnitsData}
                        keyExtractor={(item): string => {
                            return item.id;
                        }}
                        renderItem={({ item }): React.ReactElement => (
                            <Unit
                                flag={item.flag as itemFlag}
                                logo={item.logo}
                                unitTitle={item.unitTitle}
                                unitType={item.unitType}
                                unitLocation={item.unitLocation}
                                numberOfBeds={item.numberOfBeds}
                                numberOfBaths={item.numberOfBaths}
                                areaInMeterSquare={item.areaInMeterSquare}
                                price={item.price}
                                duration={item.duration}
                                onPress={() => {
                                    navigate("SingleAvUnit");
                                }}
                            />
                        )}
                    />
                </View>
            </ScrollView>
            <BottomNavBar currentRoute="Availability" />
        </View>
    );
}
