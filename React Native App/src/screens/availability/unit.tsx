import React from "react";
import { Text, View, TouchableOpacity, Image, TextInput, ImageURISource } from "react-native";
import styles from "./availability.style";
// import AvailabilityHeader from "./availability-header";
import DataWithLogo from "./data-with-logo";
import { Title, Button } from "@components";
import { colors } from "@utils";

interface UnitProps {
    // id: number;
    flag: "ready" | "resale" | "delivered" | "offPlan";
    logo: ImageURISource;
    unitTitle: string;
    unitType: string;
    unitLocation: string;
    numberOfBeds: string;
    numberOfBaths: string;
    areaInMeterSquare: number;
    price: string;
    duration: string;
    onPress: () => void;
}
export default function Unit({
    // id,
    flag,
    logo,
    unitTitle,
    unitType,
    unitLocation,
    numberOfBeds,
    numberOfBaths,
    areaInMeterSquare,
    price,
    duration,
    onPress
}: UnitProps): React.ReactElement {
    const unitFlag = (flag): React.ReactElement => {
        switch (flag) {
            case "ready":
                return (
                    <View style={styles.unitReady}>
                        <Text style={styles.unitFlagText}>Ready To Move</Text>
                    </View>
                );
            case "resale":
                return (
                    <View style={styles.unitResale}>
                        <Text style={styles.unitFlagText}>Re-sale</Text>
                    </View>
                );
            case "delivered":
                return (
                    <View style={styles.unitDelivered}>
                        <Text style={styles.unitFlagText}>Delivered</Text>
                    </View>
                );
            case "offPlan":
                return (
                    <View style={styles.unitOffPlan}>
                        <Text style={styles.unitFlagText}>Off Plan</Text>
                    </View>
                );
        }
    };

    return (
        <View style={styles.unitContainer}>
            {/* Flag */}
            {unitFlag(flag)}
            {/* InnerCard */}
            <View style={styles.unitInnerContainer}>
                {/* Card Head */}
                <View style={styles.unitCardHeader}>
                    {/* Card Head  --> itemLogo */}
                    <View style={styles.unitOwnerLogo}>
                        <Image style={styles.unitOwnerLogoImage} source={logo} />
                    </View>
                    {/* Card Head  --> itemData */}
                    <View style={styles.unitDetails}>
                        <View>
                            <Text style={styles.unitTitle}>{unitTitle}</Text>
                        </View>
                        <View>
                            <Text style={styles.unitType}>{unitType}</Text>
                        </View>
                        <DataWithLogo
                            iconPath={require("@assets/icons/mapPlaceholder.png")}
                            dataText={unitLocation}
                        />
                        <View style={styles.unitNumbers}>
                            <DataWithLogo
                                iconPath={require("@assets/icons/doubleBed.png")}
                                dataText={numberOfBeds}
                                // key="1"
                            />
                            <DataWithLogo
                                iconPath={require("@assets/icons/bathtub.png")}
                                dataText={numberOfBaths}
                                // key="1"
                            />
                            <DataWithLogo
                                iconPath={require("@assets/icons/shape.png")}
                                dataText={"" + areaInMeterSquare + String.fromCharCode(0x33a1)}
                                // key="1"
                            />
                        </View>
                    </View>
                </View>
                {/* Card Tail */}
                <View style={styles.unitCardFooter}>
                    {/* ------------------------Card Tail Upper----------------------------------------------------- */}

                    <View style={styles.cardFooterUpper}>
                        {/* Card Tail Price */}
                        <View style={styles.unitPrice}>
                            <View>
                                <Text style={styles.unitPriceTitleCont}>Price :</Text>
                            </View>
                            <View>
                                <Text style={styles.unitPriceText}>{price}</Text>
                            </View>
                        </View>
                        <View style={styles.unitPaymentTitleCont}>
                            <View>
                                <Text style={styles.unitPaymentText}>Payment Plan duration:</Text>
                            </View>
                            <View>
                                <Text style={styles.unitPaymentYearsText}>{duration}</Text>
                            </View>
                        </View>
                    </View>
                    {/* ------------------------Card Tail Footer----------------------------------------------------- */}
                </View>
                <View style={styles.cardFooterDown}>
                    <View style={{ width: "48%" }}>
                        <Button
                            text="Add Lead"
                            backgroundColor={colors.dark}
                            textColor={colors.white}
                            fontSize={12}
                            containerStyles={styles.availabilityButton}
                        />
                    </View>
                    <View style={{ width: "48%" }}>
                        <Button
                            text="View Details"
                            backgroundColor={colors.white}
                            textColor={colors.dark}
                            containerStyles={styles.availabilityButton}
                            onPress={onPress}
                        />
                    </View>
                </View>
            </View>
        </View>
    );
}
