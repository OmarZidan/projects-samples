import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    createPasswordContainer: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    titleContainer: {
        marginTop: 150
    },
    bgImg: { width: "100%", height: "100%", opacity: 0.8 },
    HLine: {
        borderBottomColor: colors.charcoalGrey16,
        borderBottomWidth: 1,
        marginVertical: 16
    },
    recoveryText: {
        fontSize: 12,
        lineHeight: 20,
        letterSpacing: 1.33,
        color: colors.dark,
        marginBottom: 55
    },
    otpContainer: {
        backgroundColor: colors.white,
        borderRadius: 2,
        height: 135,
        justifyContent: "center",
        alignItems: "center"
    },
    borderStyleBase: {
        width: 30,
        height: 45
        // color: colors.lightBlueGrey
    },

    borderStyleHighLighted: {
        borderColor: colors.dark,
        borderRadius: 2
        // color: colors.lightBlueGrey
    },

    underlineStyleBase: {
        width: 60,
        height: 60,
        borderWidth: 0,
        borderBottomWidth: 3,
        fontSize: 48,
        color: colors.dark,
        fontFamily: "Gotham-Bold"
    },

    underlineStyleHighLighted: {
        borderColor: colors.dark,
        borderRadius: 2
    },
    timer: {
        fontSize: 11,
        letterSpacing: 0,
        color: colors.offWhite,
        textAlign: "right",
        marginTop: 24
    }
});

export default styles;
