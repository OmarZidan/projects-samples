import React, { useState, useEffect } from "react";
import { View, ScrollView, ImageBackground } from "react-native";
import { Text, Header, Button, Title } from "@components";
import styles from "./recovery-code.style";
import misritalia from "@config/api";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";
import OTPInputView from "@twotalltotems/react-native-otp-input";

export default function CreatePassword(): React.ReactElement {
    const [timer, setTimer] = useState(15);

    setTimeout(() => {
        if (timer !== 0) {
            const newTimer = timer - 1;
            setTimer(newTimer);
        }
    }, 1000);

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={require("@assets/images/filtersBG.png")} style={styles.bgImg}>
                <Header withDrawerMenu={false} />
                <ScrollView>
                    <View style={{ paddingHorizontal: 25 }}>
                        <View style={styles.titleContainer}>
                            <Title fontSize={22} weight="Bold" textTransform="capitalize">
                                Enter Recovery Code
                            </Title>
                        </View>
                        <View style={styles.HLine} />
                        <Text weight="Medium" style={styles.recoveryText}>
                            Enter the 4-digits code sent on your mobile number
                        </Text>
                        <View style={styles.otpContainer}>
                            <OTPInputView
                                style={{ width: "100%", paddingHorizontal: 15 }}
                                pinCount={4}
                                autoFocusOnLoad
                                codeInputFieldStyle={styles.underlineStyleBase}
                                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                onCodeFilled={code => {
                                    console.log(`Code is ${code}, you are good to go!`);
                                }}
                            />
                        </View>
                        {timer > 9 && (
                            <Text weight="Regular" style={styles.timer}>
                                code expires in: 00:{timer}
                            </Text>
                        )}
                        {timer <= 9 && (
                            <Text weight="Regular" style={styles.timer}>
                                code expires in: 00:0{timer}
                            </Text>
                        )}
                        <View style={{ marginHorizontal: 50, marginTop: 40 }}>
                            <Button
                                text="Resend Code"
                                gradient
                                fontSize={13}
                                weight="SemiBold"
                                textColor={colors.white}
                                // onPress={() => {
                                //     setSuccessModalVisible(true);
                                // }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
