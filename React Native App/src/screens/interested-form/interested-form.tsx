/* eslint-disable @typescript-eslint/camelcase */
import React, { useState, useContext, useEffect } from "react";
import {
    View,
    ScrollView,
    ImageBackground,
    Modal,
    TouchableWithoutFeedback,
    Image
} from "react-native";
import { Text, Header, Button, Title, TextInput, PhoneInput, Select } from "@components";
import styles from "./interested-form.style";
import newPlan from "@config/api";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";
import { AuthContext } from "./../../contexts/AuthContext";

interface InterestedFormProps {
    navigation: any;
}

export default function InterestedForm({ navigation }: InterestedFormProps): React.ReactElement {
    const [user, setUser] = useContext(AuthContext);
    const { navigate } = useNavigation();
    const [successModalVisible, setSuccessModalVisible] = useState(false);
    const [errorMessages, setErrorMessages] = useState({
        name: "",
        email: "",
        mobile_number: "",
        project_id: ""
    });

    const [form, setForm] = useState({
        name: "",
        email: "",
        mobile_number: "",
        project_id: ""
    });

    useEffect(() => {
        const id = navigation.getParam("id");
        // console.log(id);
        const newState = { ...form };
        newState["project_id"] = id;
        setForm(newState);
    }, []);

    const changeForm = (property, newValue) => {
        setErrorMessages({ name: "", email: "", mobile_number: "", project_id: "" });
        const newState = { ...form };
        newState[property] = newValue;
        setForm(newState);
    };

    const setSuccessVisible = visible => {
        setSuccessModalVisible(visible);
    };

    const handleSubmit = async () => {
        console.log(form);

        try {
            const response = await newPlan.post("/interested_lead", form);
            if (response.status === 201) {
                setSuccessModalVisible(true);
            }
        } catch (error) {
            console.log(error.response);
            const statusCode = error.response.status;
            if (statusCode === 422) {
                setErrorMessages(error.response.data.error.message);
            }
        }
    };

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={require("@assets/images/filtersBG.png")} style={styles.bgImg}>
                <Header withBack={true} withDrawerMenu={false} />
                <ScrollView>
                    <View style={{ paddingHorizontal: 25 }}>
                        <View style={styles.titleContainer}>
                            <Title fontSize={22} weight="Bold" textTransform="capitalize">
                                Join Our Community
                            </Title>
                        </View>
                        <View style={styles.HLine} />
                        <TextInput
                            placeholder="Name"
                            placeholderColor={colors.charcoalGrey}
                            style={{ marginTop: 30, marginBottom: 2 }}
                            value={form.name}
                            onChangeText={val => changeForm("name", val)}
                            errorMessage={errorMessages.name}
                        />
                        <PhoneInput
                            placeholder="Mobile No."
                            style={{ marginBottom: 2, marginTop: 30 }}
                            height={42}
                            value={form.mobile_number || "+20"}
                            onChangePhoneNumber={val => changeForm("mobile_number", val)}
                            errorMessage={errorMessages.mobile_number}
                        />
                        <TextInput
                            placeholder="Email"
                            keyboardType="email-address"
                            placeholderColor={colors.charcoalGrey}
                            value={form.email}
                            style={{ marginTop: 30 }}
                            onChangeText={val => changeForm("email", val)}
                            errorMessage={errorMessages.email}
                        />
                        {/* <View style={{ marginTop: 30, marginBottom: 2 }}>
                            <Select
                                placeholder={{
                                    label: "Project interested in",
                                    vlaue: null
                                }}
                                onValueChange={val => changeForm("project_id", val)}
                                items={[
                                    { label: "First", value: "9" },
                                    { label: "Second", value: "10" }
                                ]}
                                errorMessage={errorMessages.project_id}
                            />
                        </View> */}
                        <View style={{ marginHorizontal: 50, marginTop: 50 }}>
                            <Button
                                text="Submit"
                                gradient
                                fontSize={13}
                                weight="SemiBold"
                                textColor={colors.white}
                                onPress={handleSubmit}
                            />
                        </View>
                    </View>

                    <View>
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={successModalVisible}
                        >
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    setSuccessVisible(!successModalVisible);
                                }}
                            >
                                <View style={styles.successModalContainer}>
                                    <TouchableWithoutFeedback>
                                        <View style={styles.successModal}>
                                            <Image
                                                source={require("@assets/icons/padlock.png")}
                                                style={{ height: 192, width: 140 }}
                                            />
                                            <Text
                                                weight="Bold"
                                                style={{
                                                    fontSize: 32,
                                                    letterSpacing: 1,
                                                    color: colors.white,
                                                    textAlign: "center",
                                                    marginTop: 40
                                                }}
                                            >
                                                SUCCESS!
                                            </Text>
                                            <Text
                                                weight="Regular"
                                                style={{
                                                    fontSize: 14,
                                                    letterSpacing: 0,
                                                    lineHeight: 20,
                                                    color: colors.white,
                                                    textAlign: "center",
                                                    marginTop: 14,
                                                    marginHorizontal: 35
                                                }}
                                            >
                                                Your Request has been sent.
                                            </Text>
                                            <View style={styles.cardFooterDown}>
                                                <View style={{ width: "43%", marginHorizontal: 5 }}>
                                                    <Button
                                                        text="Back to Projects"
                                                        backgroundColor={colors.white}
                                                        textColor={colors.offWhite}
                                                        fontSize={11}
                                                        borderWidth={1}
                                                        borderColor={colors.white}
                                                        containerStyles={styles.availabilityButton}
                                                        onPress={() => {
                                                            navigate("Projects");
                                                        }}
                                                    />
                                                </View>
                                                <View style={{ width: "43%", marginHorizontal: 5 }}>
                                                    <Button
                                                        text="Back to Home"
                                                        backgroundColor="transparent"
                                                        borderColor={colors.white}
                                                        textColor={colors.white}
                                                        fontSize={11}
                                                        borderWidth={1}
                                                        containerStyles={styles.availabilityButton}
                                                        onPress={() => {
                                                            navigate("Home");
                                                        }}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
