import React, { useState } from "react";
import { View, TouchableOpacity, ScrollView, Image, Platform, Linking } from "react-native";
import { useNavigation } from "react-navigation-hooks";
import styles from "./home.style";
import { Header, Text, Button, Title } from "@components";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { colors, getAssetUrl } from "@utils";

export default function Home(): React.ReactElement {
    const [activeSlide, setActiveSlide] = useState(0);
    const { navigate } = useNavigation();
    const gallery = [
        {
            path: require("@assets/images/DummyProjects/vinci1.png"),
            title: "IL BOSCO"
        },
        {
            path: require("@assets/images/DummyProjects/lanuova.jpg"),
            title: "IL BOSCO"
        }
        // {
        //     path: "/storage/DCAh14FxC1kS0FX73LPRpgFyb4IO9b1XgzRXgb5g.jpeg",
        //     title: "IL BOSCO"
        // },
        // {
        //     path: "/storage/GEEpUgOcuNhP0RfayDV7DrKlXb7N5TD0n27CXBrC.jpeg",
        //     title: "IL BOSCO"
        // },
        // {
        //     path: "/storage/g0LRyDUvqKtJSf5qCbQk2Sj7PYFVZSYQZRhjBtra.jpeg",
        //     title: "IL BOSCO"
        // },
        // {
        //     path: "/storage/8lxqhEj7lUauT2WJ0fyT0FBCCmDdymLFEYdzyuLU.jpeg",
        //     title: "IL BOSCO"
        // }
    ];

    interface ItemProps {
        item: any;
    }

    const renderItem = ({ item }: ItemProps) => {
        return (
            <View style={{ height: 250 }}>
                <Image
                    style={{
                        height: "100%",
                        width: "100%",
                        resizeMode: "cover"
                    }}
                    source={item.path}
                />
            </View>
        );
    };

    const dialCall = () => {
        let phoneNumber = "";
        if (Platform.OS === "android") {
            phoneNumber = "tel:${16534}";
        } else {
            phoneNumber = "telprompt:${16534}";
        }
        Linking.openURL(phoneNumber);
    };

    return (
        <View style={{ flex: 1 }}>
            <ScrollView style={styles.container}>
                <Header withBack={false} withDrawerMenu={false} />
                <View style={styles.conceptBackground}>
                    <View style={styles.galleryContainer}>
                        <Carousel
                            ref={c => {
                                this._carousel = c;
                            }}
                            data={gallery}
                            renderItem={renderItem}
                            sliderWidth={310}
                            itemWidth={310}
                            onSnapToItem={index => setActiveSlide(index)}
                        />
                        <Pagination
                            carouselRef={this._carousel}
                            tappableDots
                            dotsLength={gallery.length}
                            activeDotIndex={activeSlide}
                            containerStyle={styles.pagination}
                            dotStyle={styles.dots}
                            inactiveDotStyle={styles.activeDots}
                            inactiveDotOpacity={0.9}
                            inactiveDotScale={0.9}
                        />
                    </View>
                    <Image
                        style={styles.goldDots}
                        source={require("@assets/images/goldDots.png")}
                    />
                </View>
                <View style={styles.knowMore}>
                    <Button
                        backgroundColor={colors.dark}
                        textColor={colors.white}
                        text="Know More"
                        fontSize={12}
                        weight="Medium"
                        onPress={(): void => {
                            navigate("Projects");
                        }}
                    />
                </View>
                <View style={styles.titleContainer}>
                    <Title fontSize={16} weight="Bold">
                        Discover
                    </Title>
                </View>
                <View style={styles.itemsContainer}>
                    <View>
                        <TouchableOpacity
                            style={styles.discoverItem}
                            onPress={(): void => {
                                navigate("Projects");
                            }}
                        >
                            <Image
                                source={require("@assets/icons/house.png")}
                                style={styles.itemIcon}
                            />
                        </TouchableOpacity>
                        <Text weight="Medium" style={styles.itemText}>
                            Projects
                        </Text>
                    </View>
                    <View>
                        <TouchableOpacity
                            style={styles.discoverItem}
                            onPress={(): void => {
                                navigate("Press");
                            }}
                        >
                            <Image
                                source={require("@assets/icons/calendar.png")}
                                style={styles.itemIcon}
                            />
                        </TouchableOpacity>
                        <Text weight="Medium" style={styles.itemText}>
                            Press Center
                        </Text>
                    </View>
                    <View>
                        <TouchableOpacity
                            style={styles.discoverItem}
                            onPress={(): void => {
                                navigate("ConstructionUpdates");
                            }}
                        >
                            <Image
                                source={require("@assets/icons/updates.png")}
                                style={styles.itemIcon}
                            />
                        </TouchableOpacity>
                        <Text weight="Medium" style={styles.itemText}>
                            Construction{"\n"}Updates
                        </Text>
                    </View>
                    <View>
                        <TouchableOpacity
                            style={styles.discoverItem}
                            // onPress={() => {
                            //     dialCall();
                            // }}
                            onPress={(): void => {
                                navigate("Contact");
                            }}
                        >
                            <Image
                                source={require("@assets/icons/phoneCall2.png")}
                                style={styles.itemIcon}
                            />
                        </TouchableOpacity>
                        <Text weight="Medium" style={styles.itemText}>
                            Contact
                        </Text>
                    </View>
                </View>
            </ScrollView>
            {/* <View style={styles.footer}>
                <View style={{ width: "100%" }}>
                    <Button
                        backgroundColor={colors.dark}
                        textColor={colors.white}
                        text="Sign In"
                        onPress={(): void => {
                            navigate("SignIn");
                        }}
                    />
                </View>
            </View> */}
        </View>
    );
}
