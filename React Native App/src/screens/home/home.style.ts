import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    conceptBackground: {
        flex: 1,
        marginTop: 140,
        paddingTop: 30
    },
    goldDots: {
        height: 240,
        width: 240,
        position: "absolute",
        marginLeft: 25,
        resizeMode: "contain"
    },
    titleContainer: {
        marginHorizontal: 25,
        marginTop: 55,
        marginBottom: 25
    },
    galleryContainer: { alignSelf: "flex-end", elevation: 8, zIndex: 999 },
    pagination: {
        backgroundColor: "transparent",
        width: "100%",
        position: "absolute",
        bottom: 0,
        alignSelf: "center"
    },
    dots: {
        width: 7,
        height: 7,
        borderRadius: 0,
        marginHorizontal: 0,
        backgroundColor: colors.white
    },
    activeDots: {
        width: 7,
        borderRadius: 0,
        height: 7,
        borderWidth: 1,
        marginHorizontal: 0,
        borderColor: colors.white,
        backgroundColor: "transparent"
    },
    knowMore: {
        width: 130,
        alignSelf: "flex-end",
        marginHorizontal: 32,
        marginTop: -20,
        elevation: 99,
        zIndex: 99
    },
    discoverItem: {
        backgroundColor: colors.white,
        height: 135,
        width: 135,
        // height: 90,
        // width: 90,
        justifyContent: "center",
        alignItems: "center",
        shadowColor: colors.blueyGrey,
        shadowOffset: {
            width: 0,
            height: 5
        },
        shadowRadius: 4,
        shadowOpacity: 1,
        elevation: 2
    },
    itemText: {
        textAlign: "center",
        // fontSize: 13,
        fontSize: 15,
        color: colors.dark,
        letterSpacing: 0.46,
        marginTop: 15,
        marginBottom: 25
    },
    // itemIcon: { height: 42, width: 42, tintColor: colors.offWhite },
    itemIcon: { height: 50, width: 50, tintColor: colors.offWhite },
    itemsContainer: {
        flexDirection: "row",
        marginHorizontal: 35,
        flexWrap: "wrap",
        justifyContent: "space-between"
    },
    footer: {
        flexDirection: "row",
        flex: 1,
        justifyContent: "space-between",
        position: "absolute",
        zIndex: 101,
        elevation: 8,
        bottom: 0,
        width: "100%",
        backgroundColor: colors.white,
        paddingVertical: 15,
        paddingHorizontal: 25
    }
});

export default styles;
