import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.lightBlueGrey,
        flex: 1
    },
    titleContainer: {
        justifyContent: "flex-start",
        width: "100%",
        marginTop: 150,
        marginBottom: 30,
        paddingHorizontal: 25
    },
    listOfUnits: {
        marginTop: 32,
        marginHorizontal: 32,
        paddingBottom: 75
    },

    ItemContainer: {
        borderRadius: 1,
        marginBottom: 24
    },
    dealInnerContainer: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: "white"
    },
    dealCardHeader: {
        borderBottomWidth: 1,
        borderColor: colors.paleLilac,
        borderRadius: 1,
        paddingBottom: 5,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center"
    },
    dealLogo: {
        shadowColor: colors.charcoalGrey,
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowOpacity: 0.25,
        shadowRadius: 8,
        elevation: 8,
        width: 70,
        height: 70,
        borderWidth: 1,
        borderColor: "transparent"
    },
    dealStatus: {
        width: 80,
        right: 0,
        position: "absolute",
        elevation: 8,
        zIndex: 8
    },
    dealContracted: {
        backgroundColor: colors.sickGreen
    },
    dealMeeting: {
        backgroundColor: colors.offWhite
    },
    dealInterested: {
        backgroundColor: colors.blueyGrey
    },
    unitFlagText: {
        textAlign: "center",
        color: "white",
        fontFamily: "Gotham-Regular",
        fontSize: 10,
        paddingVertical: 2
    },
    dealLogoImage: { width: "100%", height: "100%", resizeMode: "cover" },
    dealDetails: {
        paddingLeft: 24,
        alignItems: "flex-start",
        width: "70%"
    },
    dealTitle: {
        fontFamily: "Gotham-Bold",
        fontSize: 13,
        color: colors.dark,
        marginBottom: 5
    },
    dealSubtitle: {
        fontFamily: "Gotham-Light",
        fontSize: 9.5,
        color: colors.charcoalGrey,
        marginBottom: 5
    },
    unitNumbers: {
        marginTop: 10,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%"
    },
    unitCardFooter: {
        paddingVertical: 7
    },
    info: {
        letterSpacing: 0,
        color: colors.charcoalGrey90,
        fontFamily: "Gotham-Light"
    },
    infoTitle: {
        fontSize: 8
    },
    infoText: {
        fontSize: 10
    }
});

export default styles;
