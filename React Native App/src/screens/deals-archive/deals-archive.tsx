import React from "react";
import { Text, View, TouchableOpacity, Image, TextInput, FlatList } from "react-native";
import styles from "./deals-archive.style";
import { Title, Button, SearchboxAndFilters, BottomNavBar, Header } from "@components";
import Unit from "./deal-item";
import { ScrollView } from "react-native-gesture-handler";
import { useNavigation } from "react-navigation-hooks";
type itemFlag = "ready" | "resale" | "delivered" | "offPlan";
const UnitsData = [
    {
        id: "1",
        flag: "contracted",
        logo: require("@assets/icons/avLogo1.png"),
        dealTitle: "Manar Fathy",
        email: "username@domain.com",
        phone: "01017182566",
        unitType: "Penthouse",
        contractNumber: "77809-564-289-445",
        unitNumber: "A7 - 3389",
        commission: "EGP 605,600",
        dateAdded: "13/10/2019",
        price: "EGP 100,000,000"
    },
    {
        id: "2",
        flag: "meeting",
        logo: require("@assets/icons/avLogo2.png"),
        dealTitle: "Manar Fathy",
        email: "username@domain.com",
        phone: "01017182566",
        unitType: "Penthouse",
        contractNumber: "77809-564-289-445",
        unitNumber: "A7 - 3389",
        commission: "EGP 605,600",
        dateAdded: "13/10/2019",
        price: "EGP 100,000,000"
    },
    {
        id: "3",
        flag: "interested",
        logo: require("@assets/icons/avLogo3.png"),
        dealTitle: "Manar Fathy",
        email: "username@domain.com",
        phone: "01017182566",
        unitType: "Penthouse",
        contractNumber: "77809-564-289-445",
        unitNumber: "A7 - 3389",
        commission: "EGP 605,600",
        dateAdded: "13/10/2019",
        price: "EGP 100,000,000"
    },
    {
        id: "4",
        flag: "interested",
        logo: require("@assets/icons/avLogo4.jpg"),
        dealTitle: "Manar Fathy",
        email: "username@domain.com",
        phone: "01017182566",
        unitType: "Penthouse",
        contractNumber: "77809-564-289-445",
        unitNumber: "A7 - 3389",
        commission: "EGP 605,600",
        dateAdded: "13/10/2019",
        price: "EGP 100,000,000"
    }
];
export default function DealsArchive(): React.ReactElement {
    const { navigate } = useNavigation();
    return (
        <View style={{ flex: 1 }}>
            <ScrollView style={styles.container}>
                <Header withBack={false} />
                <View style={styles.titleContainer}>
                    <Title fontSize={24} weight="ExtraBold">
                        Deals
                    </Title>
                </View>
                <SearchboxAndFilters
                    onPress={() => {
                        navigate("DealsFilters");
                    }}
                />
                <View style={styles.listOfUnits}>
                    {/* <View style={styles.unitsInnerContainer}> */}
                    <FlatList
                        data={UnitsData}
                        keyExtractor={(item): string => {
                            return item.id;
                        }}
                        renderItem={({ item }): React.ReactElement => (
                            <Unit
                                flag={item.flag as itemFlag}
                                logo={item.logo}
                                email={item.email}
                                phone={item.phone}
                                dealTitle={item.dealTitle}
                                unitType={item.unitType}
                                contractNumber={item.contractNumber}
                                unitNumber={item.unitNumber}
                                commission={item.commission}
                                dateAdded={item.dateAdded}
                                price={item.price}
                                onPress={() => {
                                    navigate("SingleAvUnit");
                                }}
                            />
                        )}
                    />
                </View>
            </ScrollView>
            <BottomNavBar />
        </View>
    );
}
