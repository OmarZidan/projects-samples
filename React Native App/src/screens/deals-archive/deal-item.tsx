import React from "react";
import { Text, View, Image, ImageURISource } from "react-native";
import styles from "./deals-archive.style";
import { colors } from "@utils";

interface DeatItemProps {
    // id: number;
    flag: "ready" | "resale" | "delivered" | "offPlan";
    logo: ImageURISource;
    dealTitle: string;
    email: string;
    phone: string;
    unitType: string;
    contractNumber: string;
    unitNumber: string;
    commission: string;
    dateAdded: string;
    price: string;
    onPress: () => void;
}
export default function DealItem({
    // id,
    flag,
    logo,
    dealTitle,
    email,
    phone,
    unitType,
    contractNumber,
    unitNumber,
    price,
    commission,
    dateAdded,
    onPress
}: DeatItemProps): React.ReactElement {
    const unitFlag = (flag): React.ReactElement => {
        switch (flag) {
            case "contracted":
                return (
                    <View style={[styles.dealContracted, styles.dealStatus]}>
                        <Text style={styles.unitFlagText}>Contracted</Text>
                    </View>
                );
            case "meeting":
                return (
                    <View style={[styles.dealMeeting, styles.dealStatus]}>
                        <Text style={styles.unitFlagText}>Meeting</Text>
                    </View>
                );
            case "interested":
                return (
                    <View style={[styles.dealInterested, styles.dealStatus]}>
                        <Text style={styles.unitFlagText}>Interested</Text>
                    </View>
                );
        }
    };

    return (
        <View style={styles.ItemContainer}>
            <View style={styles.dealInnerContainer}>
                {unitFlag(flag)}
                <View style={styles.dealCardHeader}>
                    <View style={styles.dealLogo}>
                        <Image style={styles.dealLogoImage} source={logo} />
                    </View>
                    <View style={styles.dealDetails}>
                        <Text style={styles.dealTitle}>{dealTitle}</Text>
                        <Text style={styles.dealSubtitle}>{email}</Text>
                        <Text style={styles.dealSubtitle}>{phone}</Text>
                    </View>
                </View>
                <View style={styles.unitCardFooter}>
                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                            marginBottom: 15
                        }}
                    >
                        <View>
                            <Text style={[styles.infoTitle, styles.info]}>Contract Number:</Text>
                            <Text style={[styles.infoText, styles.info]}>{contractNumber}</Text>
                        </View>
                        <View>
                            <Text style={[styles.infoTitle, styles.info]}>Unit Type:</Text>
                            <Text style={[styles.infoText, styles.info]}>{unitType}</Text>
                        </View>
                        <View>
                            <Text style={[styles.infoTitle, styles.info]}>Unit No:</Text>
                            <Text style={[styles.infoText, styles.info]}>{unitNumber}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <View>
                            <Text style={[styles.infoTitle, styles.info]}>Unit Price:</Text>
                            <Text style={[styles.infoText, styles.info]}>{price}</Text>
                        </View>
                        <View>
                            <Text style={[styles.infoTitle, styles.info]}>Commission (16%):</Text>
                            <Text style={[styles.infoText, styles.info]}>{commission}</Text>
                        </View>
                        <View>
                            <Text style={[styles.infoTitle, styles.info]}>Date Added:</Text>
                            <Text style={[styles.infoText, styles.info]}>{dateAdded}</Text>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
}
