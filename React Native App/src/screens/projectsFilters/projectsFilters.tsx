import React from "react";
import { Text, View, ScrollView, ImageBackground } from "react-native";
import styles from "./projectsFilters.style";
import { Title, Button, Select, Header } from "@components";
import { colors } from "@utils";

export default function ProjectsFilters(): React.ReactElement {
    return (
        <View style={{ flex: 1 }}>
            <ImageBackground
                source={require("@assets/images/filtersBG.png")}
                style={{ width: "100%", height: "100%", opacity: 0.8 }}
            >
                <ScrollView style={styles.container}>
                    <Header withBack />
                    <View style={styles.titleContainer}>
                        <Title fontSize={24} weight="ExtraBold">
                            Filter By
                        </Title>
                    </View>
                    <View
                        style={{
                            borderBottomColor: colors.charcoalGrey16,
                            borderBottomWidth: 1,
                            marginHorizontal: 25
                        }}
                    />
                    <View style={styles.filtersContainer}>
                        <View style={{ marginBottom: 32 }}>
                            <Select
                                onValueChange={v => console.log(v)}
                                items={[{ label: "test", value: "test" }]}
                                placeholder={{ label: "Select Project Category", value: null }}
                            />
                        </View>
                        <View style={{ marginBottom: 32 }}>
                            <Select
                                onValueChange={v => console.log(v)}
                                items={[{ label: "test", value: "test" }]}
                                placeholder={{ label: "Select Preferred Project", value: null }}
                            />
                        </View>
                        <View>
                            <Select
                                onValueChange={v => console.log(v)}
                                items={[{ label: "test", value: "test" }]}
                                placeholder={{ label: "Select unit Type", value: null }}
                            />
                        </View>
                        <View style={styles.btnContainer}>
                            <Button
                                text="Apply Filters"
                                gradient={true}
                                textColor={colors.white}
                                weight="SemiBold"
                                fontSize={13}
                            />
                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
