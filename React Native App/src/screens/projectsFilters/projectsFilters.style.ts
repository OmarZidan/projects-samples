import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    titleContainer: {
        marginTop: 150,
        marginBottom: 24,
        marginHorizontal: 25
    },
    filtersContainer: {
        marginHorizontal: 32,
        marginTop: 43
    },
    btnContainer: {
        marginTop: 127
    }
});

export default styles;
