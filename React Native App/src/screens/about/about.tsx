import React from "react";
import { Text, View } from "react-native";
import styles from "./about.style";

export default function Home(): React.ReactElement {
    return (
        <View style={styles.container}>
            <Text>About</Text>
        </View>
    );
}
