import React, { useState, useEffect } from "react";
import { View, FlatList, ImageURISource, ActivityIndicator } from "react-native";
import styles from "./press.style";
import { Title, SearchboxAndFilters, Header, BottomNavBar } from "@components";
import Article from "./article";
import { ScrollView } from "react-native-gesture-handler";
import { useNavigation } from "react-navigation-hooks";
import newPlan from "@config/api";
import { colors } from "@utils";

interface ItemProps {
    id: "";
    title: "";
    image: ImageURISource;
}

export default function Press(): React.ReactElement {
    const { navigate } = useNavigation();
    const [pressList, setPressList] = useState([]);
    const [loading, setLoading] = useState(true);

    const pressData = async () => {
        try {
            const response = await newPlan.get("/news?developer=7");
            setPressList(response.data.data);
            setLoading(false);

            console.log(response.data.data);
        } catch (error) {
            console.log(error.response);
        }
    };

    useEffect(() => {
        pressData();
    }, []);

    return (
        <View style={{ flex: 1 }}>
            <ScrollView
                style={styles.container}
                contentContainerStyle={{
                    alignItems: "center",
                    justifyContent: "center"
                }}
            >
                <Header withBack={true} withDrawerMenu={false} />
                <View style={styles.titleContainer}>
                    <Title fontSize={24} weight="ExtraBold">
                        Press
                    </Title>
                </View>
                <SearchboxAndFilters
                    onPress={() => {
                        navigate("PressFilters");
                    }}
                />
                <View style={styles.listOfArticles}>
                    {loading && (
                        <View style={{ marginTop: 100 }}>
                            <ActivityIndicator size="large" color={colors.offWhite} />
                        </View>
                    )}
                    <FlatList
                        style={styles.filterList}
                        contentContainerStyle={{ alignItems: "center", justifyContent: "center" }}
                        data={pressList}
                        keyExtractor={(item: ItemProps): string => {
                            return item.id;
                        }}
                        renderItem={({ item }): React.ReactElement => {
                            return (
                                <Article
                                    articleTitle={item.title}
                                    articleImage={item.image}
                                    item={item}
                                />
                            );
                        }}
                    />
                </View>
            </ScrollView>
            {/* <BottomNavBar /> */}
        </View>
    );
}
