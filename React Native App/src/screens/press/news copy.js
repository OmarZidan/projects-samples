import React from "react";
import { View, ImageBackground, FlatList, ScrollView, Text, Button } from "react-native";
// import { Text, Header, Button } from "@components";
import styles from "./press.style";
// import misritalia from "@config/api";
// import { getAssetUrl } from "@utils";

export default class News extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newsList: [],
            next: null
        };
    }
    // loadPosts(page = 1) {
    //     misritalia.get(`/news?page=${page}`).then(response => {
    //         this.setState({
    //             newsList: [...this.state.newsList, ...response.data.data],
    //             next: response.data.paginator.next_page
    //         });
    //     });
    // }
    // componentDidMount() {
    //     this.loadPosts();
    // }

    render() {
        const { newsList, next } = this.state;
        return (
            <ScrollView style={styles.projectsContainer}>
                <View style={styles.listContainer}>
                    <Text weight="extrabold" style={styles.listTitle}>
                        Popular
                    </Text>
                    <View>
                        <FlatList
                            style={styles.filterList}
                            contentContainerStyle={styles.listContainer}
                            data={newsList}
                            keyExtractor={item => {
                                return item.id;
                            }}
                            renderItem={({ item }) => {
                                return (
                                    <View style={styles.listItem}>
                                        <View>
                                            <ImageBackground
                                                // source={{

                                                // }}
                                                style={styles.itemImage}
                                                imageStyle={{ borderRadius: 15 }}
                                            >
                                                <View style={styles.overlay} />
                                                <View style={styles.itemContent}>
                                                    {/* <Image
                                                        style={styles.itemAvatar}
                                                        source={{ uri: item.author_image }}
                                                    /> */}
                                                    <View style={{ textAlign: "center" }}>
                                                        {/* <Text
                                                            weight="bold"
                                                            style={styles.itemAuthor}
                                                        >
                                                            {item.author_name}
                                                        </Text> */}
                                                        <Text style={styles.itemDate}>
                                                            {item.article_date}
                                                        </Text>
                                                    </View>
                                                </View>
                                                <View style={styles.horizontalLine} />
                                                <Text weight="bold" style={styles.itemTitle}>
                                                    {item.title}
                                                </Text>
                                            </ImageBackground>
                                            <Button
                                                text="Explore"
                                                style={"darkblue"}
                                                containerStyles={styles.exploreBtn}
                                                fontSize={12}
                                                letterSpacing={1}
                                                // onPress={() =>
                                                //     this.props.navigation.navigate("SingleNews", {
                                                //         id: item.id
                                                //     })
                                                // }
                                                fontWeight="medium"
                                                iconRight={require("@assets/icons/pathRight.png")}
                                                iconStyles={{
                                                    width: 20,
                                                    height: 20
                                                }}
                                            />
                                        </View>
                                    </View>
                                );
                            }}
                        />
                    </View>
                </View>
                <View style={styles.moreContainer}>
                    {next && (
                        <Button
                            text="Load More"
                            onPress={() => this.loadPosts(next)}
                            style={"white"}
                            containerStyles={styles.moreBtn}
                            fontSize={13}
                            letterSpacing={2}
                            fontWeight="extrabold"
                        />
                    )}
                </View>
            </ScrollView>
        );
    }
}
