import React from "react";
import { Text, View, Image, ImageURISource } from "react-native";
import styles from "./press.style";
import { Button } from "@components";
import { useNavigation } from "react-navigation-hooks";
import { colors, getAssetUrl } from "@utils";

interface ArticleProps {
    articleImage: ImageURISource;
    articleTitle: string;
    item: any;
}
export default function Article({
    articleImage,
    articleTitle,
    item
}: ArticleProps): React.ReactElement {
    const { navigate } = useNavigation();

    return (
        <View style={styles.articleWrapper}>
            <View style={styles.articleImageWarpper}>
                <Image style={styles.articleImage} source={{ uri: getAssetUrl(articleImage) }} />
            </View>
            <View style={styles.articleDetails}>
                <Text style={styles.articleTitle}>{articleTitle}</Text>
                <View style={styles.btnContainer}>
                    <Button
                        text="Know More"
                        // backgroundColor="light"
                        backgroundColor={colors.white}
                        textColor={colors.dark}
                        weight="Bold"
                        fontSize={13}
                        containerStyles={styles.pressButton}
                        onPress={() =>
                            navigate("SinglePressArticle", {
                                item: item
                            })
                        }
                    />
                </View>
            </View>
        </View>
    );
}
