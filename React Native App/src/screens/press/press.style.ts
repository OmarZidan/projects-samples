import { StyleSheet } from "react-native";

import { colors } from "@utils";

const styles = StyleSheet.create({
    filterList: {
        width: "100%"
    },

    container: {
        backgroundColor: colors.lightBlueGrey,
        flex: 1
    },
    titleContainer: {
        justifyContent: "flex-start",
        width: "100%",
        marginTop: 150,
        marginBottom: 28,
        paddingLeft: 25
    },

    listOfArticles: {
        padding: 25,
        paddingBottom: 75,
        width: "100%"
    },
    articleWrapper: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        height: 175,
        width: "100%",
        marginBottom: 25,
        backgroundColor: "white"
    },
    articleImageWarpper: {
        flexBasis: "35%"
    },
    articleImage: {
        width: "100%",
        height: "100%",
        resizeMode: "cover"
    },
    articleDetails: {
        flexBasis: "65%",
        padding: 10
    },
    articleTitle: {
        color: colors.offWhite,
        fontSize: 14,
        fontFamily: "Gotham-Bold",
        paddingTop: 10,
        letterSpacing: 0.09,
        paddingRight: 15,
        textTransform: "capitalize",
        marginBottom: 10
    },
    pressButton: {
        width: 130,
        height: 45,
        // marginTop: 10,
        shadowColor: colors.charcoalGrey32,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 4,
        shadowOpacity: 1,
        elevation: 2,
        borderWidth: 1,
        borderColor: "transparent"
    },
    btnContainer: {
        marginLeft: 55,
        marginRight: 15
    }
});

export default styles;
