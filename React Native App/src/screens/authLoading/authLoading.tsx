import React, { useEffect, useContext } from "react";
import { ActivityIndicator, View, Text } from "react-native";
import { StackActions } from "react-navigation";
import { AuthContext } from "./../../contexts/AuthContext";
import { useNavigation } from "react-navigation-hooks";

// import { useAuth } from "../../provider";

export default function AuthLoading(props) {
    // const { navigate } = props.navigation;
    // const { getAuthState } = useAuth();
    const [user, setUser] = useContext(AuthContext);
    const { navigate } = useNavigation();

    function initialize() {
        const tokenTest = user.token;
        const userRole = user.role;
        if (userRole === "sales") {
            navigate("Sales");
        } else if (userRole === "broker") {
            navigate("Broker");
        } else navigate("SignIn");
    }

    useEffect(() => {
        initialize();
    }, []);

    return (
        <View
            style={{
                backgroundColor: "#fff",
                alignItems: "center",
                justifyContent: "center",
                flex: 1
            }}
        >
            <ActivityIndicator />
            <Text>{"Loading User Data"}</Text>
        </View>
    );
}
