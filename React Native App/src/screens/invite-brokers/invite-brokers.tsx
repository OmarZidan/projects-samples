import React, { useState, useEffect } from "react";
import {
    View,
    ScrollView,
    ImageBackground,
    Modal,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image
} from "react-native";
import { Text, Header, Button, Title, TextInput, PhoneInput, Select } from "@components";
import styles from "./invite-brokers.style";
import misritalia from "@config/api";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";

export default function InviteBrokers(): React.ReactElement {
    const [brokers, setBrokers] = useState([
        {
            name: "",
            mobile: "+20",
            email: "",
            jobTitle: ""
        }
    ]);
    const [modalVisible, setModalVisible] = useState(false);

    const setVisible = visible => {
        setModalVisible(visible);
    };

    const BrokerForm = () => {
        return brokers.map((broker, index) => (
            <View key={index}>
                <View style={styles.HLine} />
                <TextInput
                    placeholder="Full Name"
                    placeholderColor={colors.charcoalGrey}
                    style={{ marginBottom: 32 }}
                />
                <PhoneInput
                    placeholder="Mobile No."
                    style={{ marginBottom: 32 }}
                    height={42}
                    value={broker.mobile}
                />
                <TextInput
                    placeholder="Email Address"
                    placeholderColor={colors.charcoalGrey}
                    keyboardType="email-address"
                    style={{ marginBottom: 32 }}
                />
                <Select
                    items={[
                        { label: "Football", value: "football" },
                        { label: "Baseball", value: "baseball" },
                        { label: "Hockey", value: "hockey" }
                    ]}
                    onValueChange={value => console.log(value)}
                    placeholder={{ label: "Select job title", value: null }}
                />
            </View>
        ));
    };

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={require("@assets/images/filtersBG.png")} style={styles.bgImg}>
                <Header withBack withDrawerMenu={false} />
                <ScrollView>
                    <View style={{ paddingHorizontal: 25 }}>
                        <View style={styles.titleContainer}>
                            <Title fontSize={22} weight="Bold" textTransform="capitalize">
                                Invite Brokers
                            </Title>
                        </View>
                        {BrokerForm()}
                        <View style={{ width: "50%", marginTop: 30, alignSelf: "center" }}>
                            <Button
                                onPress={() =>
                                    setBrokers([
                                        ...brokers,
                                        { name: "", mobile: "+20", email: "", jobTitle: "" }
                                    ])
                                }
                                text="Add Broker"
                                backgroundColor={colors.dark}
                                fontSize={11}
                                weight="SemiBold"
                                textColor={colors.white}
                            />
                        </View>
                        <View style={{ width: "100%", marginVertical: 40 }}>
                            <Button
                                text="Send Invitation"
                                gradient
                                fontSize={13}
                                weight="SemiBold"
                                textColor={colors.white}
                                onPress={() => {
                                    setModalVisible(true);
                                }}
                            />
                        </View>
                    </View>
                    <View>
                        <Modal animationType="fade" transparent={true} visible={modalVisible}>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    setVisible(!modalVisible);
                                }}
                            >
                                <View style={styles.modalContainer}>
                                    <TouchableWithoutFeedback>
                                        <View style={styles.modal}>
                                            <Image
                                                source={require("@assets/images/sent.png")}
                                                style={{ height: 192, width: 192 }}
                                            />
                                            <Text
                                                weight="Bold"
                                                style={{
                                                    fontSize: 32,
                                                    letterSpacing: 1,
                                                    color: colors.white,
                                                    textAlign: "center",
                                                    marginTop: 40
                                                }}
                                            >
                                                INVITATIONS{"\n"}SENT!
                                            </Text>
                                            <View style={styles.cardFooterDown}>
                                                <View style={{ width: "43%", marginHorizontal: 5 }}>
                                                    <Button
                                                        text="Invite Brokers"
                                                        backgroundColor={colors.white}
                                                        textColor={colors.offWhite}
                                                        fontSize={11}
                                                        borderWidth={1}
                                                        borderColor={colors.white}
                                                        containerStyles={styles.availabilityButton}
                                                    />
                                                </View>
                                                <View style={{ width: "43%", marginHorizontal: 5 }}>
                                                    <Button
                                                        text="Back to Dashboard"
                                                        backgroundColor="transparent"
                                                        borderColor={colors.white}
                                                        textColor={colors.white}
                                                        fontSize={11}
                                                        borderWidth={1}
                                                        containerStyles={styles.availabilityButton}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
