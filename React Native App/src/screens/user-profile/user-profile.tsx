import React, { useState, useEffect } from "react";
import {
    View,
    ScrollView,
    Image,
    Switch,
    Modal,
    TouchableWithoutFeedback,
    TouchableOpacity
} from "react-native";
import styles from "./user-profile.style";
import { Title, BottomNavBar, CheckBox, Header, Text, Button } from "@components";
import { useNavigation } from "react-navigation-hooks";
import newPlan from "@config/api";
import { colors } from "@utils";

export default function UserProfile(): React.ReactElement {
    const [popModalVisible, setPopModalVisible] = useState(true);
    const { navigate } = useNavigation();

    const setPopVisible = visible => {
        setPopModalVisible(visible);
    };

    useEffect(() => {
        const timer = setTimeout(() => {
            setPopVisible(false);
        }, 5000);
        return () => clearTimeout(timer);
    }, []);
    // const [projectsList, setProjectsList] = useState([]);
    // // const [loading, setLoading] = useState(true);
    // const navigation = useNavigation();

    // useEffect(() => {
    //     newPlan
    //         .get("/projects")
    //         .then(response => {
    //             // console.log(response.data.data);
    //             setProjectsList(response.data.data);
    //             // if (response.data && response.data.data.length > 0) {
    //             //     setContent({ ...response.data.data[0] });
    //             // }
    //             // setLoading(false);
    //         })
    //         .catch(err => {
    //             console.log(err);
    //             //Might need to handle errors lolzzzlzlz
    //             // setLoading(false);
    //         });
    // }, []);

    return (
        <View style={{ flex: 1 }}>
            <ScrollView style={styles.container}>
                <Header withBack={false} />
                <View style={styles.titleContainer}>
                    <Title fontSize={24} weight="ExtraBold" textTransform="capitalize">
                        My Profile
                    </Title>
                </View>
                <View style={styles.contentContainer}>
                    <Text weight="Bold" style={styles.userTitle}>
                        Abdelrahman Fathy
                    </Text>
                    <Text weight="Medium" style={styles.userSubtitle}>
                        Senior Real Estate Broker
                    </Text>
                    <View style={styles.HLine} />
                    <Text weight="Medium" style={styles.contactTitle}>
                        Contact Information
                    </Text>
                    <View style={styles.contactBox}>
                        <View style={{ flexDirection: "row", marginBottom: 15 }}>
                            <Image
                                source={require("@assets/icons/smartphone1.png")}
                                style={styles.infoIcon}
                            />
                            <Text weight="Regular" style={styles.infoText}>
                                +2 010 267 338 13
                            </Text>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                            <Image
                                source={require("@assets/icons/arroba.png")}
                                style={styles.infoIcon}
                            />
                            <Text weight="Regular" style={styles.infoText}>
                                username@domain.com
                            </Text>
                        </View>
                        <View style={styles.btnContainer}>
                            <Button
                                text="Edit Profile"
                                backgroundColor={colors.dark}
                                fontSize={10}
                                weight="SemiBold"
                                textColor={colors.white}
                                onPress={(): void => {
                                    navigate("EditUserProfile");
                                }}
                            />
                        </View>
                    </View>
                    <View style={styles.switchContainer}>
                        <Text
                            weight="SemiBold"
                            style={{ fontSize: 12, letterSpacing: 0, color: colors.dark }}
                        >
                            Allow Notifications
                        </Text>
                        <Switch
                            thumbColor={colors.white}
                            trackColor={{ false: colors.paleLilac, true: colors.offWhite }}
                            value={true}
                        />
                    </View>
                    <View>
                        <Modal animationType="fade" transparent={true} visible={popModalVisible}>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    setPopVisible(!popModalVisible);
                                }}
                            >
                                <View style={styles.popModalContainer}>
                                    <TouchableWithoutFeedback>
                                        <View style={styles.popModal}>
                                            <View>
                                                <Text
                                                    weight="Medium"
                                                    style={{
                                                        fontSize: 17,
                                                        lineHeight: 22,
                                                        width: 215
                                                    }}
                                                >
                                                    Your have updated your profile Successfully!
                                                </Text>
                                            </View>
                                            <View>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        setPopVisible(false);
                                                    }}
                                                >
                                                    <Image
                                                        source={require("@assets/icons/close.png")}
                                                        style={{ height: 17, width: 17 }}
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>
                    </View>
                </View>
            </ScrollView>
            <BottomNavBar />
        </View>
    );
}
