import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey,
        paddingHorizontal: 25
    },
    titleContainer: {
        marginTop: 150,
        marginBottom: 28
        // marginHorizontal: 25
    },
    contentContainer: {
        backgroundColor: colors.white50,
        marginHorizontal: 10,
        paddingHorizontal: 15,
        paddingVertical: 24
    },
    userTitle: {
        fontSize: 16,
        letterSpacing: 0.75,
        textAlign: "center",
        color: colors.offWhite,
        marginBottom: 6
    },
    userSubtitle: {
        fontSize: 11,
        letterSpacing: 0.5,
        color: colors.charcoalGrey,
        textAlign: "center",
        marginBottom: 16
    },
    HLine: {
        borderBottomColor: colors.veryLightPinkTwo,
        borderBottomWidth: 1,
        marginHorizontal: 15
    },
    contactTitle: {
        color: colors.dark,
        fontSize: 12,
        letterSpacing: 0,
        marginTop: 15
    },
    contactBox: {
        backgroundColor: colors.white,
        paddingHorizontal: 25,
        paddingTop: 25,
        marginTop: 16,
        borderRadius: 2
    },
    infoIcon: {
        width: 16,
        height: 16,
        resizeMode: "contain"
    },
    infoText: { color: colors.dark, fontSize: 11, marginLeft: 10 },
    btnContainer: {
        width: 125,
        marginBottom: 15,
        marginTop: 25,
        alignSelf: "center"
    },
    switchContainer: {
        backgroundColor: colors.white,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingVertical: 30,
        paddingHorizontal: 25,
        marginTop: 32,
        borderRadius: 2
    },
    popModalContainer: { backgroundColor: "transparent", flex: 1 },
    popModal: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignSelf: "center",
        width: "96%",
        backgroundColor: colors.sickGreen,
        position: "absolute",
        top: 5,
        borderRadius: 2,
        paddingHorizontal: 24,
        paddingVertical: 20
    }
});

export default styles;
