import { render } from "react-native-testing-library";
import React from "react";
import Button from "./button";

describe("<Button />", () => {
    it("renders correctly", () => {
        const tree = render(<Button text="dewd" weight="Bold" />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
