import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    btn: {
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 15.5,
        shadowColor: colors.dark,
        shadowOffset: {
            width: 0,
            height: 8
        },
        shadowRadius: 16,
        shadowOpacity: 0.6,
        elevation: 1,
        borderWidth: 1,
        borderColor: "transparent"
    },
    textStyles: {
        textTransform: "capitalize",
        letterSpacing: 1,
        textAlign: "center"
    }
});

export default styles;
