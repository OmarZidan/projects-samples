import React from "react";
import { Text, TouchableOpacityProps, TouchableOpacity, ImageURISource, View } from "react-native";
import styles from "./button.style";
import { colors } from "@utils";
import { LinearGradient } from "expo-linear-gradient";

interface ButtonProps extends TouchableOpacityProps {
    text: string;
    backgroundColor?: string;
    textColor?: string;
    containerStyles?: {};
    gradient?: boolean;
    borderWidth?: number;
    borderColor?: string;
    fontSize?: number;
    weight?: string;
    tonino?: boolean;
}

function Button({
    backgroundColor,
    // disabled,
    gradient,
    textColor,
    text,
    borderColor,
    borderWidth,
    fontSize,
    tonino,
    weight = "Bold",
    containerStyles,
    ...props
}: ButtonProps): React.ReactElement {
    return (
        <View>
            {gradient ? (
                <LinearGradient
                    colors={[tonino ? colors.red : colors.offWhite, colors.charcoalGrey]}
                    start={{ x: 0, y: 0.5 }}
                    end={{ x: 1, y: 0.5 }}
                >
                    <TouchableOpacity style={[styles.btn, containerStyles]} {...props}>
                        <Text
                            style={[
                                {
                                    color: textColor,
                                    fontSize: fontSize,
                                    fontFamily: tonino ? `Roboto-${weight}` : `Gotham-${weight}`
                                },
                                styles.textStyles
                            ]}
                        >
                            {text}
                        </Text>
                    </TouchableOpacity>
                </LinearGradient>
            ) : (
                <TouchableOpacity
                    style={[
                        styles.btn,
                        containerStyles,
                        {
                            backgroundColor: backgroundColor,
                            // borderColor: borderColor,
                            borderWidth: borderWidth
                        }
                    ]}
                    {...props}
                >
                    <Text
                        style={[
                            {
                                color: textColor,
                                fontSize: fontSize,
                                fontFamily: tonino
                                    ? `Roboto-${weight}`
                                    : `Gotham-${weight}` || "Gotham-Bold"
                            },
                            styles.textStyles
                        ]}
                    >
                        {text}
                    </Text>
                </TouchableOpacity>
            )}
        </View>
    );
}

export default Button;
