import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },
    titleContainer: {
        marginTop: 150,
        marginBottom: 24,
        marginHorizontal: 25
    },
    leadsContainer: {
        marginTop: 28,
        marginHorizontal: 32
    },
    leadContainer: {
        flexDirection: "row",
        backgroundColor: colors.white,
        marginBottom: 32,
        height: 185
    },
    leadImage: { height: "100%", width: 100 },
    leadContent: {
        padding: 15,
        flex: 1
    },
    leadTitle: {
        fontFamily: "Gotham-Bold",
        fontSize: 13,
        color: colors.dark,
        letterSpacing: 0,
        textTransform: "capitalize"
    },
    HLine: {
        borderBottomColor: colors.veryLightPink,
        borderBottomWidth: 1
    },
    btnContainer: { width: 115, marginLeft: 15, marginTop: 15 },
    status: {
        position: "absolute",
        left: 0,
        bottom: 0,
        width: 100,
        color: colors.white,
        fontSize: 10,
        fontFamily: "Gotham-Medium",
        paddingVertical: 6,
        textAlign: "center"
    },
    modalContainer: { backgroundColor: "rgba(0,0,0,.4)", flex: 1 },
    modal: {
        alignSelf: "center",
        width: "100%",
        height: 175,
        backgroundColor: colors.white,
        position: "absolute",
        bottom: 0,
        borderRadius: 10
    },
    modalItem: {
        height: 87,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    modalText: {
        fontSize: 14,
        fontFamily: "Gotham-Regular",
        color: colors.dark,
        marginLeft: 28
    }
});

export default styles;
