import React, { useState } from "react";
import { Modal, Text, Image, View, TouchableWithoutFeedback, TouchableOpacity } from "react-native";
import ContactLead from "./../contact-lead/contact-lead";
import Button from "./../button/button";
import styles from "./lead-item.style";
import { colors } from "@utils";

interface LeadItemProps {
    lead: any;
}

function LeadItem({ lead }: LeadItemProps) {
    const [modalVisible, setModalVisible] = useState(false);

    const setVisible = visible => {
        setModalVisible(visible);
    };

    let background = "";
    switch (lead.status) {
        case "Contracted":
            background = colors.sickGreen;
            break;
        case "Down Payment":
            background = colors.offWhite;
            break;
        case "not interested":
            background = colors.redWine;
            break;
        case "URF":
            background = colors.offWhite;
            break;
        default:
            background = colors.redWine;
    }
    return (
        <View key={lead.id} style={styles.leadContainer}>
            <View>
                <Image style={styles.leadImage} source={lead.image} />
            </View>
            <View style={styles.leadContent}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={styles.leadTitle}>{lead.title}</Text>
                    <TouchableOpacity
                        onPress={() => {
                            setModalVisible(true);
                        }}
                    >
                        <Image
                            source={require("@assets/icons/3dots.png")}
                            style={{ width: 4, height: 16 }}
                        />
                    </TouchableOpacity>
                </View>
                <ContactLead lead={lead} containerStyles={{ marginBottom: 12, marginTop: 15 }} />
                <View style={styles.HLine} />
                <View style={styles.btnContainer}>
                    <Button
                        text="View Details"
                        backgroundColor={colors.dark}
                        textColor={colors.white}
                        fontSize={11}
                        weight="SemiBold"
                    />
                </View>
            </View>
            <Text style={[styles.status, { backgroundColor: background }]}>{lead.status}</Text>

            <View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={modalVisible}
                    // onRequestClose={() => {
                    //     Alert.alert("Modal has been closed.");
                    // }}
                >
                    <TouchableWithoutFeedback
                        onPress={() => {
                            setVisible(!modalVisible);
                        }}
                    >
                        <View style={styles.modalContainer}>
                            <TouchableWithoutFeedback>
                                <View style={styles.modal}>
                                    <View>
                                        <TouchableOpacity>
                                            <View style={styles.modalItem}>
                                                <Image
                                                    source={require("@assets/icons/compose.png")}
                                                    style={{ width: 32, height: 32 }}
                                                />
                                                <Text style={styles.modalText}>
                                                    Edit Lead Information
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity>
                                            <View style={styles.HLine} />
                                            <View style={styles.modalItem}>
                                                <Image
                                                    source={require("@assets/icons/compose.png")}
                                                    style={{ width: 32, height: 32 }}
                                                />
                                                <Text style={styles.modalText}>
                                                    Delete Lead from List
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        </View>
    );
}
export default LeadItem;
