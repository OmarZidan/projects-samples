import React from "react";
import { View } from "react-native";
import Text from "../text/text";
import { colors } from "@utils";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import { MultiSliderProps } from "ptomasroos__react-native-multi-slider";

interface RangeSliderProps extends MultiSliderProps {
    formatNumber?: boolean; //formats 100000 to 100,000
}
//Other props here: https://github.com/ptomasroos/react-native-multi-slider

function formatNumberCommas(num: number | string): string {
    const numParts = num.toString().split(".");
    numParts[0] = numParts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return numParts.join(".");
}

function RangeSlider({ formatNumber, ...props }: RangeSliderProps): React.ReactElement {
    return (
        <View style={{ paddingBottom: 30 }}>
            <MultiSlider
                selectedStyle={{
                    backgroundColor: "#c09867"
                }}
                unselectedStyle={{
                    backgroundColor: "rgba(255,255,255,0.5)"
                }}
                containerStyle={{
                    height: 24
                }}
                trackStyle={{
                    height: 8
                }}
                touchDimensions={{
                    height: 24,
                    width: 24,
                    borderRadius: 12,
                    slipDisplacement: 1000
                }}
                customMarker={({ currentValue }): React.ReactElement => {
                    const value = formatNumber
                        ? formatNumberCommas(currentValue)
                        : currentValue.toString();
                    return (
                        <View>
                            <View
                                style={{
                                    backgroundColor: colors.offWhite,
                                    width: 24,
                                    height: 24,
                                    borderRadius: 12,
                                    shadowColor: "rgba(82, 55, 24, 0.32)",
                                    shadowOffset: {
                                        width: 0,
                                        height: 2
                                    },
                                    shadowRadius: 4,
                                    shadowOpacity: 1,
                                    elevation: 2,
                                    top: 4
                                }}
                            />
                            <View style={{ flexDirection: "row" }} pointerEvents="none">
                                <Text
                                    numberOfLines={1}
                                    style={{
                                        position: "absolute",
                                        textAlign: "center",
                                        left: value.length > 3 ? -value.length * 1.5 : 0,
                                        marginTop: 10,
                                        color: colors.offWhite,
                                        flex: 1,
                                        fontSize: 10
                                    }}
                                >
                                    {value}
                                </Text>
                            </View>
                        </View>
                    );
                }}
                {...props}
            />
        </View>
    );
}

export default RangeSlider;
