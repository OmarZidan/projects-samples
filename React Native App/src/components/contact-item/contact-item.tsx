import React, { useState } from "react";
import { Modal, Text, Image, View, TouchableWithoutFeedback, TouchableOpacity } from "react-native";
import ContactLead from "./../contact-lead/contact-lead";
import Button from "./../button/button";
import styles from "./contact-item.style";
import { colors } from "@utils";

interface LeadItemProps {
    item: any;
}

function ContactItem({ item }: LeadItemProps) {
    const [modalVisible, setModalVisible] = useState(false);

    const setVisible = visible => {
        setModalVisible(visible);
    };

    return (
        <View key={item.id} style={styles.leadContainer}>
            <View style={styles.leadContent}>
                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={styles.leadTitle}>{item.title}</Text>
                    <TouchableOpacity
                        onPress={() => {
                            setModalVisible(true);
                        }}
                    >
                        <Image
                            source={require("@assets/icons/3dots.png")}
                            style={{ width: 4, height: 16 }}
                        />
                    </TouchableOpacity>
                </View>
                <Text style={styles.contactText}>{item.email}</Text>
                <Text style={styles.contactText}>{item.phone}</Text>
                <View
                    style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 32 }}
                >
                    <View style={styles.btnContainer}>
                        <Button
                            text="Add Deal"
                            backgroundColor={colors.dark}
                            textColor={colors.white}
                            fontSize={11}
                            weight="SemiBold"
                        />
                    </View>
                    <View style={styles.btnContainer}>
                        <Button
                            text="View Details"
                            backgroundColor={colors.white}
                            textColor={colors.dark}
                            fontSize={11}
                            weight="SemiBold"
                        />
                    </View>
                </View>
            </View>

            <View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={modalVisible}
                    // onRequestClose={() => {
                    //     Alert.alert("Modal has been closed.");
                    // }}
                >
                    <TouchableWithoutFeedback
                        onPress={() => {
                            setVisible(!modalVisible);
                        }}
                    >
                        <View style={styles.modalContainer}>
                            <TouchableWithoutFeedback>
                                <View style={styles.modal}>
                                    <View>
                                        <TouchableOpacity>
                                            <View style={styles.modalItem}>
                                                <Image
                                                    source={require("@assets/icons/compose.png")}
                                                    style={{ width: 32, height: 32 }}
                                                />
                                                <Text style={styles.modalText}>
                                                    Edit Lead Information
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity>
                                            <View style={styles.HLine} />
                                            <View style={styles.modalItem}>
                                                <Image
                                                    source={require("@assets/icons/compose.png")}
                                                    style={{ width: 32, height: 32 }}
                                                />
                                                <Text style={styles.modalText}>
                                                    Delete Lead from List
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        </View>
    );
}
export default ContactItem;
