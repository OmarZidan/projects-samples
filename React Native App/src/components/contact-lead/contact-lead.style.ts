import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    box: {
        height: 32,
        width: 32,
        backgroundColor: colors.white,
        justifyContent: "center",
        alignItems: "center",
        shadowColor: colors.lightBlueGrey,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 4,
        shadowOpacity: 1,
        elevation: 2,
        borderRadius: 1,
        marginBottom: 6
    },
    icon: { height: 24, width: 24, resizeMode: "cover" },
    iconText: {
        fontSize: 8,
        letterSpacing: 0,
        color: colors.offWhite,
        textAlign: "center",
        fontFamily: "Gotham-Medium"
    }
});

export default styles;
