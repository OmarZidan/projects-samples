import React from "react";
import { View, Image, Text, TouchableOpacity } from "react-native";
import { Button } from "@components";
import styles from "./contact-lead.style";
import { colors } from "@utils";

interface ContactLeadProps {
    lead: any;
    containerStyles?: any;
}

function ContactLead({ lead, containerStyles, ...props }: ContactLeadProps) {
    return (
        <View style={[containerStyles, { flexDirection: "row", justifyContent: "space-between" }]}>
            <TouchableOpacity>
                <View style={[styles.box, { marginLeft: 4 }]}>
                    <Image source={require("@assets/icons/whatsapp.png")} style={styles.icon} />
                </View>
                <Text style={styles.iconText}>Whatsapp</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <View style={styles.box}>
                    <Image source={require("@assets/icons/phoneCall.png")} style={styles.icon} />
                </View>
                <Text style={styles.iconText}>Call</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <View style={styles.box}>
                    <Image source={require("@assets/icons/arroba.png")} style={styles.icon} />
                </View>
                <Text style={styles.iconText}>Mail</Text>
            </TouchableOpacity>
        </View>
    );
}
export default ContactLead;
