import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        paddingHorizontal: 18,
        flexDirection: "row",
        paddingVertical: 13,
        borderBottomColor: colors.redWine
    },
    textInput: {
        height: "100%",
        fontFamily: "Gotham-Medium",
        fontSize: 11,
        flex: 1,
        color: colors.charcoalGrey
    },
    iconWrap: {
        paddingLeft: 15,
        alignItems: "center",
        justifyContent: "center"
    },
    icon: {
        height: 18,
        width: 18,
        resizeMode: "contain"
    },
    togglePasswordButton: {
        width: 50,
        marginRight: -16,
        alignItems: "center",
        justifyContent: "center"
    },
    togglePasswordIcon: {
        width: 18,
        height: 18,
        resizeMode: "contain"
    },
    errorMessage: {
        fontSize: 12,
        color: colors.redWine,
        paddingVertical: 5
    },
    dateTouchable: {
        backgroundColor: "transparent",
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
    iosDateModal: {
        backgroundColor: "#fff",
        width: "90%",
        alignSelf: "center",
        padding: 20,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 10,
        shadowOpacity: 0.3
    }
});

export default styles;
