import React, { useState } from "react";
import {
    Image,
    TouchableOpacity,
    View,
    TextInput as NativeTextInput,
    Text,
    TextInputProps as NativeTextInputProps,
    ImageSourcePropType,
    StyleProp,
    ViewStyle,
    Platform
} from "react-native";
import styles from "./text-input.style";
import { colors } from "@utils";
import DateTimePicker from "@react-native-community/datetimepicker";
import Modal from "react-native-modal";
import Button from "../button/button";

interface TextInputProps extends NativeTextInputProps {
    icon?: ImageSourcePropType;
    height?: number;
    iconColor?: string;
    togglePassword?: boolean;
    secureTextEntry?: boolean;
    style?: StyleProp<ViewStyle>;
    errorMessage?: string;
    placeholder?: string;
    placeholderColor?: string;
    date?: boolean;
    onDateChange?: (date: Date) => void;
}

function TextInput({
    icon,
    date = false,
    height = 44,
    iconColor = colors.charcoalGrey,
    onDateChange,
    togglePassword,
    placeholder,
    secureTextEntry,
    style = {},
    errorMessage,
    placeholderColor,
    ...props
}: TextInputProps): React.ReactElement {
    const [showPassword, setShowPassword] = useState(false);
    const [dateModal, setDateModal] = useState(false);
    const [currentDate, setCurrentDate] = useState<Date>(new Date(props.value || null));

    const datePicker = dateModal ? (
        <DateTimePicker
            value={currentDate}
            mode="date"
            onChange={(e, d) => {
                if (Platform.OS === "android") {
                    setDateModal(false);
                    onDateChange && onDateChange(d);
                }
                setCurrentDate(d);
            }}
        />
    ) : null;
    return (
        <View style={[style]}>
            <View
                style={[styles.container, { height, borderBottomWidth: errorMessage ? 1 : null }]}
            >
                <NativeTextInput
                    secureTextEntry={secureTextEntry === true && !showPassword}
                    style={styles.textInput}
                    {...props}
                    placeholder={placeholder}
                    placeholderTextColor={placeholderColor}
                />

                {secureTextEntry === true && togglePassword && (
                    <TouchableOpacity
                        onPressIn={(): void => setShowPassword(true)}
                        onPressOut={(): void => setShowPassword(false)}
                        style={{ ...styles.togglePasswordButton, height }}
                    >
                        <Image
                            style={{
                                ...styles.togglePasswordIcon,
                                tintColor: showPassword ? colors.blueyGrey : colors.charcoalGrey
                            }}
                            source={require("@assets/icons/view.png")}
                        />
                    </TouchableOpacity>
                )}
                {icon && (
                    <View
                        style={{
                            ...styles.iconWrap
                            // height
                        }}
                    >
                        <Image
                            source={icon}
                            style={{
                                ...styles.icon,
                                tintColor: iconColor
                            }}
                        />
                    </View>
                )}
                {date && (
                    <TouchableOpacity
                        onPress={() => setDateModal(true)}
                        style={styles.dateTouchable}
                    />
                )}
            </View>
            {Boolean(errorMessage) && <Text style={styles.errorMessage}>{errorMessage}</Text>}
            {date && Platform.OS === "android" && datePicker}
            {date && Platform.OS === "ios" && (
                <Modal isVisible={dateModal} onBackdropPress={() => setDateModal(false)}>
                    <View style={styles.iosDateModal}>
                        {datePicker}
                        <View style={{ marginTop: 10 }}>
                            <Button
                                onPress={() => {
                                    onDateChange && onDateChange(currentDate);
                                    setDateModal(false);
                                }}
                                gradient
                                textColor="#fff"
                                text="OK"
                            />
                        </View>
                    </View>
                </Modal>
            )}
        </View>
    );
}

export default TextInput;
