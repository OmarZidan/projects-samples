import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        paddingHorizontal: 33,
        flexDirection: "row",
        paddingVertical: 23,
        height: 90,
        borderBottomColor: colors.redWine,
        borderRadius: 1
    },
    textInput: {
        // height: "100%",
        fontFamily: "Gotham-Medium",
        fontSize: 12,
        paddingLeft: 25,
        letterSpacing: 0,
        borderLeftWidth: 1,
        borderLeftColor: colors.paleLilac,
        flex: 1,
        color: colors.charcoalGrey
    },
    iconWrap: {
        marginRight: 25,
        alignItems: "center",
        justifyContent: "center"
    },
    icon: {
        height: 24,
        width: 18,
        resizeMode: "contain"
    },
    togglePasswordButton: {
        width: 50,
        marginRight: -16,
        alignItems: "center",
        justifyContent: "center"
    },
    togglePasswordIcon: {
        width: 18,
        height: 18,
        resizeMode: "contain"
    },
    errorMessage: {
        fontSize: 12,
        color: colors.redWine,
        paddingVertical: 5
    }
});

export default styles;
