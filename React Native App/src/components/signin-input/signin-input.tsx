import React, { useState } from "react";
import {
    Image,
    TouchableOpacity,
    View,
    TextInput as NativeTextInput,
    Text,
    TextInputProps as NativeTextInputProps,
    ImageSourcePropType,
    StyleProp,
    ViewStyle
} from "react-native";
import styles from "./signin-input.style";
import { colors } from "@utils";

interface TextInputProps extends NativeTextInputProps {
    icon?: ImageSourcePropType;
    height?: number;
    iconColor?: string;
    togglePassword?: boolean;
    secureTextEntry?: boolean;
    style?: StyleProp<ViewStyle>;
    errorMessage?: string;
    placeholder?: string;
    placeholderColor?: string;
}

function SignInInput({
    icon,
    // height = 90,
    iconColor = colors.charcoalGrey,
    togglePassword,
    placeholder,
    secureTextEntry,
    style = {},
    errorMessage,
    placeholderColor,
    ...props
}: TextInputProps): React.ReactElement {
    const [showPassword, setShowPassword] = useState(false);
    return (
        <View style={[style]}>
            <View style={[styles.container, { borderBottomWidth: errorMessage ? 1 : null }]}>
                {icon && (
                    <View
                        style={{
                            ...styles.iconWrap
                        }}
                    >
                        <Image
                            source={icon}
                            style={{
                                ...styles.icon,
                                tintColor: iconColor
                            }}
                        />
                    </View>
                )}
                <NativeTextInput
                    secureTextEntry={secureTextEntry === true && !showPassword}
                    style={styles.textInput}
                    {...props}
                    placeholder={placeholder}
                    placeholderTextColor={placeholderColor}
                />

                {secureTextEntry === true && togglePassword && (
                    <TouchableOpacity
                        onPressIn={(): void => setShowPassword(true)}
                        onPressOut={(): void => setShowPassword(false)}
                        style={{ ...styles.togglePasswordButton }}
                    >
                        <Image
                            style={{
                                ...styles.togglePasswordIcon,
                                tintColor: showPassword ? colors.blueyGrey : colors.charcoalGrey
                            }}
                            source={require("@assets/icons/view.png")}
                        />
                    </TouchableOpacity>
                )}
            </View>
            {Boolean(errorMessage) && <Text style={styles.errorMessage}>{errorMessage}</Text>}
        </View>
    );
}

export default SignInInput;
