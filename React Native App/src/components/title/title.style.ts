import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    title: {
        color: colors.dark,
        // textTransform: "uppercase",
        letterSpacing: 1.09
    }
});

export default styles;
