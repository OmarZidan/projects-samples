import React from "react";
import { Text } from "react-native";
import styles from "./title.style";

interface TitleProps {
    children: React.ReactNode;
    fontSize?: number;
    weight?: string;
    textTransform?: any;
}

const Title = ({
    children,
    fontSize,
    weight = "Bold",
    textTransform = "uppercase",
    ...rest
}: TitleProps) => (
    <Text
        style={[
            styles.title,
            { fontSize: fontSize, fontFamily: `Gotham-${weight}`, textTransform: textTransform }
        ]}
    >
        {children}
    </Text>
);

export default Title;
