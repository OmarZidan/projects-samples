import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    searchboxAndFiltersContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        height: 45,
        width: "100%",
        paddingHorizontal: 32
    },
    textInputContainer: {
        overflow: "hidden",
        height: 45,
        flexBasis: "80%"
    },
    filterButton: {
        borderRadius: 1,
        backgroundColor: "white",
        height: "100%",
        width: 45,
        justifyContent: "center",
        alignItems: "center"
    }
});

export default styles;
