import React from "react";
import { View, TouchableOpacityProps } from "react-native";
import styles from "./searchbox.style";
import TextInput from "../text-input/text-input";
import SquareButton from "../square-button/square-button";

interface SearchboxAndFiltersProps extends TouchableOpacityProps {
    onPress: () => void;
}
export default function SearchboxAndFilters({
    onPress
}: SearchboxAndFiltersProps): React.ReactElement {
    return (
        <View style={styles.searchboxAndFiltersContainer}>
            <View style={styles.textInputContainer}>
                <TextInput
                    placeholder="Search"
                    height={45}
                    style={{
                        flex: 1
                    }}
                    icon={require("@assets/icons/magnifyingGlass.png")}
                />
            </View>
            <SquareButton
                style={styles.filterButton}
                iconPath={require("@assets/icons/filterResultsButton.png")}
                onPress={onPress}
            />
        </View>
    );
}
