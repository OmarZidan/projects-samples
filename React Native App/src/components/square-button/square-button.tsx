import React from "react";
import {
    Image,
    TouchableOpacityProps,
    TouchableOpacity,
    ImageURISource,
    ImageSourcePropType
} from "react-native";

interface SquarButtonProps extends TouchableOpacityProps {
    iconPath: ImageSourcePropType;
    onPress: () => void;
}

function SquareButton({
    iconPath,
    style,
    onPress,
    ...props
}: SquarButtonProps): React.ReactElement {
    return (
        <TouchableOpacity style={style} {...props} onPress={onPress}>
            <Image source={iconPath} />
        </TouchableOpacity>
    );
}

export default SquareButton;
