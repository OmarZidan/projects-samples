import React from "react";
import { Text as OriginalText, TextProps as NativeTextProps } from "react-native";

interface TextProps extends NativeTextProps {
    weight?:
        | "Black"
        | "Medium"
        | "Regular"
        | "Light"
        | "Bold"
        | "SemiBold"
        | "ExtraBold"
        | "ExtraLight"
        | "Thin";
    children: React.ReactNode;
}

function Text({ weight = "Regular", children, ...props }: TextProps): React.ReactElement {
    return (
        <OriginalText
            {...props}
            style={[
                {
                    fontFamily: `Gotham-${weight}`
                },
                props.style
            ]}
        >
            {children}
        </OriginalText>
    );
}

export default Text;
