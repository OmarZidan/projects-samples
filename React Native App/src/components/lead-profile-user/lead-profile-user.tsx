import React, { useState } from "react";
import { Modal, Text, Image, View, TouchableWithoutFeedback, TouchableOpacity } from "react-native";
import ContactLead from "./../contact-lead/contact-lead";
import Button from "./../button/button";
import styles from "./lead-profile-user.style";
import { colors } from "@utils";

interface LeadItemProps {
    user: any;
}

function LeadProfileUser({ user }: LeadItemProps) {
    const [modalVisible, setModalVisible] = useState(false);

    const setVisible = visible => {
        setModalVisible(visible);
    };

    return (
        <View key={user.id} style={styles.leadContainer}>
            <View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={modalVisible}
                    // onRequestClose={() => {
                    //     Alert.alert("Modal has been closed.");
                    // }}
                >
                    <TouchableWithoutFeedback
                        onPress={() => {
                            setVisible(!modalVisible);
                        }}
                    >
                        <View style={styles.modalContainer}>
                            <TouchableWithoutFeedback>
                                <View style={styles.modal}>
                                    <View>
                                        <TouchableOpacity>
                                            <View style={styles.modalItem}>
                                                <Image
                                                    source={require("@assets/icons/compose.png")}
                                                    style={{ width: 32, height: 32 }}
                                                />
                                                <Text style={styles.modalText}>
                                                    Edit Lead Information
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity>
                                            <View style={styles.HLine} />
                                            <View style={styles.modalItem}>
                                                <Image
                                                    source={require("@assets/icons/compose.png")}
                                                    style={{ width: 32, height: 32 }}
                                                />
                                                <Text style={styles.modalText}>
                                                    Delete Lead from List
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        </View>
    );
}
export default LeadProfileUser;
