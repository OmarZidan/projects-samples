import React from "react";
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacityProps } from "react-native";
import styles from "./checkBox.style";

interface CheckBoxProps extends TouchableOpacityProps {
    toggleCheck: () => void;
    checked: boolean;
    label: string;
}

function CheckBox({ toggleCheck, checked, label }: CheckBoxProps): React.ReactElement {
    return (
        <TouchableWithoutFeedback
            style={{
                alignSelf: "flex-start",
                flexBasis: "33%"
            }}
            onPress={toggleCheck}
        >
            <View style={styles.acceptRow}>
                <View style={styles.checkBox}>
                    {checked && (
                        <Image source={require("@assets/icons/tick.png")} style={styles.tick} />
                    )}
                </View>
                <Text style={styles.checkBoxLabel}>{label}</Text>
            </View>
        </TouchableWithoutFeedback>
    );
}

export default CheckBox;
