import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    acceptRow: {
        flexDirection: "row",
        justifyContent: "flex-start",
        marginTop: 30,
        alignItems: "center",
        flexBasis: "33%"
    },
    checkBox: {
        width: 25,
        height: 25,
        marginRight: 10,
        backgroundColor: "white",
        alignItems: "center",
        justifyContent: "center"
    },
    tick: {
        width: 18,
        resizeMode: "contain"
    },
    checkBoxLabel: {
        fontFamily: "Gotham-Medium",
        fontSize: 11,
        lineHeight: 20
        // color: colors.charcoalGrey
    }
});

export default styles;
