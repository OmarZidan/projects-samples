import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    text: {
        fontFamily: "Gotham-Medium",
        fontSize: 13,
        color: colors.charcoalGrey,
        letterSpacing: 1,
        borderLeftWidth: 1,
        borderColor: colors.veryLightPink,
        height: 30,
        marginLeft: 5,
        paddingLeft: 20
    },
    flag: {
        borderWidth: 0,
        width: 30,
        height: 25
    },
    errorMessage: {
        fontSize: 12,
        color: colors.redWine,
        paddingVertical: 5
    }
});

export default styles;
