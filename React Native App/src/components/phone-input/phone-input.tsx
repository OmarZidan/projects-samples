import React, { forwardRef } from "react";
import { View, Text } from "react-native";
import styles from "./phone-input.style";
import inputStyles from "../text-input/text-input.style";
import PhoneInput from "react-native-phone-input";

interface PhoneInputProps {
    placeholder?: any;
    style?: any;
    height?: any;
    value: any;
    onChangePhoneNumber?: any;
    errorMessage?: any;
}
function Select(
    { height = 42, placeholder, errorMessage, value, style = {}, ...props }: PhoneInputProps,
    ref
) {
    return (
        <View>
            <View
                style={[
                    inputStyles.container,
                    { height },
                    style,
                    { borderBottomWidth: errorMessage ? 1 : null }
                ]}
            >
                <PhoneInput
                    initialCountry={null}
                    onPressFlag={() => null}
                    style={{ width: "100%" }}
                    // autoFormat
                    textStyle={styles.text}
                    textProps={{
                        placeholder
                        // ,
                        // ...props.textProps
                    }}
                    flagStyle={styles.flag}
                    ref={ref}
                    value={value}
                    {...props}
                />
            </View>
            {Boolean(errorMessage) && <Text style={styles.errorMessage}>{errorMessage}</Text>}
        </View>
    );
}

export default forwardRef(Select);
