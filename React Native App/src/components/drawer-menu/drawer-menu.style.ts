import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 5,
        elevation: 5
        // maxWidth: 225
    },
    inner: {
        padding: 30,
        paddingVertical: 60
    },
    menu: {
        marginTop: 50
    },
    menuItem: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 15,
        marginVertical: 12
    },
    menuItemIcon: {
        width: 30,
        height: 30,
        resizeMode: "contain",
        marginRight: 20,
        tintColor: "#fff"
    },
    menuItemText: {
        fontSize: 16,
        color: "#fff"
    },
    bottomMenu: {
        marginTop: 50,
        paddingHorizontal: 15,
        flexDirection: "row",
        alignItems: "center"
    },
    bottomMenuItemIcon: {
        width: 20,
        height: 20,
        resizeMode: "contain",
        marginRight: 15
    },
    bottomMenuItemText: {
        fontSize: 13,
        color: "#fff"
    },
    bottomMenuItem: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,
        marginVertical: 5
    },
    bottomMenuSeparator: {
        width: 1,
        height: 16,
        backgroundColor: "#fff",
        opacity: 0.5,
        marginHorizontal: 20
    },
    horizontalLine: {
        borderBottomColor: colors.white,
        borderBottomWidth: 2,
        width: "60%",
        marginHorizontal: "20%",
        opacity: 0.25
    },
    socialMediaSection: {
        justifyContent: "center",
        marginTop: 17,
        marginHorizontal: "21%"
    },
    socialMediaTitle: {
        color: colors.white,
        fontSize: 15,
        lineHeight: 24,
        letterSpacing: 0.08,
        marginBottom: 12
    },
    socialMediaIcon: {
        height: 35,
        width: 35,
        resizeMode: "cover"
    },
    absolute: {
        // @ts-ignore:
        ...StyleSheet.absoluteFill
    }
});

export default styles;
