import React from "react";
import {
    View,
    Animated,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ScrollView,
    Image,
    ImageRequireSource
} from "react-native";
import { colors } from "@utils";
import { BlurView } from "expo-blur";
import Text from "../text/text";
// import Button from "../button/button";
import NavigationService from "@config/navigationService";
import { LinearGradient } from "expo-linear-gradient";
import styles from "./drawer-menu.style";

const screenWidth = Dimensions.get("window").width;
const AnimatedBlurView = Animated.createAnimatedComponent(BlurView);

const items: {
    title: string;
    to?: string;
    icon?: ImageRequireSource;
    onPress?: () => void;
}[] = [
    {
        title: "Dashboard",
        icon: require("@assets/icons/dashboard.png"),
        to: "BrokerDashboard"
    },
    {
        title: "Leads",
        icon: require("@assets/icons/handshake.png"),
        to: "BrokerLeads"
    },
    {
        title: "Home",
        icon: require("@assets/icons/house.png"),
        to: "Home"
    },
    {
        title: "Availability",
        icon: require("@assets/icons/availability.png"),
        to: "Availability"
    },
    {
        title: "Press",
        icon: require("@assets/icons/calendar.png"),
        to: "Press"
    },
    {
        title: "Notifications",
        icon: require("@assets/icons/notification.png"),
        to: "Notifications"
    },
    {
        title: "Commissions History",
        icon: require("@assets/icons/abacus.png"),
        to: "CommissionsHistory"
    },

    {
        title: "Profile",
        icon: require("@assets/icons/user.png"),
        to: "UserProfile"
    }
];

function DrawerMenu({ progress, closeDrawer }: { [key: string]: any }) {
    return (
        <View style={{ flex: 1 }}>
            <AnimatedBlurView
                pointerEvents="none"
                tint="dark"
                intensity={60}
                style={[
                    styles.absolute,
                    {
                        left: -screenWidth,
                        opacity: progress.interpolate({
                            inputRange: [0, 0.2, 0.4, 1],
                            outputRange: [0, 0.95, 1, 1],
                            extrapolate: "clamp"
                        })
                    }
                ]}
            />

            <View style={styles.container}>
                <LinearGradient
                    colors={[colors.offWhite, colors.charcoalGrey]}
                    style={[styles.absolute, { opacity: 1 }]}
                    start={[0, 0]}
                    end={[1, 1]}
                />
                <ScrollView contentContainerStyle={styles.inner}>
                    {/* <Button
                        onPress={() => {
                            closeDrawer();
                        }}
                        style="white"
                        text="Close"
                        iconLeft={require("@assets/icons/close.png")}
                    /> */}
                    <TouchableOpacity
                        onPress={() => {
                            closeDrawer && closeDrawer();
                        }}
                        style={{
                            width: 50,
                            height: 50,
                            alignItems: "center",
                            alignSelf: "flex-end",
                            marginTop: 30
                        }}
                    >
                        <Image
                            style={{
                                width: 20,
                                height: 20,
                                tintColor: "#fff"
                            }}
                            source={require("@assets/icons/close.png")}
                        />
                    </TouchableOpacity>
                    <View style={styles.menu}>
                        {items.map(item => (
                            <TouchableOpacity
                                key={item.title}
                                onPress={() => {
                                    if (item.onPress) {
                                        item.onPress();
                                        return;
                                    }
                                    closeDrawer && closeDrawer();
                                    NavigationService.navigate(item.to || item.title, {});
                                }}
                                style={styles.menuItem}
                            >
                                <Image source={item.icon} style={styles.menuItemIcon} />
                                <Text weight="SemiBold" style={styles.menuItemText}>
                                    {item.title}
                                </Text>
                            </TouchableOpacity>
                        ))}
                    </View>
                </ScrollView>
            </View>
        </View>
    );
}

export default DrawerMenu;
