import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    projectsList: {
        marginTop: 40,
        marginBottom: 100
    },
    projectImage: {
        // width: "100%",
        height: 220,
        marginHorizontal: 2,
        marginTop: 2,
        resizeMode: "cover"
    }
});

export default styles;
