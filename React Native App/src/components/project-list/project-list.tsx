import React from "react";
import { View, Image, TouchableOpacity, Text } from "react-native";
import styles from "./project-list.style";
import { getAssetUrl } from "@utils";

interface ProjectItemProps {
    project: ProjectItem;
    navigation: any;
}

const ProjectItem = ({ project, navigation, ...props }: ProjectItemProps) => (
    <TouchableOpacity onPress={() => navigation.navigate("SingleProject", { id: project.id })}>
        <Image
            style={styles.projectImage}
            source={{ uri: getAssetUrl(project.featured_image) }}
        ></Image>
    </TouchableOpacity>
);

interface ProjectItem {
    id: number;
    featured_image: any;
}

interface ProjectListProps {
    projects: ProjectItem[];
    navigation: any;
}

const ProjectList = ({ projects, navigation, ...props }: ProjectListProps) => (
    <View style={styles.projectsList}>
        {projects.map(project => (
            <ProjectItem key={project.id} project={project} navigation={navigation} />
        ))}
    </View>
);

export default ProjectList;
