import React from "react";
import { Image, View } from "react-native";
import styles from "./select.style";
import RNPickerSelect, { PickerProps } from "react-native-picker-select";
import { Text } from "react-native";
import { colors } from "@utils";

interface SelectProps extends PickerProps {
    height?: number;
    errorMessage?: string;
    tonino?: boolean;
}

function Select({ height = 44, errorMessage, tonino, ...props }: SelectProps): React.ReactElement {
    return (
        <View>
            <View
                style={[
                    styles.container,
                    {
                        borderBottomWidth: errorMessage ? 1 : null,
                        backgroundColor: tonino ? colors.black40 : colors.white
                    }
                ]}
            >
                <RNPickerSelect
                    {...props}
                    placeholderTextColor={tonino ? colors.white : colors.charcoalGrey}
                    style={{
                        inputIOS: {
                            ...styles.input,
                            color: tonino ? colors.white : colors.charcoalGrey,
                            height
                        },
                        inputAndroid: {
                            ...styles.input,
                            color: tonino ? colors.white : colors.charcoalGrey,
                            height
                        },
                        iconContainer: styles.icon
                    }}
                    useNativeAndroidPickerStyle={false}
                    textInputProps={{
                        underlineColor: colors.charcoalGrey
                    }}
                    Icon={(): React.ReactElement => {
                        return (
                            <Image
                                source={require("@assets/icons/arrowPointToRight.png")}
                                style={{
                                    width: 14,
                                    height: 8,
                                    resizeMode: "contain",
                                    tintColor: tonino ? colors.white : colors.black
                                }}
                            />
                        );
                    }}
                />
            </View>
            {Boolean(errorMessage) && <Text style={styles.errorMessage}>{errorMessage}</Text>}
        </View>
    );
}

export default Select;
