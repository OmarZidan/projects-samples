import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        // backgroundColor: colors.white,
        borderBottomColor: colors.redWine
    },
    input: {
        fontFamily: "Gotham-Medium",
        // color: colors.charcoalGrey,
        fontSize: 11,
        paddingHorizontal: 16
    },
    icon: {
        right: 16,
        alignItems: "center",
        justifyContent: "center",
        height: "100%"
    },
    errorMessage: {
        fontSize: 12,
        color: colors.redWine,
        paddingVertical: 5
    }
});

export default styles;
