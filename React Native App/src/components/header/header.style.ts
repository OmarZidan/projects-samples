import { StyleSheet } from "react-native";
import { statusBarHeight, colors } from "@utils";
// import { colors } from "@utils";

const styles = StyleSheet.create({
    bg: {
        // ...StyleSheet.absoluteFill,
        backgroundColor: "transparent"
    },
    header: {
        position: "absolute",
        top: 0,
        width: "100%",
        paddingHorizontal: 25,
        // paddingTop: 100,

        // paddingTop: statusBarHeight(),
        // paddingBottom: 15,
        justifyContent: "center",
        zIndex: 101,
        elevation: 8
    },
    headerLogo: {
        width: "100%",
        height: 50,
        resizeMode: "contain",
        tintColor: colors.dark
    },
    headerInner: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%"
    },
    contactBtn: {
        alignSelf: "flex-end"
    },
    button: {
        width: 45,
        height: 45,
        // backgroundColor: colors.white,
        alignItems: "center",
        justifyContent: "center"
    }
});

export default styles;
