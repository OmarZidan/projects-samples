import React from "react";
import { Image, TouchableOpacity, View } from "react-native";
import styles from "./header.style";
// import { useDrawer } from "@contexts/drawer";
import { useNavigation } from "react-navigation-hooks";
import { colors } from "@utils";
import { useDrawer } from "@contexts/drawer";

interface HeaderProps {
    withBack?: boolean;
    withDrawerMenu?: boolean;
    weight?: string;
    renderRight?: boolean;
    renderLeft?: boolean;
    style?: any;
    backgroundColor?: string;
    opacity?: number;
    tonino?: boolean;
}
function Header({
    withBack = true,
    withDrawerMenu = true,
    renderRight,
    renderLeft,
    style,
    tonino,
    backgroundColor = "transparent",
    opacity = 1
}: HeaderProps) {
    // const { openDrawer } = useDrawer();
    const { goBack, navigate } = useNavigation();
    const { openDrawer } = useDrawer();
    function _renderLeft() {
        if (renderLeft || renderLeft === null) {
            return renderLeft;
        }
        if (withBack) {
            return (
                <TouchableOpacity
                    style={[
                        styles.button,
                        { backgroundColor: tonino ? colors.dark : colors.white }
                    ]}
                    onPress={() => goBack()}
                >
                    <Image
                        style={{
                            tintColor: tonino ? colors.white : colors.dark,
                            width: 22,
                            height: 18,
                            resizeMode: "contain"
                        }}
                        source={require("@assets/icons/path.png")}
                    />
                </TouchableOpacity>
            );
        }
        return null;
    }
    function _renderRight() {
        if (renderRight || renderRight === null) {
            return renderRight;
        }
        if (withDrawerMenu) {
            return (
                <TouchableOpacity style={styles.button} onPress={() => openDrawer()}>
                    <Image
                        style={{
                            tintColor: colors.dark,
                            width: 19,
                            height: 15,
                            resizeMode: "contain"
                        }}
                        source={require("@assets/icons/hamburger.png")}
                    />
                </TouchableOpacity>
            );
        }
        return null;
    }
    return (
        <View style={[styles.header, { height: 130 }, style]}>
            <View style={[styles.bg, { opacity, backgroundColor }]} />
            <View style={styles.headerInner}>
                <View style={{ width: "20%" }}>{_renderLeft()}</View>
                <View style={{ width: "60%", alignItems: "center" }}>
                    <TouchableOpacity onPress={() => navigate("Home")} style={[styles.headerLogo]}>
                        <Image
                            style={[
                                styles.headerLogo,
                                { tintColor: tonino ? colors.white : colors.dark }
                            ]}
                            source={require("@assets/images/newPlanLogo.png")}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ width: "20%", alignItems: "flex-end" }}>{_renderRight()}</View>
            </View>
        </View>
    );
}

export default Header;
