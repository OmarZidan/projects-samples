import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlueGrey
    },

    leadsContainer: {
        // marginTop: 28,
        // marginHorizontal: 32
    },
    leadContainer: {
        flexDirection: "row",
        backgroundColor: colors.white,
        marginBottom: 32,
        height: 220
    },
    leadImage: { height: "100%", width: 100 },
    leadContent: {
        padding: 15,
        flex: 1
    },
    btnContainer: { marginTop: 15 },
    status: {
        position: "absolute",
        left: 0,
        bottom: 0,
        width: 100,
        color: colors.white,
        fontSize: 10,
        fontFamily: "Gotham-Medium",
        paddingVertical: 6,
        textAlign: "center"
    },
    HLine: {
        borderBottomColor: colors.charcoalGrey16,
        borderBottomWidth: 1,
        marginVertical: 13
    },
    infoRow: {
        // marginBottom: 13,
        flexDirection: "row"
    },
    infoTitle: {
        fontFamily: "Gotham-Regular",
        fontSize: 8,
        letterSpacing: 0,
        marginBottom: 5
    },
    infoText: {
        fontFamily: "Gotham-Medium",
        fontSize: 10,
        letterSpacing: 0
    }
});

export default styles;
