import React, { useState } from "react";
import { Modal, Text, Image, View, TouchableWithoutFeedback, TouchableOpacity } from "react-native";
import ContactLead from "./../contact-lead/contact-lead";
import Button from "./../button/button";
import styles from "./lead-profile-item.style";
import { colors } from "@utils";

interface LeadItemProps {
    lead: any;
}

function LeadProfileItem({ lead }: LeadItemProps) {
    let background = "";
    switch (lead.status) {
        case "Contracted":
            background = colors.sickGreen;
            break;
        case "Down Payment":
            background = colors.offWhite;
            break;
        case "not interested":
            background = colors.redWine;
            break;
        case "URF":
            background = colors.offWhite;
            break;
        default:
            background = colors.redWine;
    }
    return (
        <View key={lead.id} style={styles.leadContainer}>
            <View>
                <Image style={styles.leadImage} source={lead.image} />
            </View>
            <View style={styles.leadContent}>
                <View style={[styles.infoRow, { marginBottom: 13 }]}>
                    <View style={{ width: "50%" }}>
                        <Text style={styles.infoTitle}>Unit Type:</Text>
                        <Text style={styles.infoText}>Penthouse</Text>
                    </View>
                    <View style={{ width: "50%" }}>
                        <Text style={styles.infoTitle}>Unit No.:</Text>
                        <Text style={styles.infoText}>A-709</Text>
                    </View>
                </View>
                <View style={styles.infoRow}>
                    <View style={{ width: "50%" }}>
                        <Text style={styles.infoTitle}>Total Price:</Text>
                        <Text style={styles.infoText}>EGP 7,560,000</Text>
                    </View>
                    <View style={{ width: "50%" }}>
                        <Text style={styles.infoTitle}>Commission (6%):</Text>
                        <Text style={styles.infoText}>EGP 459,000</Text>
                    </View>
                </View>
                <View style={styles.HLine} />
                <View style={styles.infoRow}>
                    <View>
                        <Text style={styles.infoTitle}>Contract Number:</Text>
                        <Text style={styles.infoText}>77809-564-289-445</Text>
                    </View>
                </View>
                <View style={styles.btnContainer}>
                    <Button
                        text="View Details"
                        backgroundColor={colors.dark}
                        textColor={colors.white}
                        fontSize={11}
                        weight="SemiBold"
                    />
                </View>
            </View>
            <Text style={[styles.status, { backgroundColor: background }]}>{lead.status}</Text>
        </View>
    );
}

export default LeadProfileItem;
