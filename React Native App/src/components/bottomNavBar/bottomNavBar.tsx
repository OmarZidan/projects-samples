import React from "react";
import { Text, TouchableOpacityProps, TouchableOpacity, View, Image } from "react-native";
import styles from "./bottomNavBar.style";
import { colors } from "@utils";
import { useNavigation } from "react-navigation-hooks";

interface BottomNavBarProps extends TouchableOpacityProps {
    currentRoute?: string;
}

function BottomNavBar({ currentRoute, ...props }: BottomNavBarProps): React.ReactElement {
    const { navigate } = useNavigation();

    return (
        <View style={styles.navBarContainer}>
            <TouchableOpacity
                onPress={(): void => {
                    navigate("BrokerDashboard");
                }}
                style={styles.itemContainer}
            >
                <Image
                    style={[
                        styles.itemIcon,
                        {
                            tintColor:
                                currentRoute === "brokerDashboard" ? colors.offWhite : colors.dark
                        }
                    ]}
                    source={require("@assets/images/BottomNavBar/dashboard.png")}
                />
                <Text
                    style={[
                        styles.itemTitle,
                        {
                            color:
                                currentRoute === "brokerDashboard" ? colors.offWhite : colors.dark
                        }
                    ]}
                >
                    Dashboard
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={(): void => {
                    navigate("BrokerLeads");
                }}
                style={styles.itemContainer}
            >
                <Image
                    style={[
                        styles.itemIcon,
                        {
                            tintColor: currentRoute === "leads" ? colors.offWhite : colors.dark
                        }
                    ]}
                    source={require("@assets/images/BottomNavBar/handshake.png")}
                />
                <Text
                    style={[
                        styles.itemTitle,
                        { color: currentRoute === "leads" ? colors.offWhite : colors.dark }
                    ]}
                >
                    Leads
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                // onPress={(): void => {
                //     navigate("Home");
                // }}
                style={styles.itemContainer}
            >
                <Image
                    style={{ height: 80, width: 80, position: "absolute", top: -35 }}
                    source={require("@assets/images/BottomNavBar/add.png")}
                />
                <Text style={[styles.itemTitle, { marginTop: 30 }]}>Add</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={(): void => {
                    navigate("Availability");
                }}
                style={styles.itemContainer}
            >
                <Image
                    style={[
                        styles.itemIcon,
                        {
                            tintColor:
                                currentRoute === "Availability" ? colors.offWhite : colors.dark
                        }
                    ]}
                    source={require("@assets/images/BottomNavBar/availability.png")}
                />
                <Text
                    style={[
                        styles.itemTitle,
                        { color: currentRoute === "Availability" ? colors.offWhite : colors.dark }
                    ]}
                >
                    Availability
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={(): void => {
                    navigate("Notifications");
                }}
                style={styles.itemContainer}
            >
                <Image
                    style={[
                        styles.itemIcon,
                        {
                            tintColor:
                                currentRoute === "Notifications" ? colors.offWhite : colors.dark
                        }
                    ]}
                    source={require("@assets/images/BottomNavBar/notification.png")}
                />
                <Text
                    style={[
                        styles.itemTitle,
                        { color: currentRoute === "Notifications" ? colors.offWhite : colors.dark }
                    ]}
                >
                    Notifications
                </Text>
            </TouchableOpacity>
        </View>
    );
}

export default BottomNavBar;
