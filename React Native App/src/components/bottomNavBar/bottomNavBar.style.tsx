import { StyleSheet } from "react-native";
import { colors } from "@utils";

const styles = StyleSheet.create({
    navBarContainer: {
        flexDirection: "row",
        flex: 1,
        justifyContent: "space-between",
        position: "absolute",
        zIndex: 101,
        elevation: 8,
        bottom: 0,
        width: "100%",
        backgroundColor: colors.white,
        paddingTop: 13,
        paddingHorizontal: 20
    },
    itemContainer: {
        justifyContent: "center",
        alignItems: "center",
        width: "18%"
    },
    itemIcon: {
        height: 27,
        width: 27,
        resizeMode: "contain"
    },
    itemTitle: {
        fontSize: 9,
        marginTop: 7,
        marginBottom: 13
    }
});

export default styles;
