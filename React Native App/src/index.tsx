import React, { useState } from "react";
import { View, StatusBar } from "react-native";
import * as Font from "expo-font";
import { AppLoading } from "expo";
import { AuthProvider } from "./contexts/AuthContext";
import AppContent from "./AppContent";

export default function App(): React.ReactNode {
    const [isReady, setIsReady] = useState(false);

    const _cacheSplashResourcesAsync = (): Promise<void> => {
        return Font.loadAsync({
            "Montserrat-Black": require("@assets/fonts/Montserrat-Black.otf"),
            "Montserrat-Medium": require("@assets/fonts/Montserrat-Medium.otf"),
            "Montserrat-Regular": require("@assets/fonts/Montserrat-Regular.otf"),
            "Montserrat-Light": require("@assets/fonts/Montserrat-Light.otf"),
            "Montserrat-Bold": require("@assets/fonts/Montserrat-Bold.otf"),
            "Montserrat-SemiBold": require("@assets/fonts/Montserrat-SemiBold.otf"),
            "Montserrat-ExtraBold": require("@assets/fonts/Montserrat-ExtraBold.otf"),
            "Montserrat-ExtraLight": require("@assets/fonts/Montserrat-ExtraLight.otf"),
            "Montserrat-Thin": require("@assets/fonts/Montserrat-Thin.otf"),
            "Gotham-Black": require("@assets/fonts/Gotham-Black.otf"),
            "Gotham-Medium": require("@assets/fonts/GothamMedium.ttf"),
            "Gotham-Regular": require("@assets/fonts/GothamBook.ttf"),
            "Gotham-Light": require("@assets/fonts/Gotham-Light.otf"),
            "Gotham-Bold": require("@assets/fonts/Gotham-Bold.otf"),
            "Gotham-SemiBold": require("@assets/fonts/GothamBook.ttf"),
            "Gotham-ExtraBold": require("@assets/fonts/Gotham-Black.otf"),
            "Gotham-ExtraLight": require("@assets/fonts/Gotham-XLight.otf"),
            "Gotham-Thin": require("@assets/fonts/Gotham-Thin.otf"),
            "Roboto-Bold": require("@assets/fonts/Roboto-Bold.ttf"),
            "ChakraPetch-Bold": require("@assets/fonts/ChakraPetch-Bold.ttf"),
            "Roboto-Regular": require("@assets/fonts/Roboto-Regular.ttf"),
            "Roboto-Medium": require("@assets/fonts/Roboto-Medium.ttf")
        }).then(() => {
            setIsReady(true);
        });
    };
    if (!isReady) {
        return (
            <AppLoading
                startAsync={_cacheSplashResourcesAsync}
                onFinish={(): void => {
                    setIsReady(true);
                }}
                autoHideSplash={true}
            />
        );
    }

    return (
        <View style={{ flex: 1 }}>
            {/* <StatusBar backgroundColor="blue" barStyle="light-content" /> */}
            <AuthProvider>
                <AppContent />
            </AuthProvider>
        </View>
    );
}
