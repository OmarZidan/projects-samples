import React, { useRef } from "react";
import Navigator from "@config/navigator";
import { DrawerMenu } from "@components";
import { View } from "react-native";
import DrawerLayout from "react-native-gesture-handler/DrawerLayout";
import NavigationService from "@config/navigationService";
import { DrawerProvider } from "@contexts/drawer";

// const screenWidth = Dimensions.get("window").width;

export default function AppContent() {
    const drawer = useRef(null);
    const openDrawer = () => {
        drawer && drawer.current.openDrawer();
    };
    const closeDrawer = () => {
        drawer && drawer.current.closeDrawer();
    };
    return (
        <DrawerLayout
            hideStatusBar={true}
            drawerWidth={320}
            overlayColor="rgba(1,1,1,0.8)"
            drawerPosition={"right"}
            drawerType="front"
            renderNavigationView={progress => (
                <DrawerMenu progress={progress} closeDrawer={closeDrawer} />
            )}
            ref={drawer}
        >
            <DrawerProvider openDrawer={openDrawer} closeDrawer={closeDrawer}>
                <View style={{ flex: 1 }}>
                    <Navigator
                        ref={navigatorRef => {
                            NavigationService.setTopLevelNavigator(navigatorRef);
                        }}
                    />
                </View>
            </DrawerProvider>
        </DrawerLayout>
    );
}
