import Constants from "expo-constants";

const isProduction = (): boolean => {
    const releaseChannel = Constants.manifest.releaseChannel;
    if (releaseChannel === undefined) return false;
    if (releaseChannel.indexOf("prod") !== -1) return true;
    if (releaseChannel.indexOf("staging") !== -1) return false;
    return false;
};

export default isProduction;
