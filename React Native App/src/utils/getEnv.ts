import Constants from "expo-constants";

const getEnv = (): "development" | "production" | "staging" => {
    const releaseChannel = Constants.manifest.releaseChannel;
    if (releaseChannel === undefined) return "development";
    if (releaseChannel.indexOf("prod") !== -1) return "production";
    if (releaseChannel.indexOf("staging") !== -1) return "staging";
    return "staging";
};

export default getEnv;
