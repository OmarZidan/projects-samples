import isProduction from "./isProduction";

const getAssetUrl = path =>
    isProduction()
        ? ``
        : ``;

export default getAssetUrl;
