const colors = {
    lightBlueGrey: "rgba(180,205,237, .25)",
    dark: "rgb(13, 24, 33)",
    dark90: "rgba(13, 24, 33, .9)",
    leather: "rgb(170, 115, 50)",
    offWhite: "rgb(168, 157, 155)",
    beige: "rgb(224, 204, 180)",
    white: "rgb(255,255,255)",
    white50: "rgba(255,255,255,.5)",
    sickGreen: "rgb(172, 193, 47)",
    sickGreen95: "rgba(172, 193, 47, .95)",
    redWine: "rgb(132, 0, 50)",
    paleLilac: "rgb(235, 235, 237)",
    charcoalGrey: "rgb(50, 56, 65)",
    charcoalGrey90: "rgba(50,56,65,.9)",
    charcoalGrey16: "rgba(50,56,65,.16)",
    charcoalGrey24: "rgba(50, 56, 65, .24)",
    charcoalGrey32: "rgba(50, 56, 65, .32)",
    blueyGrey: "rgb(148, 168, 194)",
    blueyGrey32: "rgba(148, 168, 194, .32)",
    black: "rgb(0,0,0)",
    black40: "rgba(0,0,0,.4)",
    veryLightPink: "rgba(198,198,198,.12)",
    veryLightPinkTwo: "rgb(216,216,216)",
    red: "rgb(255,0,0)",
    lightWhite: "rgba(255, 255, 255, .16)"
};

export default colors;
