import { createStackNavigator } from "react-navigation-stack";

import { BrokerDashboard } from "@screens";

const BrokerStack = createStackNavigator(
    {
        BrokerDashboard: BrokerDashboard
    },
    {
        initialRouteName: "BrokerDashboard",
        headerMode: "none"
    }
);

export default BrokerStack;
