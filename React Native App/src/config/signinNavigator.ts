import { createStackNavigator } from "react-navigation-stack";

import {
    Home,
    SignIn,
    CreatePassword,
    Projects,
    ProjectsFilters,
    Press,
    SingleProject,
    ConstructionUpdates,
    SingleConstructionUpdates,
    InterestedForm,
    SinglePressArticle,
    Contact,
    Gallery
} from "@screens";

const SignInStack = createStackNavigator(
    {
        Home: Home,
        Projects: Projects,
        ProjectsFilters: ProjectsFilters,
        Press: Press,
        SignIn: SignIn,
        CreatePassword: CreatePassword,
        SingleProject: SingleProject,
        ConstructionUpdates: ConstructionUpdates,
        SingleConstructionUpdates: SingleConstructionUpdates,
        InterestedForm: InterestedForm,
        SinglePressArticle,
        Contact: Contact,
        Gallery
    },
    {
        initialRouteName: "Home",
        headerMode: "none"
    }
);

export default SignInStack;
