import { createStackNavigator } from "react-navigation-stack";

import { Projects, CreatePassword } from "@screens";

const SalesStack = createStackNavigator(
    {
        Projects: Projects
    },
    {
        initialRouteName: "Projects",
        headerMode: "none"
    }
);

export default SalesStack;
