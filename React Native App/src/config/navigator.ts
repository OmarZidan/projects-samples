import {
    Home,
    About,
    BrokerDashboard,
    Availability,
    Press,
    SingleAvUnit,
    Projects,
    BrokerLeads,
    ProjectsFilters,
    SingleProject,
    AvailabilityFilters,
    Notifications,
    ConstructionUpdates,
    SingleConstructionUpdates,
    CreatePassword,
    ResetPassword,
    InviteBrokers,
    SignIn,
    RecoveryCode,
    UserProfile,
    EditUserProfile,
    PressFilters,
    DealsArchive,
    DealsFilters,
    SinglePressArticle,
    SalesDashboard,
    BrokerLeadsFilters,
    BrokerLeadsContacts,
    LeadProfile
} from "@screens";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

const SCREENS = {
    Home,
    About,
    BrokerDashboard,
    Availability,
    Projects,
    BrokerLeads,
    ProjectsFilters,
    Press,
    SingleAvUnit,
    SingleProject,
    AvailabilityFilters,
    Notifications,
    ConstructionUpdates,
    SingleConstructionUpdates,
    CreatePassword,
    ResetPassword,
    InviteBrokers,
    SignIn,
    RecoveryCode,
    UserProfile,
    EditUserProfile,
    PressFilters,
    DealsArchive,
    DealsFilters,
    SinglePressArticle,
    SalesDashboard,
    BrokerLeadsFilters,
    BrokerLeadsContacts,
    LeadProfile
};

import BrokerStack from "./brokerNavigator";
import SalesStack from "./salesNavigator";
import SigninStack from "./signinNavigator";
import { AuthLoading } from "@screens";

// const AppStack = createStackNavigator(SCREENS, {
//     initialRouteName: "LeadProfile",
//     headerMode: "none"
// });

const AppStack = createSwitchNavigator(
    {
        Loading: AuthLoading,
        Broker: BrokerStack,
        Sales: SalesStack,
        SignIn: SigninStack
    },
    // { initialRouteName: "Loading" }
    { initialRouteName: "SignIn" }
);

export default createAppContainer(AppStack);
