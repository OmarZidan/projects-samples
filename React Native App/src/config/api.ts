import axios, { AxiosResponse } from "axios";
import { isProduction } from "@utils";

export const SERVER_URL = isProduction()
    ? ""
    : "";

export const API_URL = isProduction() ? `${SERVER_URL}/api` : `${SERVER_URL}/api`;

const newPlan = axios.create({
    baseURL: API_URL
});

export default newPlan;
