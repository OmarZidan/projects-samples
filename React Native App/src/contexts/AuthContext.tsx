/* eslint-disable @typescript-eslint/camelcase */
import React, { createContext, useState } from "react";

export const AuthContext = createContext(undefined);

interface AuthProviderProps {
    children: any;
}
export const AuthProvider = ({ children }: AuthProviderProps) => {
    const [user, setUser] = useState({
        access_token: "123",
        first_time_login: 0,
        id: "",
        role: ""
    });

    return <AuthContext.Provider value={[user, setUser]}>{children}</AuthContext.Provider>;
};
