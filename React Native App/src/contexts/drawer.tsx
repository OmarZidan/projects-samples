import React, { createContext } from "react";

const DrawerContext = createContext<
    | {
          openDrawer: () => void;
          closeDrawer: () => void;
      }
    | undefined
>(undefined);

function useDrawer() {
    const context = React.useContext(DrawerContext);
    if (!context) {
        throw new Error(`useDrawer must be used within a DrawerProvider`);
    }
    return context;
}
function DrawerProvider({ openDrawer, closeDrawer, ...props }: { [key: string]: any }) {
    return (
        <DrawerContext.Provider
            {...props}
            value={{
                openDrawer,
                closeDrawer
            }}
        />
    );
}
export { DrawerProvider, useDrawer };
